/*!
 *  \author    Dirk Kraft
 *  \date      2014-2015
 *  \copyright BSD 3-clause
 */

#ifndef COL_SEG_H
#define COL_SEG_H

#include "ui_col_seg.h"
#include <opencv2/opencv.hpp>

class ColorSegmentationWindow: public QMainWindow {
    Q_OBJECT

    public:
        ColorSegmentationWindow(QMainWindow *parent = 0);
        void loadFile(QString fileName);
        void loadDir(QString dirName, bool add);

    private:
        void displayImage(cv::Mat image);
        void saveFile(QString fileName);
        void resizeEvent(QResizeEvent * event);
        void morphImage(cv::Mat & image) const;
        void dirMode(bool enable);
        void setImageFromList(int index);

    private slots:
        void onLoadFilePressed();
        void onLoadDirPressed();
        void onAddDirPressed();
        void onSaveFilePressed();
        void onSegParametersHSVChanged();
        void onSegParametersRGBChanged();
        void onCamImagePressed();
        void onImageChanged();
        void onResizeModeChanged();
        void onMorphChanged();
        void onLeftPressed();
        void onRightPressed();

    signals:
        void imageChanged();
        void resizeModeChanged();

    private:
        Ui::MainWindow _ui;
        cv::Mat _src_loaded;
        cv::Mat _src;
        cv::Mat _dst;
        cv::Mat _dispCV;
        bool _dirMode;
        QStringList _imageNames;
        unsigned int _imageIndex;
};

#endif
