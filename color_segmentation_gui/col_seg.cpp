/*!
 *  \author    Dirk Kraft
 *  \date      2014-2015
 *  \copyright BSD 3-clause
 */

#include <iostream>
#include <QtGui>
#include <QFileDialog>
#include <QMessageBox>
#include <QComboBox>

#include "col_seg.h"

ColorSegmentationWindow::ColorSegmentationWindow(QMainWindow *parent) :
        QMainWindow(parent) {
    _ui.setupUi(this);
    connect(_ui.pbLoadImage, SIGNAL(pressed()), this, SLOT(onLoadFilePressed()));
    connect(_ui.pbLoadDir, SIGNAL(pressed()), this, SLOT(onLoadDirPressed()));
    connect(_ui.pBAddDir, SIGNAL(pressed()), this, SLOT(onAddDirPressed()));
    connect(_ui.pbSaveImage, SIGNAL(pressed()), this, SLOT(onSaveFilePressed()));
    connect(_ui.pbCamImage, SIGNAL(pressed()), this, SLOT(onCamImagePressed()));

    connect(this, SIGNAL(imageChanged()), this, SLOT(onImageChanged()));

    connect(_ui.colorTab, SIGNAL(currentChanged(int)), this, SLOT(onImageChanged()));

    connect(_ui.cbResize, SIGNAL(currentIndexChanged(int)), this, SLOT(onResizeModeChanged()));

    connect(this, SIGNAL(resizeModeChanged()), this, SLOT(onResizeModeChanged()));

    // HSV controls
    connect(_ui.cbViewHSV, SIGNAL(currentIndexChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsHmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsHmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsSmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsSmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsVmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));
    connect(_ui.hsVmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersHSVChanged()));

    // RGB controls
    connect(_ui.cbViewRGB, SIGNAL(currentIndexChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsRmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsRmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsGmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsGmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsBmin, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));
    connect(_ui.hsBmax, SIGNAL(valueChanged(int)), this, SLOT(onSegParametersRGBChanged()));

    connect(_ui.cbMorph, SIGNAL(currentIndexChanged(int)), this, SLOT(onMorphChanged()));
    connect(_ui.sbMorphKernelS, SIGNAL(valueChanged(int)), this, SLOT(onMorphChanged()));

    connect(_ui.pBLeft, SIGNAL(pressed()), this, SLOT(onLeftPressed()));
    connect(_ui.pBRight, SIGNAL(pressed()), this, SLOT(onRightPressed()));
}

void ColorSegmentationWindow::onLoadFilePressed() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"));
    if (fileName != "") {
        dirMode(false);
        loadFile(fileName);
    }
}

void ColorSegmentationWindow::onLoadDirPressed() {
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dirName != "") {
        loadDir(dirName, false);
    }
}

void ColorSegmentationWindow::onAddDirPressed() {
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dirName != "") {
        loadDir(dirName, true);
    }
}

void ColorSegmentationWindow::onSaveFilePressed() {
    saveFile("out.png");
}

void ColorSegmentationWindow::onImageChanged() {
    if (_ui.colorTab->currentIndex() == 0) {
        onSegParametersHSVChanged();
    } else if (_ui.colorTab->currentIndex() == 1) {
        onSegParametersRGBChanged();
    } else {
        std::cout << "Unknown tab: " << _ui.colorTab->currentIndex() << std::endl;
        abort();
    }
}

void ColorSegmentationWindow::onMorphChanged() {
    if (_ui.colorTab->currentIndex() == 0) {
        onSegParametersHSVChanged();
    } else if (_ui.colorTab->currentIndex() == 1) {
        onSegParametersRGBChanged();
    } else {
        std::cout << "Unknown tab: " << _ui.colorTab->currentIndex() << std::endl;
        abort();
    }
}

void ColorSegmentationWindow::loadFile(QString fileName) {
    cv::Mat newSrc = cv::imread(fileName.toUtf8().constData(), CV_LOAD_IMAGE_COLOR);
    if (!newSrc.data) {
        QMessageBox::warning(this, "Error", "Unable to read file " + fileName + " as an image!");
    } else {
        _src_loaded = newSrc;
        if (_ui.cbResize->currentText() == "Resize before processing") {
            double scaleW = ((double)_ui.lImage->size().width()) / _src_loaded.cols;
            double scaleH = ((double)_ui.lImage->size().height()) / _src_loaded.rows;
            double scale = std::min(scaleW, scaleH);
            cv::resize(_src_loaded, _src, cv::Size(0, 0), scale, scale, cv::INTER_LINEAR);
        } else {
            _src = _src_loaded;
        }
        emit(imageChanged());
    }
}

void ColorSegmentationWindow::loadDir(QString dirName, bool add) {
    QStringList filters;
    filters << "*.png" << "*.jpg";

    QDir dir = QDir(dirName);

    QStringList files = dir.entryList(filters);

    if (files.size() > 0) {

        if (!add) {
            _imageNames.clear();
        }
        for (unsigned int i = 0; i< files.size(); i++) {
            _imageNames.push_back(dir.absoluteFilePath(files.at(i)));

        }
        if (!add) {
            _imageIndex = 0;
        }
        dirMode(true);

        if (!add) {
            loadFile(_imageNames.at(_imageIndex));
        }
    } else {
        QMessageBox::warning(this, "Error", "No png or jpg images in " + dirName);
    }
}

void ColorSegmentationWindow::saveFile(QString fileName) {
	cv::Mat saveFile;
	if (_dispCV.channels() == 3) {
		cv::cvtColor(_dispCV, saveFile, CV_BGR2RGB);
	} else {
		saveFile = _dispCV;
	}
    cv::imwrite(fileName.toUtf8().constData(), saveFile);
}

void ColorSegmentationWindow::onCamImagePressed() {

    cv::VideoCapture cap(0);

    if (!cap.isOpened()) {
        QMessageBox::warning(this, "Error", "Unable to find a camera!");
        return;
    }

    // Capture multiple images to give the auto stuff a chance to work
    for (int i = 0; i < 30; i++) {
        cap >> _src_loaded;
    }

    if (_ui.cbResize->currentText() == "Resize before processing") {
        double scaleW = ((double)_ui.lImage->size().width()) / _src_loaded.cols;
        double scaleH = ((double)_ui.lImage->size().height()) / _src_loaded.rows;
        double scale = std::min(scaleW, scaleH);
        cv::resize(_src_loaded, _src, cv::Size(0, 0), scale, scale, cv::INTER_LINEAR);
    } else {
        _src = _src_loaded;
    }

    emit(imageChanged());
}

void ColorSegmentationWindow::onSegParametersHSVChanged() {
    if (!_src.data) {
        return;
    }

    if (_ui.cbViewHSV->currentText() == "Original image") {
        cv::cvtColor(_src, _dispCV, CV_BGR2RGB);
    } else if (_ui.cbViewHSV->currentText() == "Segmented mask") {
        cv::Mat srcHSV;
        cv::cvtColor(_src, srcHSV, CV_BGR2HSV);
        int hmin = _ui.hsHmin->value();
        int hmax = _ui.hsHmax->value();
        _dst.create(_src.rows, _src.cols, CV_8U);

        cv::MatIterator_<cv::Vec3b> its, ends;
        cv::MatIterator_<uchar> itd, endd;

        for (its = srcHSV.begin<cv::Vec3b>(), ends = srcHSV.end<cv::Vec3b>(), itd = _dst.begin<uchar>(), endd =
                _dst.end<uchar>(); (its != ends) && (itd != endd); ++its, ++itd) {

            if (((hmin <= hmax) && (((*its).val[0] < hmin / 2) || ((*its).val[0] >= hmax / 2)))
                    || ((hmin > hmax) && (((*its).val[0] >= hmax / 2) && ((*its).val[0] <= hmin / 2)))
                    || ((*its).val[1] < _ui.hsSmin->value()) || ((*its).val[1] >= _ui.hsSmax->value())
                    || ((*its).val[2] < _ui.hsVmin->value()) || ((*its).val[2] >= _ui.hsVmax->value())) {
                *itd = 0;
            } else {
                *itd = 255;
            }

        }

        morphImage(_dst);
        cv::cvtColor(_dst, _dispCV, CV_GRAY2RGB);
    } else if (_ui.cbViewHSV->currentText() == "Original image segmented") {
        cv::Mat srcHSV;
        cv::cvtColor(_src, srcHSV, CV_BGR2HSV);
        int hmin = _ui.hsHmin->value();
        int hmax = _ui.hsHmax->value();

        cv::Mat mask(_src.rows, _src.cols, CV_8U);
        {
			cv::MatIterator_<cv::Vec3b> its, ends;
			cv::MatIterator_<uchar> itd, endd;

			for (its = srcHSV.begin<cv::Vec3b>(), ends = srcHSV.end<cv::Vec3b>(), itd = mask.begin<uchar>(), endd =
					mask.end<uchar>(); (its != ends) && (itd != endd); ++its, ++itd) {

				if (((hmin <= hmax) && (((*its).val[0] < hmin / 2) || ((*its).val[0] >= hmax / 2)))
						|| ((hmin > hmax) && (((*its).val[0] >= hmax / 2) && ((*its).val[0] <= hmin / 2)))
						|| ((*its).val[1] < _ui.hsSmin->value()) || ((*its).val[1] >= _ui.hsSmax->value())
						|| ((*its).val[2] < _ui.hsVmin->value()) || ((*its).val[2] >= _ui.hsVmax->value())) {
					*itd = 0;
				} else {
					*itd = 255;
				}

			}
        }
        morphImage(mask);

        _src.copyTo(_dst);
        {
        	cv::MatIterator_<cv::Vec3b> its, ends;
			cv::MatIterator_<cv::Vec3b> itd, endd;
			cv::MatIterator_<uchar> itm, endm;

			for (its = _src.begin<cv::Vec3b>(), ends = _src.end<cv::Vec3b>(),
				 itd = _dst.begin<cv::Vec3b>(), endd = _dst.end<cv::Vec3b>(),
				 itm = mask.begin<uchar>(), endm = mask.end<uchar>();
				 (its != ends) && (itd != endd) && (itm != endm);
				 ++its, ++itd, ++itm) {

				if ((*itm) == 0) {
					*itd = cv::Vec3b(0, 0, 0);
				} else {
					*itd = (*its);
				}

			}
        }

        cv::cvtColor(_dst, _dispCV, CV_BGR2RGB);
    } else if ((_ui.cbViewHSV->currentText() == "Original H") || (_ui.cbViewHSV->currentText() == "Original S")
            || (_ui.cbViewHSV->currentText() == "Original V")) {
        cv::Mat srcHSV;
        cv::cvtColor(_src, srcHSV, CV_BGR2HSV);
        cv::Mat srcHSVchannels[3];
        cv::split(srcHSV, srcHSVchannels);
        if (_ui.cbViewHSV->currentText() == "Original H") {
            cv::cvtColor(srcHSVchannels[0], _dispCV, CV_GRAY2RGB);
        } else if (_ui.cbViewHSV->currentText() == "Original S") {
            cv::cvtColor(srcHSVchannels[1], _dispCV, CV_GRAY2RGB);
        } else if (_ui.cbViewHSV->currentText() == "Original V") {
            cv::cvtColor(srcHSVchannels[2], _dispCV, CV_GRAY2RGB);
        }
    } else if (_ui.cbViewHSV->currentText() == "Segmented mask H") {
    } else if (_ui.cbViewHSV->currentText() == "Segmented mask S") {
    } else if (_ui.cbViewHSV->currentText() == "Segmented mask V") {
    } else {
        std::cout << "Unknown view value: " << qPrintable(_ui.cbViewHSV->currentText()) << std::endl;
        abort();
    }
    displayImage(_dispCV);
}

void ColorSegmentationWindow::onSegParametersRGBChanged() {
    if (!_src.data) {
        return;
    }

    if (_ui.cbViewRGB->currentText() == "Original image") {
        cv::cvtColor(_src, _dispCV, CV_BGR2RGB);
    } else if (_ui.cbViewRGB->currentText() == "Segmented mask") {
        cv::inRange(_src, cv::Vec3b(_ui.hsBmin->value(), _ui.hsGmin->value(), _ui.hsRmin->value()),
                cv::Vec3b(_ui.hsBmax->value() - 1, _ui.hsGmax->value() - 1, _ui.hsRmax->value() - 1), _dst);
        morphImage(_dst);
        cv::cvtColor(_dst, _dispCV, CV_GRAY2RGB);
    } else if (_ui.cbViewRGB->currentText() == "Original image segmented") {
        _src.copyTo(_dst);

        cv::Mat mask;
        cv::inRange(_src, cv::Vec3b(_ui.hsBmin->value(), _ui.hsGmin->value(), _ui.hsRmin->value()),
                 	 cv::Vec3b(_ui.hsBmax->value() - 1, _ui.hsGmax->value() - 1, _ui.hsRmax->value() - 1), mask);
        morphImage(mask);

        cv::MatIterator_<cv::Vec3b> its, ends;
        cv::MatIterator_<cv::Vec3b> itd, endd;
        cv::MatIterator_<uchar> itm, endm;

        for (its = _src.begin<cv::Vec3b>(), ends = _src.end<cv::Vec3b>(),
             itd = _dst.begin<cv::Vec3b>(), endd = _dst.end<cv::Vec3b>(),
			 itm = mask.begin<uchar>(), endm = mask.end<uchar>();
        	 (its != ends) && (itd != endd) && (itm != endm);
        	 ++its, ++itd, ++itm) {

        	if ((*itm) == 0) {
        		*itd = cv::Vec3b(0, 0, 0);
        	} else {
        		*itd = (*its);
        	}

        }
        cv::cvtColor(_dst, _dispCV, CV_BGR2RGB);
    } else if ((_ui.cbViewRGB->currentText() == "Original R") || (_ui.cbViewRGB->currentText() == "Original G")
            || (_ui.cbViewRGB->currentText() == "Original B")) {
        cv::Mat srcBGRchannels[3];
        cv::split(_src, srcBGRchannels);
        if (_ui.cbViewRGB->currentText() == "Original R") {
            cv::cvtColor(srcBGRchannels[2], _dispCV, CV_GRAY2RGB);
        } else if (_ui.cbViewRGB->currentText() == "Original G") {
            cv::cvtColor(srcBGRchannels[1], _dispCV, CV_GRAY2RGB);
        } else if (_ui.cbViewRGB->currentText() == "Original B") {
            cv::cvtColor(srcBGRchannels[0], _dispCV, CV_GRAY2RGB);
        }
    } else if (_ui.cbViewRGB->currentText() == "Segmented mask R") {
    } else if (_ui.cbViewRGB->currentText() == "Segmented mask G") {
    } else if (_ui.cbViewRGB->currentText() == "Segmented mask B") {
    } else {
        std::cout << "Unknown view value: " << qPrintable(_ui.cbViewRGB->currentText()) << std::endl;
        abort();
    }
    displayImage(_dispCV);
}

void ColorSegmentationWindow::resizeEvent(QResizeEvent * event) {
    if (_ui.cbResize->currentText() == "Resize before processing") {
        emit(resizeModeChanged());
    }
}

void ColorSegmentationWindow::onResizeModeChanged() {
    if (_ui.cbResize->currentText() == "Resize before processing") {
        double scaleW = ((double)_ui.lImage->size().width()) / _src_loaded.cols;
        double scaleH = ((double)_ui.lImage->size().height()) / _src_loaded.rows;
        double scale = std::min(scaleW, scaleH);
        cv::resize(_src_loaded, _src, cv::Size(0, 0), scale, scale, cv::INTER_LINEAR);
        emit(imageChanged());
    } else if (_ui.cbResize->currentText() == "Resize after processing") {
    	_src = _src_loaded;
        emit(imageChanged());
    } else {
    	_src = _src_loaded;
    	emit(imageChanged());
    }
}

void ColorSegmentationWindow::displayImage(cv::Mat image) {
    if (_ui.cbResize->currentText() == "Resize after processing") {
        double scaleW = ((double)_ui.lImage->size().width()) / image.cols;
        double scaleH = ((double)_ui.lImage->size().height()) / image.rows;
        double scale = std::min(scaleW, scaleH);
        cv::resize(image, image, cv::Size(0, 0), scale, scale, cv::INTER_LINEAR);
    }
    QImage dispQt = QImage((uchar*)image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
    _ui.lImage->setPixmap(QPixmap::fromImage(dispQt));
}

void ColorSegmentationWindow::morphImage(cv::Mat & image) const {
	if (_ui.cbMorph->currentText() == "None")
		return;

	int kernelSize = _ui.sbMorphKernelS->value();
	cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(kernelSize, kernelSize));

	if (_ui.cbMorph->currentText() == "Erosion") {
		cv::erode( image, image, element);
	} else if (_ui.cbMorph->currentText() == "Dilation") {
		cv::dilate( image, image, element);
	}  else if (_ui.cbMorph->currentText() == "Opening") {
		cv::morphologyEx(image, image, cv::MORPH_OPEN, element);
	}  else if (_ui.cbMorph->currentText() == "Closing") {
		cv::morphologyEx(image, image, cv::MORPH_CLOSE, element);
	}  else {
		std::cout << "Unknown morph value: " << qPrintable(_ui.cbMorph->currentText()) << std::endl;
		abort();
	}

}

void ColorSegmentationWindow::dirMode(bool enable) {
    _dirMode = enable;

    _ui.pBLeft->setEnabled(enable);
    _ui.pBRight->setEnabled(enable);
    _ui.pBAddDir->setEnabled(enable);

    if (enable) {
        if (_imageIndex == 0) {
            _ui.pBLeft->setEnabled(false);
        } else if (_imageIndex == (_imageNames.size()-1)) {
            _ui.pBRight->setEnabled(false);
        }
    }

}

void ColorSegmentationWindow::onLeftPressed() {
    if (_imageIndex > 0) {
        setImageFromList(--_imageIndex);
        dirMode(true);
    }
}

void ColorSegmentationWindow::onRightPressed() {
    if (_imageIndex < (_imageNames.size()-1)) {
        setImageFromList(++_imageIndex);
        dirMode(true);
    }
}

void ColorSegmentationWindow::setImageFromList(int index) {
    loadFile(_imageNames.at(index));
}
