/*! 
 *  \author    Dirk Kraft
 *  \date      2014-2015
 *  \copyright BSD 3-clause
 */

#include <QApplication>
#include "col_seg.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    ColorSegmentationWindow colSeg;
    colSeg.show();
    if (QCoreApplication::arguments().size() == 2) {
        QString fileName = QCoreApplication::arguments().at(1);
        colSeg.loadFile(fileName);
    }
    return app.exec();
}
