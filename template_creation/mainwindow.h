#define PCL_NO_PRECOMPILE
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "QVTKWidget.h"


#include <pcl/visualization/cloud_viewer.h>
typedef pcl::PointXYZRGBA PointT;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    pcl::PointCloud<PointT>::Ptr mainCloud;
    pcl::PointCloud<PointT>::Ptr small_organized;
    pcl::PointCloud<PointT>::Ptr display_points_query_cloud;
    pcl::PointCloud<PointT>::Ptr display_points_target_cloud;
    std::vector<int> points_query;
    std::vector<int> points_target;
    std::vector<pcl::PointXYZ> color_query;
    std::vector<pcl::PointXYZ> color_target;

    //display features
    pcl::PointCloud<PointT>::Ptr mainCloudDispayFeaturesTab;
    std::vector<int> points_display_features;
    std::vector<pcl::PointXYZ> color_display_features;
    std::vector<int> points_display_features_real_idx;
    std::vector<int> points_display_features_edge_idx;
    
private:
    Ui::MainWindow *ui;
    QString rgbImageQString;
    QString depthImageQString;
    QString binaryMaskQString;
    QString pcdFileQString;
    void showPointCloud(QVTKWidget* widget, pcl::PointCloud<PointT>::Ptr);
    //void showEdgeCloud(QVTKWidget* widget, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr pointCloud);
    void renderPointCloudWithSelectedPoints(QVTKWidget* widget, pcl::PointCloud<PointT>::Ptr pointCloud, std::vector<int> points, std::vector<pcl::PointXYZ> colors, float radius);
    bool usingBinaryMap;

    //DISPLAY FEATURES TAB
    void renderPointCloudWithSelectedPointsEdgeCloud();
    void renderPointCloudWithSelectedPointsOriginalCloud();
    void drawSpheres(std::vector<int> idx_original, bool left);


private slots:
    void loadPCDFileEvent();
    void loadRGBDImgsEvent();
    void createPCDfromRGBD();
    void loadBinaryMaskEvent();
    void createTemplateEvent();
    void saveTemplateEvent();
    void loadPCDFileForDispalyQueryPointsEvent();
    void loadPCDFileForDispalyTargetPointsEvent();
    void drawSelectedPointQuery();
    void resetButtonClicked();
    void drawSelectedPointTarget();
    void loadTargetFile();

    //DISPLAY FEATURES TAB
    void loadPCDFileEventDispayFeatures();
    void computeEdgesDispayFeatures();
    void displayEdgeNormals();
    void displayEdgeGradient();
    void processColor();
    void drawSelectedPointDisplayFeaturesTab();
    void resetDisplayFeatures();
};

#endif // MAINWINDOW_H
