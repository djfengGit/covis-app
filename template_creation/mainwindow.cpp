#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include "QVTKWidget.h"
#include <vtkRenderWindow.h>

#include <covis/covis.h>
#include "covislib/pcl_edge_detection/organized_edge_detection.h"

#include <opencv2/opencv.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/features/shot_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>

#include <fstream>

#include <pcl/filters/impl/radius_outlier_removal.hpp>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/search/search.h>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/search/impl/organized.hpp>
#include <pcl/search/organized.h>
#include <pcl/filters/impl/filter.hpp>
#include <pcl/filters/filter.h>
#include <pcl/filters/impl/filter_indices.hpp>
#include <pcl/filters/filter_indices.h>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/pcl_base.h>
#include <pcl/outofcore/outofcore.h>



typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointXYZRGB PointTR;

//using namespace covis;
//using namespace covis::calib;
using namespace cv;
using namespace std;

//DISPLAY FEATURES TAB
pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edgeCloudDispayFeaturesTab;
pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge;
vtkSmartPointer<vtkRenderWindow> renderWindow;
boost::shared_ptr<pcl::visualization::PCLVisualizer> vis (new pcl::visualization::PCLVisualizer ("3D Viewer", false));
bool existNormals = false, existGradient = false, existEdges = false;
float rad_sphere = 0.01;//0.005;

vtkSmartPointer<vtkRenderWindow> renderWindow_original;
boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_original (new pcl::visualization::PCLVisualizer ("3D Viewer original", false));



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->loadStereoImgsButton, SIGNAL(clicked()), this, SLOT(loadRGBDImgsEvent()));
    connect(ui->create_pcd_from_rgbd, SIGNAL(clicked()), this, SLOT(createPCDfromRGBD()));
    connect(ui->create_template_button, SIGNAL(clicked()), this, SLOT(createTemplateEvent()));
    connect(ui->load_binary_mask_button, SIGNAL(clicked()), this, SLOT(loadBinaryMaskEvent()));
    connect(ui->refresh_button, SIGNAL(clicked()), this, SLOT(createTemplateEvent()));
    connect(ui->save_template_button, SIGNAL(clicked()), this, SLOT(saveTemplateEvent()));
    connect(ui->load_pcd_button, SIGNAL(clicked()), this, SLOT(loadPCDFileEvent()));
    ui->Templatesettings->setVisible(false);
    ui->create_template_button->setVisible(false);
    ui->save_template_button->setVisible(false);
    ui->amount_of_points_template->setVisible(false);
    ui->amount_of_points->setVisible(false);
    usingBinaryMap = false;

    connect(ui->load_query_pcd, SIGNAL(clicked()), this, SLOT(loadPCDFileForDispalyQueryPointsEvent()));
    connect(ui->load_target_pcd, SIGNAL(clicked()), this, SLOT(loadPCDFileForDispalyTargetPointsEvent()));
    connect(ui->show_query, SIGNAL(clicked()), this, SLOT(drawSelectedPointQuery()));
    connect(ui->show_target, SIGNAL(clicked()), this, SLOT(drawSelectedPointTarget()));
    connect(ui->reset_points_button, SIGNAL(clicked()), this, SLOT(resetButtonClicked()));
    connect(ui->load_target_file, SIGNAL(clicked()), this, SLOT(loadTargetFile()));

    ui->sphere_radius->setPlainText("0.01");
    ui->color_r->setPlainText("0.0");
    ui->color_g->setPlainText("0.0");
    ui->color_b->setPlainText("1.0");


    //DISPLAY FEATURES TAB

    ui->sphere_radius_display_features->setPlainText("0.001");
    ui->color_r_display_features->setPlainText("0.0");
    ui->color_g_display_features->setPlainText("0.0");
    ui->color_b_display_features->setPlainText("1.0");

    ui->groupBox_compute_edges_display_points->setVisible(false);
    ui->display_display_features->setVisible(false);

    connect(ui->display_features_load_object_pcd, SIGNAL(clicked()), this, SLOT(loadPCDFileEventDispayFeatures()));
    connect(ui->compute_edges_display_features, SIGNAL(clicked()), this, SLOT(computeEdgesDispayFeatures()));

    connect(ui->display_normals_button_display_features, SIGNAL(clicked()), this, SLOT(displayEdgeNormals()));
    connect(ui->display_edge_gradient_button_display_features, SIGNAL(clicked()), this, SLOT(displayEdgeGradient()));
    connect(ui->color_button_display_features, SIGNAL(clicked()), this, SLOT(processColor()));
    connect(ui->show_point_display_features, SIGNAL(clicked()), this, SLOT(drawSelectedPointDisplayFeaturesTab()));
    connect(ui->reset_points_button_display_features, SIGNAL(clicked()), this, SLOT(resetDisplayFeatures()));


}

void MainWindow::loadBinaryMaskEvent()
{
    this->binaryMaskQString = QFileDialog::getOpenFileName(this, tr("Open File"), rgbImageQString , tr("Files (*.*)"));
    QPixmap pixMask = QPixmap(binaryMaskQString);
    ui->maskImageView->setPixmap(pixMask.scaled(ui->maskImageView->size(), Qt::KeepAspectRatio));
    usingBinaryMap = true;
    ui->usingBinaryMask->setChecked(true);

}

void MainWindow::saveTemplateEvent()
{
    QString saveFileName = QFileDialog::getSaveFileName(this,"Save As","./cloud.pcd",tr("files(*.pcd )"));
    pcl::io::savePCDFile(saveFileName.toStdString(), *small_organized);
}

MainWindow::~MainWindow()
{
    delete ui;
}
//------Display points tab
void MainWindow::loadPCDFileForDispalyQueryPointsEvent(){
    QString qstr("/home/lilita/covis/covis-new/data/correspondence_test/feature_experiment/objects/");
    this->pcdFileQString = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)"));
    display_points_query_cloud.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile (pcdFileQString.toStdString(), *display_points_query_cloud);
    showPointCloud(ui->vtk_query, display_points_query_cloud);
}

void MainWindow::loadPCDFileForDispalyTargetPointsEvent(){
    QString qstr("/home/lilita/covis/covis-new/data/correspondence_test/feature_experiment/scenes/");
    this->pcdFileQString = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)"));
    display_points_target_cloud.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile (pcdFileQString.toStdString(), *display_points_target_cloud);
    showPointCloud(ui->vtk_target, display_points_target_cloud);
}
void MainWindow::resetButtonClicked(){
    points_query.clear();
    points_target.clear();
    color_query.clear();
    color_target.clear();
    if (display_points_query_cloud->size() > 0) renderPointCloudWithSelectedPoints(ui->vtk_query, display_points_query_cloud, points_query, color_query, ui->sphere_radius->toPlainText().toDouble());
    if (display_points_target_cloud->size() > 0) renderPointCloudWithSelectedPoints(ui->vtk_target, display_points_target_cloud, points_target, color_target, ui->sphere_radius->toPlainText().toDouble());
}

void MainWindow::drawSelectedPointQuery(){
    int point = ui->point_index_query->toPlainText().toInt();
    points_query.push_back(point);
    pcl::PointXYZ c;
    c.x = ui->color_r->toPlainText().toDouble();
    c.y = ui->color_g->toPlainText().toDouble();
    c.z = ui->color_b->toPlainText().toDouble();
    color_query.push_back(c);
    if (display_points_query_cloud->size() > 0) renderPointCloudWithSelectedPoints(ui->vtk_query, display_points_query_cloud, points_query, color_query, ui->sphere_radius->toPlainText().toDouble());
}

void MainWindow::drawSelectedPointTarget(){
    int point = ui->point_index_target->toPlainText().toInt();
    points_target.push_back(point);
    pcl::PointXYZ c;
    c.x = ui->color_r->toPlainText().toDouble();
    c.y = ui->color_g->toPlainText().toDouble();
    c.z = ui->color_b->toPlainText().toDouble();
    color_target.push_back(c);
    if (display_points_target_cloud->size() > 0) renderPointCloudWithSelectedPoints(ui->vtk_target, display_points_target_cloud, points_target, color_target, ui->sphere_radius->toPlainText().toDouble());
}

void MainWindow::renderPointCloudWithSelectedPoints(QVTKWidget* widget, pcl::PointCloud<PointT>::Ptr pointCloud,
                                                    std::vector<int> points, std::vector<pcl::PointXYZ> colors, float radius)
{
    vtkSmartPointer<vtkRenderWindow> renderWindow;

    pcl::visualization::PCLVisualizer localPclVisor = pcl::visualization::PCLVisualizer("Display points",false);

    localPclVisor.addPointCloud(pointCloud);
    localPclVisor.setBackgroundColor(0,0,0);
    for (size_t i = 0; i < points.size(); i++){
        PointT p = (*pointCloud)[points[i]];
        QString n = QString::number(points[i]) + "_point";
        std::string name = n.toStdString();
        QString nn = QString::number(points[i]);
        std::string name_test = nn.toStdString();
        localPclVisor.addSphere (p, radius, colors[i].x, colors[i].y, colors[i].z, name);
        localPclVisor.addText(name_test, 0, 0 + i*20, colors[i].x, colors[i].y, colors[i].z, name_test);

        std::stringstream sphere_name;
        sphere_name << points[i] << "sphere";
        localPclVisor.addSphere<PointT>(p, rad_sphere, 1,1,1,sphere_name.str().c_str(), 0);
        localPclVisor.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.6, sphere_name.str().c_str());
    }
    renderWindow = localPclVisor.getRenderWindow();
    widget->SetRenderWindow(renderWindow);
    widget->show();
}

void MainWindow::loadTargetFile(){
    QString qstr("/home/lilita/covis/covis-new/build/");
    QString path = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)"));
    vector <string>  mv;
    fstream file;
    string line;
    file.open(path.toStdString().c_str());
    while (getline(file,line)){
        mv.push_back(line);
    }
    file.close();
    int i = mv.size();
    for (i= i-1; i >=0; i--) {
        int p = boost::lexical_cast<int>(mv[i]);
        bool contains = false;
        for(size_t pp = 0; pp < points_target.size(); pp++){
            if (points_target[pp] == p){
                contains = true;
                break;
            }
        }
        if (!contains) {
            //		    std::cout << p << std::endl;
            points_target.push_back(p);
            pcl::PointXYZ c;
            c.x = ui->color_r->toPlainText().toDouble();
            c.y = ui->color_g->toPlainText().toDouble();
            c.z = ui->color_b->toPlainText().toDouble();
            color_target.push_back(c);
        }
        mv.pop_back();
    }
    drawSelectedPointTarget();
}
//----end display points tab

void MainWindow::loadPCDFileEvent()
{
    QString qstr("/home/lilita/covis/covis-new/data/correspondence_test/");
    this->pcdFileQString = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)")); //""
    mainCloud.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile (pcdFileQString.toStdString(), *mainCloud);

    showPointCloud(ui->qvtkWidget, mainCloud);

    ui->amount_of_points->setVisible(true);
    ui->amount_of_points->setText(QString::number(mainCloud->width * mainCloud->height));

    float x_min3d = 10000.0f;
    float x_max3d = 0.0f;
    float y_min3d = 10000.0f;
    float y_max3d = 0.0f;
    float z_min3d = 10000.0f;
    float z_max3d = 0.0f;

    for(size_t i = 0; i < mainCloud->size(); i++){

        PointT p = (*mainCloud)[i];

        if (p.x < x_min3d) x_min3d = p.x;
        if (p.x > x_max3d) x_max3d = p.x;
        if (p.y < y_min3d) y_min3d = p.y;
        if (p.y > y_max3d) y_max3d = p.y;
        if (p.z < z_min3d) z_min3d = p.z;
        if (p.z > z_max3d) z_max3d = p.z;
    }



    ui->create_template_button->setVisible(true);
    ui->Templatesettings->setVisible(true);

    ui->x_2d_max->setPlainText(QString::number(mainCloud->width));
    ui->y_2d_max->setPlainText(QString::number(mainCloud->height));
    int zero = 0;
    ui->x_2d_min->setPlainText(QString::number(zero));
    ui->y_2d_min->setPlainText(QString::number(zero));


    ui->x_3d_min->setPlainText(QString::number(x_min3d));
    ui->y_3d_min->setPlainText(QString::number(y_min3d));

    ui->x_3d_max->setPlainText(QString::number(x_max3d));
    ui->y_3d_max->setPlainText(QString::number(y_max3d));

    ui->z_3d_min->setPlainText(QString::number(z_min3d));
    ui->z_3d_max->setPlainText(QString::number(z_max3d));
}


void MainWindow::loadRGBDImgsEvent()
{
    QString qstr("/home/lilita/covis/covis-new/data/correspondence_test/");
    this->rgbImageQString = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)")); //""
    this->depthImageQString = QFileDialog::getOpenFileName(this, tr("Open File"),rgbImageQString , tr("Files (*.*)"));
    //show selected images in gui
    QPixmap pixRGB = QPixmap(rgbImageQString);
    ui->rgbImageView->setPixmap(pixRGB.scaled(ui->rgbImageView->size(), Qt::KeepAspectRatio));
    ui->stereoGroupBox->setVisible(true);
}


void MainWindow::createTemplateEvent(){

    std::cout << "createTemplateEvent " << std::endl;
    small_organized.reset(new pcl::PointCloud<PointT>);
    cv::Mat mask;
    if (usingBinaryMap && ui->usingBinaryMask->isChecked()){
        mask = cv::imread(binaryMaskQString.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        /// Find contours
        findContours( mask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
        //		if (contours.size() == 1){
        vector<Point>  contours_poly;
        Rect boundRect;

        approxPolyDP( Mat(contours[0]), contours_poly, 3, true );
        boundRect = boundingRect( Mat(contours_poly) );

        ui->x_2d_min->setPlainText(QString::number(boundRect.x));
        ui->x_2d_max->setPlainText(QString::number(boundRect.x + boundRect.width));
        ui->y_2d_min->setPlainText(QString::number(boundRect.y));
        ui->y_2d_max->setPlainText(QString::number(boundRect.y + boundRect.height));

        //		} else {
        //			std::cout << "Found more contours then one!" << std::endl;
        //		}
    }

    int x_min = ui->x_2d_min->toPlainText().toInt();
    int x_max = ui->x_2d_max->toPlainText().toInt();
    int y_min = ui->y_2d_min->toPlainText().toInt();
    int y_max = ui->y_2d_max->toPlainText().toInt();

    double zmin = ui->z_3d_min->toPlainText().toDouble();
    double zmax = ui->z_3d_max->toPlainText().toDouble();

    double xmin = ui->x_3d_min->toPlainText().toDouble();
    double xmax = ui->x_3d_max->toPlainText().toDouble();

    double ymin = ui->y_3d_min->toPlainText().toDouble();
    double ymax = ui->y_3d_max->toPlainText().toDouble();
    //	small_organized->height = x_max - x_min;
    //	small_organized->width = y_max - y_min;
    small_organized->width = x_max - x_min;
    small_organized->height = y_max - y_min;
    small_organized->points.resize(small_organized->width * small_organized->height);
    std::cout << small_organized->height<< std::endl;
    std::cout << small_organized->width<< std::endl;
    for (unsigned int row = 0; row < small_organized->height; row++)
        for (unsigned int col = 0; col < small_organized->width; col++){
            (*small_organized)(col, row).x = std::numeric_limits<float>::quiet_NaN();
            (*small_organized)(col, row).y = std::numeric_limits<float>::quiet_NaN();
            (*small_organized)(col, row).z = std::numeric_limits<float>::quiet_NaN();
            (*small_organized)(col, row).r = std::numeric_limits<float>::quiet_NaN();
            (*small_organized)(col, row).g = std::numeric_limits<float>::quiet_NaN();
            (*small_organized)(col, row).b = std::numeric_limits<float>::quiet_NaN();
            //			PointT p = (*mainCloud)(row + x_min, col + y_min);
            PointT p = (*mainCloud)(col + x_min, row + y_min);

            if ( p.z >= zmin && p.z <= zmax && p.x >= xmin && p.x <= xmax && p.y >= ymin && p.y <= ymax )
                (*small_organized)(col, row) = p;
        }
    ui->amount_of_points_template->setText(QString::number(small_organized->width * small_organized->height));
    ui->amount_of_points_template->setVisible(true);
    pcl::io::savePCDFile("small_organized.pcd", *small_organized);
    small_organized.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile ("small_organized.pcd", *small_organized);
    showPointCloud(ui->qvtkWidget_template, small_organized);
    ui->save_template_button->setVisible(true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MainWindow::showPointCloud(QVTKWidget* widget, pcl::PointCloud<PointT>::Ptr pointCloud)
{
    vtkSmartPointer<vtkRenderWindow> renderWindow;

    pcl::visualization::PCLVisualizer localPclVisor = pcl::visualization::PCLVisualizer("pclVisorName",false);
    //localPclVisor.setCameraPosition(0,0,0,0,0,0,0,0,0);
    localPclVisor.addPointCloud(pointCloud);
    localPclVisor.setBackgroundColor(0,0,0);
    renderWindow = localPclVisor.getRenderWindow();
    widget->SetRenderWindow(renderWindow);
    widget->show();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MainWindow::createPCDfromRGBD()
{
    cv::Mat_<cv::Vec3b> rgb;
    try {
        cv::Mat tmp = cv::imread(rgbImageQString.toStdString(), -1);
        if(tmp.type() == CV_8UC3) {
            rgb = tmp;
        } else if(tmp.type() == CV_8UC1) {
            std::cout << "Image seems to be grayscale, converting to BGR..." << std::endl;
            cv::cvtColor(tmp, rgb, CV_GRAY2BGR);
        } else if(tmp.type() == CV_16UC1) {
            std::cout << "Image is 16-bit unsigned, normalizing to [0,255] and converting to BGR..." << std::endl;
            cv::Mat_<float> tmpf(tmp);
            cv::normalize(tmpf, tmpf, 255, 0, CV_MINMAX);
            cv::cvtColor(cv::Mat_<uchar>(tmpf), rgb, CV_GRAY2BGR);
        } else if(tmp.type() == CV_32FC1) {
            std::cout << "Image seems to be floating-point grayscale, assuming range [0,1] and converting to BGR..." << std::endl;
            cv::cvtColor(cv::Mat_<uchar>(255.0f * tmp), rgb, CV_GRAY2BGR);
        } else {
            throw std::runtime_error(std::string("Invalid color image format!"));
        }
    } catch (const cv::Exception& e) {
        std::cerr << "Failed to load color image!" << std::endl;
        std::cerr << e.what() << std::endl;
    }

    if (rgb.empty()) {
        std::cerr << "Color image empty!" << std::endl;
    }

    cv::Mat_<float> d;
    try {
        cv::Mat tmp = cv::imread(depthImageQString.toStdString(), -1);
        if(tmp.type() == CV_16UC1) {
            std::cout << "Depth image seems to be in [mm], scaling to [m]..." << std::endl;
            d = 0.001f * cv::Mat_<float>(tmp);

            double dmin, dmax;
            cv::minMaxLoc(tmp, &dmin, &dmax);
            std::cout << "Depth [min max]: [" << dmin << " " << dmax << "]" << std::endl;
        } else {
            throw std::runtime_error(std::string("Invalid depth image format!"));
        }
    } catch (const cv::Exception& e) {
        std::cerr << "Depth image empty!" << std::endl;
        std::cerr << e.what() << std::endl;
    }

    if (d.empty()) {
        std::cerr << "Failed to load depth image!" << std::endl;
    }
    float fx = 525.0f;
    float fy = 525.0f;
    float cx = 319.5f;
    float cy = 239.5f;

    pcl::PointCloud<PointTR>::Ptr cloud(new pcl::PointCloud<PointTR>());
    cloud->height = d.cols;
    cloud->width = d.rows;
    cloud->points.resize(cloud->height * cloud->width);
    for (size_t i = 0; i < cloud->points.size(); i++){
        (*cloud)[i].x = std::numeric_limits<float>::quiet_NaN();
        (*cloud)[i].y = std::numeric_limits<float>::quiet_NaN();
        (*cloud)[i].z = std::numeric_limits<float>::quiet_NaN();
    }

    float x_min3d = 10000.0f;
    float x_max3d = 0.0f;
    float y_min3d = 10000.0f;
    float y_max3d = 0.0f;
    float z_min3d = 10000.0f;
    float z_max3d = 0.0f;

    for(int r = 0; r < d.rows; ++r) {
        for(int c = 0; c < d.cols; ++c) {
            float drc = d[r][c];
            if(isnan(drc) || drc == 0.0f)
                continue;

            PointTR p;
            p.z = drc;
            p.x = (float(c)-cx) * drc / fx;
            p.y = (float(r)-cy) * drc / fy;

            if (p.x < x_min3d) x_min3d = p.x;
            if (p.x > x_max3d) x_max3d = p.x;
            if (p.y < y_min3d) y_min3d = p.y;
            if (p.y > y_max3d) y_max3d = p.y;
            if (p.z < z_min3d) z_min3d = p.z;
            if (p.z > z_max3d) z_max3d = p.z;

            const cv::Vec3b& rgbrc = rgb[r][c];
            p.r = rgbrc[2];
            p.g = rgbrc[1];
            p.b = rgbrc[0];

            (*cloud)(r, c) = p;
        }

    }

    pcl::io::savePCDFile("temp.pcd", *cloud);
    mainCloud.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile ("temp.pcd", *mainCloud);

    showPointCloud(ui->qvtkWidget, mainCloud);

    ui->amount_of_points->setVisible(true);
    ui->amount_of_points->setText(QString::number(mainCloud->width * mainCloud->height));

    ui->create_template_button->setVisible(true);
    ui->Templatesettings->setVisible(true);

    ui->x_2d_max->setPlainText(QString::number(cloud->width));
    ui->y_2d_max->setPlainText(QString::number(cloud->height));
    int zero = 0;
    ui->x_2d_min->setPlainText(QString::number(zero));
    ui->y_2d_min->setPlainText(QString::number(zero));


    ui->x_3d_min->setPlainText(QString::number(x_min3d));
    ui->y_3d_min->setPlainText(QString::number(y_min3d));

    ui->x_3d_max->setPlainText(QString::number(x_max3d));
    ui->y_3d_max->setPlainText(QString::number(y_max3d));

    ui->z_3d_min->setPlainText(QString::number(z_min3d));
    ui->z_3d_max->setPlainText(QString::number(z_max3d));

}
//*********************************************************************************************
//*********************************************************************************************
//--------------------------------------------------------------------
// DISPAY FEATURES TAB SECTION
void MainWindow::loadPCDFileEventDispayFeatures()
{
    QString qstr("covis/covis-data/datasets/icvs_2015/");
    this->pcdFileQString = QFileDialog::getOpenFileName(this, tr("Open File"), qstr, tr("Files (*.*)")); //""
    mainCloudDispayFeaturesTab.reset(new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile (pcdFileQString.toStdString(), *mainCloudDispayFeaturesTab);

    //showPointCloud(ui->vtk_dispay_features_tab, mainCloudDispayFeaturesTab);
    vis_original->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(mainCloudDispayFeaturesTab);
    vis_original->addPointCloud<PointT> (mainCloudDispayFeaturesTab, rgb, "color cloud");
    vis_original->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "color cloud");
    renderWindow_original = vis_original->getRenderWindow();
    ui->vtk_dispay_features_tab->SetRenderWindow(renderWindow_original);
    ui->vtk_dispay_features_tab->show();



    ui->groupBox_compute_edges_display_points->setVisible(true);

    ui->depth_display_features->setPlainText(QString::number(0.01));
    ui->low_canny_display_features->setPlainText(QString::number(75));
    ui->high_canny_display_features->setPlainText(QString::number(150));

}

// threshold edge points
pcl::PointCloud<covis::core::CategorisedEdge>::Ptr thresholdEdgePoints(pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge){

    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr out (new pcl::PointCloud<covis::core::CategorisedEdge>());

    typename pcl::search::Search<covis::core::CategorisedEdge>::Ptr s;
    s.reset(new pcl::search::KdTree<covis::core::CategorisedEdge>);
    s->setInputCloud(only_edge);

    std::vector<bool> edges_out(only_edge->size(), true);


    for (size_t p = 0; p < only_edge->size(); p++){
        if (edges_out[p] ) {
            covis::core::CategorisedEdge point = (*only_edge)[p];
            std::vector<int> idx;
            std::vector<float> distsq;
            s->radiusSearch(point, 0.005, idx, distsq);
            for (size_t i = 0; i < idx.size(); i++) {
                if (p != (size_t)idx[i])
                    edges_out[idx[i]] = false;
            }
        }
    }
    int index = 0;
    for (size_t i = 0; i < edges_out.size(); i++) {
        if (edges_out[i]) {
            covis::core::CategorisedEdge point = (*only_edge)[i];
            point.idx_edge = index;
            out->push_back(point);
            index++;
        }
    }
    return out;
}
//------DISPLAY EDGE NORMALS & GRADIENT
inline QImage  cvMatToQImage( const cv::Mat &inMat )
{
    switch ( inMat.type() )
    {
    // 8-bit, 4 channel
    case CV_8UC4:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

        return image;
    }

        // 8-bit, 3 channel
    case CV_8UC3:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

        return image.rgbSwapped();
    }

        // 8-bit, 1 channel
    case CV_8UC1:
    {
        static QVector<QRgb>  sColorTable;

        // only create our color table once
        if ( sColorTable.isEmpty() )
        {
            for ( int i = 0; i < 256; ++i )
                sColorTable.push_back( qRgb( i, i, i ) );
        }

        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

        image.setColorTable( sColorTable );

        return image;
    }

    default:
        std::cout << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << std::endl;
        break;
    }

    return QImage();
}

inline QPixmap cvMatToQPixmap( const cv::Mat &inMat )
{
    return QPixmap::fromImage( cvMatToQImage( inMat ) );
}
//_----------------------------------------------------------------------------------
void MainWindow::displayEdgeNormals() {
    vis->removeAllPointClouds(0);
    vis->addPointCloudNormals<covis::core::CategorisedEdge, covis::core::CategorisedEdge> (edgeCloudDispayFeaturesTab, edgeCloudDispayFeaturesTab, 1, 0.01, "normals");
}

void MainWindow::displayEdgeGradient() {
    vis->removeAllPointClouds(0);

    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr cloud_normals (new pcl::PointCloud<covis::core::CategorisedEdge>());
    cloud_normals->width = edgeCloudDispayFeaturesTab->width;
    cloud_normals->height = edgeCloudDispayFeaturesTab->height;
    cloud_normals->points.resize(cloud_normals->height * cloud_normals->width);
    for (size_t e1 = 0; e1 < edgeCloudDispayFeaturesTab->size(); e1++) {
        (*cloud_normals)[e1].x = (*edgeCloudDispayFeaturesTab)[e1].x;
        (*cloud_normals)[e1].y = (*edgeCloudDispayFeaturesTab)[e1].y;
        (*cloud_normals)[e1].z = (*edgeCloudDispayFeaturesTab)[e1].z;
        (*cloud_normals)[e1].r = (*edgeCloudDispayFeaturesTab)[e1].r;
        (*cloud_normals)[e1].g = (*edgeCloudDispayFeaturesTab)[e1].g;
        (*cloud_normals)[e1].b = (*edgeCloudDispayFeaturesTab)[e1].b;
        (*cloud_normals)[e1].normal_x = 0;
        (*cloud_normals)[e1].normal_y = 0;
        (*cloud_normals)[e1].normal_z = 0;
    }



    for(size_t i = 0; i < edgeCloudDispayFeaturesTab->size(); i++) {
        //get the current point
        const covis::core::CategorisedEdge& pi = (*edgeCloudDispayFeaturesTab)[i];
        Eigen::Vector3f _point, _normal, g1;
        _point[0] = pi.dx;
        _point[1] = pi.dy;
        _point[2] = 0;

        _normal[0] =  pi.normal_x;
        _normal[1] =  pi.normal_y;
        _normal[2] =  pi.normal_z;

        if (!ui->display_edge_direction_checkBox_Display_features->isChecked()) g1 = _normal.cross(_point.cross(_normal));
        else g1 = _normal.cross(_point);
        g1.normalize();

        (*cloud_normals)[i].normal_x = g1[0];
        (*cloud_normals)[i].normal_y = g1[1];
        (*cloud_normals)[i].normal_z = g1[2];

    }

    vis->addPointCloudNormals<covis::core::CategorisedEdge, covis::core::CategorisedEdge> (cloud_normals, cloud_normals, 1, 0.01, "edge_gradient");
}


//---------------------------------------

void MainWindow::computeEdgesDispayFeatures() {
    ui->display_display_features->setVisible(true);
    vis->removeAllPointClouds(0);



    covis::feature::PCLEdgeExtraction<PointT> pclee;
    int boundary = 0, occluding = 0, occluded = 0, highcurvature = 0, canny = 0;
    bool _istemplate = false;

    //get value from GUI
    if (ui->boundary_checkBox_display_features->isChecked()) boundary = 1;
    if (ui->occluding_checkBox_display_features->isChecked()) occluding = 2;
    if (ui->occluded_checkBox_display_features->isChecked()) occluded = 4;
    if (ui->highcurvature_checkBox_display_features->isChecked()) highcurvature = 8;
    if (ui->canny_checkBox_display_features->isChecked()) canny = 16;
    if (ui->isTemplate_checkBox_display_features->isChecked()) _istemplate = true;

    float depth_val = ui->depth_display_features->toPlainText().toDouble();
    int _low_canny = ui->low_canny_display_features->toPlainText().toInt();
    int _high_canny = ui->high_canny_display_features->toPlainText().toInt();

    pclee =  covis::feature::PCLEdgeExtraction<PointT>(depth_val, 50, _low_canny, _high_canny, boundary | occluding | occluded | highcurvature | canny, _istemplate);

    std::vector<pcl::PointIndices> label_indices;
    pcl::PointIndices::Ptr edge_indices (new pcl::PointIndices);

    edgeCloudDispayFeaturesTab.reset(new pcl::PointCloud<covis::core::CategorisedEdge>);
    pclee.extractPCLEdges(mainCloudDispayFeaturesTab, label_indices, edge, edgeCloudDispayFeaturesTab, edge_indices);

    //display amount of points
    ui->boundary_size_display_features->setText(QString::number(label_indices[0].indices.size()));
    ui->occluding_size_display_features->setText(QString::number(label_indices[1].indices.size()));
    ui->occluded_size_display_features->setText(QString::number(label_indices[2].indices.size()));
    ui->curvature_size_display_features->setText(QString::number(label_indices[3].indices.size()));
    ui->canny_size_display_features->setText(QString::number(label_indices[4].indices.size()));

    ui->edge_size_display_features->setText(QString::number(edgeCloudDispayFeaturesTab->size()));

    if (ui->threshold_checkBox_display_features->isChecked()) {
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_thresholded = thresholdEdgePoints(edgeCloudDispayFeaturesTab);
        pcl::console::print_warn("After thresholding : %d\n", only_edge_thresholded->size());
        edgeCloudDispayFeaturesTab = only_edge_thresholded;
    }


    ui->dowsampled_edge_size_display_features->setText(QString::number(edgeCloudDispayFeaturesTab->size()));

    vis->setBackgroundColor (0, 0, 0);

    pcl::visualization::PointCloudColorHandlerRGBField<covis::core::CategorisedEdge> rgb(edgeCloudDispayFeaturesTab);


    vis->addPointCloud<covis::core::CategorisedEdge> (edgeCloudDispayFeaturesTab, rgb, "sample cloud");
    vis->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    renderWindow = vis->getRenderWindow();
    ui->vtk_dispay_features_tab_edges->SetRenderWindow(renderWindow);
    ui->vtk_dispay_features_tab_edges->show();
}


void drawHorizontalLine(cv::Mat img, cv::Point a){

    cv::Point p(0,a.y);
    cv::Point q(img.cols, a.y);

    cv::line(img, p, q, cv::Scalar(0, 255, 255),1,8,0);
    cv::Point p1(a.x, 0);
    cv::Point q1(a.x, img.rows);

    cv::line(img, p1, q1, cv::Scalar(0, 255, 255),1,8,0);
}

std::vector<cv::Point3d> findPoints(cv::Point idx_2d, float rad){

    std::vector<cv::Point3d> points_out;

    typename pcl::search::Search<covis::core::CategorisedEdge>::Ptr s;
    s.reset(new pcl::search::KdTree<covis::core::CategorisedEdge>);
    s->setInputCloud(edge);

    for (size_t p = 0; p < edge->size(); p++){
        covis::core::CategorisedEdge point = (*edge)[p];
        if (point.x_2D != idx_2d.x || point.y_2D != idx_2d.y) continue;

        std::vector<int> idx;
        std::vector<float> distsq;
        s->radiusSearch(point, rad, idx, distsq);
        for (size_t i = 0; i < idx.size(); i++){
            covis::core::CategorisedEdge n = (*edge)[idx[i]];
            cv::Point3d data;
            data.x = n.x_2D;
            data.y = n.y_2D;
            data.z = n.idx_original;
            points_out.push_back(data);
        }
    }
    return points_out;
}

void MainWindow::drawSpheres(std::vector<int> idx_original, bool left){
    cv::Point3f leftcol (1,1,0);
    cv::Point3f rightcol (1,0,0);
    cv::Point3d col;
    if (left) col = leftcol;
    else col = rightcol;

    for (size_t i = 0; i < idx_original.size(); i++){
        PointT p = (*mainCloudDispayFeaturesTab)[idx_original[i]];
        std::stringstream point_name;
        point_name << idx_original[i]<< "neighb point";
        vis_original->addSphere<PointT>(p, 0.0005, col.x, col.y, col.z,point_name.str().c_str(), 0);
        vis_original->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.4, point_name.str().c_str());
    }
}

void MainWindow::processColor(){

    //show rgb image
    ConvertInput<PointT> ci;
    cv::Mat rgb_image = ci.getRgbImageFromPointCloud(mainCloudDispayFeaturesTab);
    float rad = 40;

    for (size_t i = 0; i < points_display_features.size(); i++){
        covis::core::CategorisedEdge point = (*edgeCloudDispayFeaturesTab)[points_display_features[i]];
        std::cout << "r: " << point.r << " g: " << point.g << ", b: " << point.b << std::endl;
        //draw edge direction
        std::cout << i << " :: " << point.idx_original << ", " << point.idx_edge << std::endl;
        float result = cv::fastAtan2(point.dy, point.dx);
        ui->color_info_display_features_1->setText("theta: " + QString::number(result));
        std::cout << "theta before: " << result << std::endl;

        if (result > 90) result -= 90;
        else result += 90;
        std::cout << "theta after: "<< result << std::endl;
        ui->color_info_display_features_2->setText("theta ||: " + QString::number(result));
        float slop = tan(result *  M_PI/ 180);
        ui->color_info_display_features_3->setText("slope: " + QString::number(slop));
        std::cout << "slope: " << slop << std::endl;

        cv::circle(rgb_image, cv::Point(point.x_2D, point.y_2D), rad, cv::Scalar(0, 255, 0), 2, 1, 0);

        //b = y - kx, -y because axis goes down
        float b = (-point.y_2D) - slop * point.x_2D;
        std::cout << "b: " << b << std::endl;

        //process theta
        cv::Point _crossing_y_0, _crossing_y_rows, _crossing_x_0, _crossing_x_cols;

        _crossing_y_0.x = (int)((- b) / slop);
        _crossing_y_0.y = 0;

        _crossing_y_rows.y = -rgb_image.rows;
        _crossing_y_rows.x = (int)((_crossing_y_rows.y - b) / slop);

        _crossing_x_0.y = (int)(b);
        _crossing_x_0.x = 0;

        _crossing_x_cols.x = rgb_image.cols;
        _crossing_x_cols.y = (int)(slop * _crossing_x_cols.x + b);

        std::cout << "rgb_image.cols : " << rgb_image.cols << ", rgb_image.rows : " << rgb_image.rows << std::endl;
        std::cout << "_crossing_y_0.x "  << _crossing_y_0.x << ", _crossing_y_0.y " << _crossing_y_0.y << std::endl;
        std::cout << "_crossing_y_rows.x "  << _crossing_y_rows.x << ", _crossing_y_rows.y " << _crossing_y_rows.y << std::endl;
        std::cout << "_crossing_x_0.x "  << _crossing_x_0.x << ", _crossing_x_0.y " << _crossing_x_0.y << std::endl;
        std::cout << "_crossing_x_cols.x "  << _crossing_x_cols.x << ", _crossing_x_cols.y " << _crossing_x_cols.y << std::endl;

        bool cross_y_0 = false, cross_y_rows = false, cross_x_0 = false, cross_x_cols = false;
        if (_crossing_y_0.x <= rgb_image.cols && _crossing_y_0.x >= 0) cross_y_0 = true;

        if (_crossing_y_rows.x <= rgb_image.cols && _crossing_y_rows.x >= 0) cross_y_rows = true;

        if (_crossing_x_0.y >= -rgb_image.rows && _crossing_x_0.y <= 0) cross_x_0 = true;

        if (_crossing_x_cols.y >= -rgb_image.rows && _crossing_x_cols.y <= 0) cross_x_cols = true;


        std::cout << "cross_y_0: " << cross_y_0 << ", cross_y_rows: " << cross_y_rows << ", cross_x_0: " << cross_x_0 << ", cross_x_cols: " << cross_x_cols << std::endl;
        cv::Point start, end; bool found_start = false;
        if (cross_y_0) {
            start = _crossing_y_0;
            found_start = true;
        }
        if (cross_y_rows && !found_start) {
            start = _crossing_y_rows;
            found_start = true;
        } else if (cross_y_rows && found_start) {
            end = _crossing_y_rows;
        }
        if (cross_x_0 && !found_start) {
            start = _crossing_x_0;
            found_start = true;
        } else if (cross_x_0 && found_start) {
            end = _crossing_x_0;
        }
        if (cross_x_cols && !found_start) {
            start = _crossing_x_cols;
            found_start = true;
        } else if (cross_x_cols && found_start) {
            end = _crossing_x_cols;
        }

        if (start.y < 0) start.y = -start.y;
        if (end.y < 0) end.y = -end.y;
        std::cout << "start: " << start << " end: " << end << std::endl;
        cv::line(rgb_image, start, end, cv::Scalar(0, 0, 255), 1, 8, 0);

        drawHorizontalLine(rgb_image, cv::Point(point.x_2D, point.y_2D));

        //for that point show all left/right color neighborhs

        std::vector<cv::Point3d> all_neighborhs = findPoints(cv::Point(point.x_2D, point.y_2D), rad_sphere);
        std::cout << "neigh size: " << all_neighborhs.size() << std::endl;

        //for each neighborh find on which size of the line it is...
        std::vector<int> onLeftSide, onRightSide, onLine;
        for (size_t n = 0; n < all_neighborhs.size(); n++){
            cv::Point3d current = all_neighborhs[n];
            int position = (end.x-start.x)*(current.y-start.y) - (end.y-start.y)*(current.x-start.x);
            if (position > 0) onLeftSide.push_back(current.z);
            else if (position < 0) onRightSide.push_back(current.z);
            else onLine.push_back(current.z);
        }
        pcl::console::print_value("On leftSide: %d, on rightSize: %d, on Line: %d\n", onLeftSide.size(), onRightSide.size(), onLine.size());
        drawSpheres(onLeftSide, true);
        drawSpheres(onRightSide, false);
    }

    QPixmap pixRGB = cvMatToQPixmap(rgb_image);
    ui->rgbImage_display_features->setPixmap(pixRGB.scaled(ui->rgbImage_display_features->size(), Qt::KeepAspectRatio));
}

void MainWindow::renderPointCloudWithSelectedPointsEdgeCloud()
{
    vis->removeAllShapes(0);

    for (size_t i = 0; i < points_display_features.size(); i++){
        covis::core::CategorisedEdge p = (*edgeCloudDispayFeaturesTab)[points_display_features[i]];
        QString n = QString::number(points_display_features[i]) + "_point";
        std::string name = n.toStdString();
        QString nn = QString::number(points_display_features[i]);
        std::string name_test = nn.toStdString();
        vis->addSphere (p, ui->sphere_radius_display_features->toPlainText().toDouble(), color_display_features[i].x, color_display_features[i].y, color_display_features[i].z, name);
        vis->addText(name_test, 20, 0 + i*20, color_display_features[i].x, color_display_features[i].y, color_display_features[i].z, name_test);

        std::stringstream sphere_name;
        sphere_name << p.idx_edge << "sphere";
        vis->addSphere<covis::core::CategorisedEdge>(p, rad_sphere, 1,1,1,sphere_name.str().c_str(), 0);
        vis->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.2, sphere_name.str().c_str());
    }
}

void MainWindow::renderPointCloudWithSelectedPointsOriginalCloud()
{
    vis_original->removeAllShapes(0);

    for (size_t i = 0; i < points_display_features_real_idx.size(); i++){
        PointT p = (*mainCloudDispayFeaturesTab)[points_display_features_real_idx[i]];
        QString n = QString::number(points_display_features_real_idx[i]) + "_point";
        std::string name = n.toStdString();
        QString nn = QString::number(points_display_features_real_idx[i]);
        std::string name_test = nn.toStdString();
        vis_original->addSphere (p, ui->sphere_radius_display_features->toPlainText().toDouble(), color_display_features[i].x, color_display_features[i].y, color_display_features[i].z, name);
        vis_original->addText(name_test, 20, 0 + i*20, color_display_features[i].x, color_display_features[i].y, color_display_features[i].z, name_test);

        std::stringstream sphere_name;
        sphere_name << points_display_features_real_idx[i]<< "sphere";
        vis_original->addSphere<PointT>(p, rad_sphere, 1,1,1,sphere_name.str().c_str(), 0);
        vis_original->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.2, sphere_name.str().c_str());
    }
}



void MainWindow::drawSelectedPointDisplayFeaturesTab(){
    int point = ui->point_index_query_display_features->toPlainText().toInt();
    //find point with edge index of entry
    for (size_t i = 0; i < edgeCloudDispayFeaturesTab->size(); i++){
        covis::core::CategorisedEdge point_x = (*edgeCloudDispayFeaturesTab)[i];
        if (point_x.idx_edge != point) continue;
        points_display_features.push_back(i);
        points_display_features_real_idx.push_back(point_x.idx_original);
        points_display_features_edge_idx.push_back(point_x.idx_edge);
    }

    pcl::PointXYZ c;
    c.x = ui->color_r_display_features->toPlainText().toDouble();
    c.y = ui->color_g_display_features->toPlainText().toDouble();
    c.z = ui->color_b_display_features->toPlainText().toDouble();
    color_display_features.push_back(c);
    /*if (mainCloudDispayFeaturesTab->size() > 0)
        renderPointCloudWithSelectedPoints(ui->vtk_dispay_features_tab, mainCloudDispayFeaturesTab, points_display_features_real_idx, color_display_features, ui->sphere_radius_display_features->toPlainText().toDouble());*/

    renderPointCloudWithSelectedPointsOriginalCloud();
    renderPointCloudWithSelectedPointsEdgeCloud();
}

void MainWindow::resetDisplayFeatures(){
    points_display_features.clear();
    color_display_features.clear();
    points_display_features_real_idx.clear();
    points_display_features_edge_idx.clear();
}
