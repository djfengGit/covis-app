/*
 *  \author    Guðmundur Geir Gunnarsson
 *  \date      2015
 *  \copyright BSD 3-clause
 */

#include <iostream>
#include <cmath>

#include <QtGui>
#include <QFileDialog>
#include <QMessageBox>
#include <QComboBox>

#include "stereo_gui.h"

StereoWindow::StereoWindow(QMainWindow *parent) : QMainWindow(parent) {
  _ui.setupUi(this);

  // Variables
  _images_loaded = false;
  _intrinsic_loaded = false;
  _extrinsic_loaded = false;
  _use_rectified = false;
  _last_dir = QString();

  //_ui.leftImage->setScaledContents(true);
  _left_scene = new QGraphicsScene();
  _right_scene = new QGraphicsScene();
  _disp_scene = new QGraphicsScene();
  _ui.leftImage->setScene(_left_scene);
  _ui.rightImage->setScene(_right_scene);
  _ui.dispImage->setScene(_disp_scene);

  // Setup Buttons
  connect(_ui.loadImages, SIGNAL(pressed()), this, SLOT(onLoadImagePressed()));
  connect(_ui.loadIntrinsic, SIGNAL(pressed()), this, SLOT(onLoadIntrinsicPressed()));
  connect(_ui.loadExtrinsic, SIGNAL(pressed()), this, SLOT(onLoadExtrinsicPressed()));
  connect(_ui.rectifyCB, SIGNAL(stateChanged(int)), this, SLOT(onRectifiedChecked()));

  connect(_ui.stereoTab, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged()));

  // BM Controls
  connect(_ui.bmMinDispSlider         , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmMaxDispSlider         , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmBlockSizeSlider       , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmSpeckleWindowSlider   , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmSpeckleRangeSlider    , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmTextureThresholdSlider, SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmUniquenessRatioSlider , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmPreFilterCapSlider    , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));
  connect(_ui.bmPreFilterSizeSlider   , SIGNAL(valueChanged(int)), this, SLOT(onChangeBMParameters()));

  // SGBM Controls
  connect(_ui.sgbmUseDynamicProgrammingCB, SIGNAL(stateChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmMinDispSlider          , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmMaxDispSlider          , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmBlockSizeSlider        , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmSpeckleWindowSlider    , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmSpeckleRangeSlider     , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmUniquenessRatioSlider  , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));
  connect(_ui.sgbmPreFilterCapSlider     , SIGNAL(valueChanged(int)), this, SLOT(onChangeSGBMParameters()));

}

void StereoWindow::resizeEvent(QResizeEvent* event) {
  if(_images_loaded) {
    displayImage(_left, _ui.leftImage, _left_scene);
    displayImage(_right, _ui.rightImage, _right_scene);
    if(!_disp.empty()) {
      displayDisparityImage();
    }
  }
}

void StereoWindow::onTabChanged() {
  QWidget* curr_widget = _ui.stereoTab->currentWidget();
  QString tab_name = curr_widget->objectName().toUtf8().constData();

  if(tab_name == "bmTab") {
    onChangeBMParameters();
  } else if (tab_name == "sgbmTab") {
    onChangeSGBMParameters();
  }

}

void StereoWindow::onLoadImagePressed() {
  QString filename_left, filename_right;
  filename_left = QFileDialog::getOpenFileName(this, tr("Open Left Image"), _last_dir, tr("Images (*.png *.xpm *.jpg)"));
  if(!filename_left.isEmpty()) {
    _last_dir = QFileInfo(filename_left).absoluteDir().absolutePath();
    filename_right = QFileDialog::getOpenFileName(this, tr("Open Right Image"), _last_dir, tr("Images (*.png *.xpm *.jpg)"));
    if(!filename_right.isEmpty()) {
    _last_dir = QFileInfo(filename_right).absoluteDir().absolutePath();
    loadImages(filename_left, filename_right);
    }
  }
}

void StereoWindow::onLoadIntrinsicPressed() {
  QString filename = QFileDialog::getOpenFileName(this, tr("Open Intrinsic Parameter File"), _last_dir, tr("Parameter Files (*.xml *.yml)"));
  if(!filename.isEmpty()) {
    _last_dir = QFileInfo(filename).absoluteDir().absolutePath();
    loadParameters(filename, 0);
  }
}

void StereoWindow::onLoadExtrinsicPressed() {
  QString filename = QFileDialog::getOpenFileName(this, tr("Open Extrinsic Parameter File"), _last_dir, tr("Parameter Files (*.xml *.yml)"));
  if(!filename.isEmpty()) {
    _last_dir = QFileInfo(filename).absoluteDir().absolutePath();
    loadParameters(filename, 1);
  }
}

void StereoWindow::onRectifiedChecked() {
  if( _intrinsic_loaded && _extrinsic_loaded ) {
    _use_rectified = _ui.rectifyCB->isChecked();
  } else {
    bool old_state = _ui.rectifyCB->blockSignals(true);
    _ui.rectifyCB->setCheckState(Qt::Unchecked);
    _use_rectified = false;
    QMessageBox::warning(this, "Error", "Can't use rectified images without calibration");
    _ui.rectifyCB->blockSignals(old_state);
  }
}

void StereoWindow::loadImages(QString filename_left, QString filename_right) {
  _left = cv::imread(filename_left.toUtf8().constData(), CV_LOAD_IMAGE_COLOR).getUMat(cv::ACCESS_RW);
  _right = cv::imread(filename_right.toUtf8().constData(), CV_LOAD_IMAGE_COLOR).getUMat(cv::ACCESS_RW);

  if( _left.empty() ) {
    QMessageBox::warning(this, "Error", "Unable to read file " + filename_left + " as an image!");
  } else if( _right.empty() ) {
    QMessageBox::warning(this, "Error", "Unable to read file " + filename_right + " as an image!");
  }  else {
    _images_loaded = true;

    undistortRectify();

    displayImage(_left, _ui.leftImage, _left_scene);
    displayImage(_right, _ui.rightImage, _right_scene);
  }
}

void StereoWindow::loadParameters(QString filename, int param_set) {
  try {
    cv::FileStorage fs(filename.toUtf8().constData(), CV_STORAGE_READ);
    if( fs.isOpened() ) {
      if(param_set == 0) {
     	// Load intrinsic parameters
	cv::Mat M1_tmp, D1_tmp, M2_tmp, D2_tmp;
	fs["M1"] >> M1_tmp;
	fs["D1"] >> D1_tmp;
	fs["M2"] >> M2_tmp;
	fs["D2"] >> D2_tmp;
	if(M1_tmp.empty() || D1_tmp.empty() || M2_tmp.empty() || D2_tmp.empty()) {
	  QMessageBox::warning(this, "Error", "Not a valid intrinsic parameter file");
	} else {
	  _M1 = M1_tmp.getUMat(cv::ACCESS_RW);
	  _D1 = D1_tmp.getUMat(cv::ACCESS_RW);
	  _M2 = M2_tmp.getUMat(cv::ACCESS_RW);
	  _D2 = D2_tmp.getUMat(cv::ACCESS_RW);
	  _intrinsic_loaded = true;
	}
      } else if (param_set == 1) {
	// Load extrinsic parameters
	cv::Mat R_tmp, T_tmp;
	fs["R"] >> R_tmp;
	fs["T"] >> T_tmp;
	if(R_tmp.empty() || T_tmp.empty()) {
	  QMessageBox::warning(this, "Error", "Not a valid extrinsic parameter file");
	} else {
	  _R = R_tmp.getUMat(cv::ACCESS_RW);
	  _T = T_tmp.getUMat(cv::ACCESS_RW);
	  _extrinsic_loaded = true;
 	}
      }
    } else {
      QMessageBox::warning(this, "Error", "Unable to open parameter file!");
    }
    undistortRectify();
  } catch(...) {
    QMessageBox::warning(this, "Error", "Malformed parameter file!");
    _intrinsic_loaded = false;
    _extrinsic_loaded = false;
    _ui.rectifyCB->setCheckState(Qt::Unchecked);
  }
}

void StereoWindow::undistortRectify() {
  if(_images_loaded && _intrinsic_loaded && _extrinsic_loaded) {
    cv::Size imgSize = _left.size();
    cv::stereoRectify(_M1, _D1, _M2,  _D2, imgSize, _R, _T, _R1, _R2, _P1, _P2, _Q, cv::CALIB_ZERO_DISPARITY, 0, imgSize, &_roi1, &_roi2 );
    cv::Mat map1_1, map2_1, map1_2, map2_2;
    cv::initUndistortRectifyMap(_M1, _D1, _R1, _M1_rec, _left.size(), CV_32FC1, map1_1, map2_1);
    cv::initUndistortRectifyMap(_M2, _D2, _R2, _M2_rec, _right.size(), CV_32FC1, map1_2, map2_2);
    cv::remap(_left, _left_rect, map1_1, map2_1, cv::INTER_LINEAR);
    cv::remap(_right, _right_rect, map1_2, map2_2, cv::INTER_LINEAR);
  }
}

void StereoWindow::onChangeBMParameters() {
  // Check for legal values
  makeLegalBMParameterValues();

  updateBMSpinBoxes();

  if(!_images_loaded) {
    return;
  }

  cv::Ptr<cv::StereoBM> bm = cv::StereoBM::create(_ui.bmMaxDispSlider->value(), _ui.bmBlockSizeSlider->value());

  bm->setPreFilterCap(_ui.bmPreFilterCapSlider->value());
  bm->setPreFilterSize(_ui.bmPreFilterSizeSlider->value());
  bm->setMinDisparity(_ui.bmMinDispSlider->value());
  bm->setTextureThreshold(_ui.bmTextureThresholdSlider->value());
  bm->setSpeckleWindowSize(_ui.bmSpeckleWindowSlider->value());
  bm->setSpeckleRange(_ui.bmSpeckleRangeSlider->value());
  bm->setDisp12MaxDiff(1);
  bm->setUniquenessRatio(_ui.bmUniquenessRatioSlider->value());

  cv::UMat left_gray, right_gray;

  if(_use_rectified) {
    bm->setROI1(_roi1);
    bm->setROI2(_roi2);
    cv::cvtColor(_left_rect, left_gray, CV_RGB2GRAY);
    cv::cvtColor(_right_rect, right_gray, CV_RGB2GRAY);
    bm->compute(left_gray, right_gray, _disp);
  } else {
    bm->setROI1(cv::Rect());
    bm->setROI2(cv::Rect());
    cv::cvtColor(_left, left_gray, CV_RGB2GRAY);
    cv::cvtColor(_right, right_gray, CV_RGB2GRAY);
    bm->compute(left_gray, right_gray, _disp);
  }

  displayDisparityImage();
}

void StereoWindow::makeLegalBMParameterValues() {
  blockSignals(true);
  // If values are not legal, round them to the nearest legal value.
  // Max disparity mod 16
  if((_ui.bmMaxDispSlider->value() % 16) != 0) {
   _ui.bmMaxDispSlider->setValue(round(_ui.bmMaxDispSlider->value() / 16)*16);
  }

  // Block Size Odd
  if((_ui.bmBlockSizeSlider->value() % 2) == 0) {
    _ui.bmBlockSizeSlider->setValue(_ui.bmBlockSizeSlider->value() - 1);
  }

  // Speckle Window Size Odd
  if((_ui.bmSpeckleWindowSlider->value() % 2) == 0) {
    _ui.bmSpeckleWindowSlider->setValue(_ui.bmSpeckleWindowSlider->value() - 1);
  }

  // Pre-filter Size odd
  if((_ui.bmPreFilterSizeSlider->value() % 2) == 0) {
    _ui.bmPreFilterSizeSlider->setValue(_ui.bmPreFilterSizeSlider->value() - 1);
  }

  blockSignals(false);
}

void StereoWindow::updateBMSpinBoxes() {
  _ui.bmMinDispSpin->setValue(_ui.bmMinDispSlider->value());
  _ui.bmMaxDispSpin->setValue(_ui.bmMaxDispSlider->value());
  _ui.bmBlockSizeSpin->setValue(_ui.bmBlockSizeSlider->value());
  _ui.bmSpeckleWindowSpin->setValue(_ui.bmSpeckleWindowSlider->value());
  _ui.bmSpeckleRangeSpin->setValue(_ui.bmSpeckleRangeSlider->value());
  _ui.bmTextureThresholdSpin->setValue(_ui.bmTextureThresholdSlider->value());
  _ui.bmUniquenessRatioSpin->setValue(_ui.bmUniquenessRatioSlider->value());
  _ui.bmPreFilterCapSpin->setValue(_ui.bmPreFilterCapSlider->value());
  _ui.bmPreFilterSizeSpin->setValue(_ui.bmPreFilterSizeSlider->value());

}

void StereoWindow::onChangeSGBMParameters() {
  makeLegalSGBMParameterValues();

  updateSGBMSpinBoxes();

  if(!_images_loaded) {
    return;
  }
  cv::UMat left_gray, right_gray;
  cv::Ptr<cv::StereoSGBM> sgbm = cv::StereoSGBM::create(_ui.sgbmMinDispSlider->value(),
							_ui.sgbmMaxDispSlider->value(),
							_ui.sgbmBlockSizeSlider->value(),
							8*3*_ui.sgbmBlockSizeSlider->value()*_ui.sgbmBlockSizeSlider->value(),
							32*3*_ui.sgbmBlockSizeSlider->value()*_ui.sgbmBlockSizeSlider->value(),
							1,
							_ui.sgbmPreFilterCapSlider->value(),
							_ui.sgbmUniquenessRatioSlider->value(),
							_ui.sgbmSpeckleWindowSlider->value(),
							_ui.sgbmSpeckleRangeSlider->value(),
							_ui.sgbmUseDynamicProgrammingCB->isChecked()
							? cv::StereoSGBM::MODE_HH
							: cv::StereoSGBM::MODE_SGBM );

  if(_use_rectified) {
    cv::cvtColor(_left_rect, left_gray, CV_RGB2GRAY);
    cv::cvtColor(_right_rect, right_gray, CV_RGB2GRAY);
    sgbm->compute(left_gray, right_gray, _disp);
  } else {
    cv::cvtColor(_left, left_gray, CV_RGB2GRAY);
    cv::cvtColor(_right, right_gray, CV_RGB2GRAY);
    sgbm->compute(left_gray, right_gray, _disp);
  }

  displayDisparityImage();
}

void StereoWindow::makeLegalSGBMParameterValues() {
  blockSignals(true);

   // If values are not legal, round them to the nearest legal value.
  // Max disparity mod 16
  if((_ui.sgbmMaxDispSlider->value() % 16) != 0) {
   _ui.sgbmMaxDispSlider->setValue(round(_ui.sgbmMaxDispSlider->value() / 16)*16);
  }

  // Block Size Odd
  if((_ui.sgbmBlockSizeSlider->value() % 2) == 0) {
    _ui.sgbmBlockSizeSlider->setValue(_ui.sgbmBlockSizeSlider->value() - 1);
  }

  // Speckle Window Size Odd
  if((_ui.sgbmSpeckleWindowSlider->value() % 2) == 0) {
    _ui.sgbmSpeckleWindowSlider->setValue(_ui.sgbmSpeckleWindowSlider->value() - 1);
  }

  blockSignals(false);
}

void StereoWindow::updateSGBMSpinBoxes() {
  _ui.sgbmMinDispSpin->setValue(_ui.sgbmMinDispSlider->value());
  _ui.sgbmMaxDispSpin->setValue(_ui.sgbmMaxDispSlider->value());
  _ui.sgbmBlockSizeSpin->setValue(_ui.sgbmBlockSizeSlider->value());
  _ui.sgbmSpeckleWindowSpin->setValue(_ui.sgbmSpeckleWindowSlider->value());
  _ui.sgbmSpeckleRangeSpin->setValue(_ui.sgbmSpeckleRangeSlider->value());
  _ui.sgbmUniquenessRatioSpin->setValue(_ui.sgbmUniquenessRatioSlider->value());
  _ui.sgbmPreFilterCapSpin->setValue(_ui.sgbmPreFilterCapSlider->value());

}

void StereoWindow::displayDisparityImage() {
  cv::UMat disp_norm, disp_norm_color;
  cv::normalize(_disp, disp_norm, 0, 255, cv::NORM_MINMAX, CV_8UC1);
  cv::cvtColor(disp_norm, disp_norm_color, CV_GRAY2RGB);
  displayImage(disp_norm_color, _ui.dispImage, _disp_scene);

}

void StereoWindow::displayImage(cv::UMat image, QGraphicsView* dst_view, QGraphicsScene* dst_scene) {
  cv::Mat display_image = image.getMat(cv::ACCESS_READ);
  QSize size = dst_view->size();

  QImage imageQt = QImage((uchar*) display_image.data,
			  display_image.cols,
			  display_image.rows,
			  display_image.step,
			  QImage::Format_RGB888);

  QImage imageQtScaled = imageQt.scaled(_ui.rightImage->size(), Qt::KeepAspectRatio);

  dst_scene->clear();
  dst_scene->addPixmap(QPixmap::fromImage(imageQtScaled));
  dst_view->setSceneRect(0, 0, imageQtScaled.width(), imageQtScaled.height());
  dst_view->show();
}
