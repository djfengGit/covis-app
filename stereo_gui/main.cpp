/*
 *  \author    Guðmundur Geir Gunnarsson
 *  \date      2015
 *  \copyright BSD 3-clause
 */

#include <QApplication>
#include "stereo_gui.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  StereoWindow stereoGui;
  stereoGui.show();
  return app.exec();
}
