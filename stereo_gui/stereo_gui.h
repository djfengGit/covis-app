/*
 *  \author    Guðmundur Geir Gunnarsson
 *  \date      2015
 *  \copyright BSD 3-clause
 */

#ifndef STEREO_GUI_H
#define STEREO_GUI_H

#include "ui_stereo_gui.h"
//#include <covis/covis.h>
#include <opencv2/opencv.hpp>

class StereoWindow: public QMainWindow {
  Q_OBJECT

  public:
    StereoWindow(QMainWindow *parent = 0);
    void loadImages(QString filename_left, QString filename_right);
    void loadParameters(QString filename, int param_set);
    
  private:
    void displayImage(cv::UMat image, QGraphicsView* dst_view, QGraphicsScene* dst_scene);
    void displayDisparityImage();
    void undistortRectify();
    void makeLegalBMParameterValues();
    void makeLegalSGBMParameterValues();
    void updateBMSpinBoxes();
    void updateSGBMSpinBoxes();
    void resizeEvent(QResizeEvent * event);

  private slots:
    void onTabChanged();
    void onLoadImagePressed();
    void onLoadIntrinsicPressed();
    void onLoadExtrinsicPressed();
    void onRectifiedChecked();
    void onChangeBMParameters();
    void onChangeSGBMParameters();
    
  signals:
    
  private:
    Ui::MainWindow _ui;
    cv::UMat _left;
    cv::UMat _left_rect;
    cv::UMat _right;
    cv::UMat _right_rect;
    cv::UMat _disp;    
    cv::Rect _roi1, _roi2;
    
    cv::UMat _M1, _D1, _M2, _D2, _R, _T; // Parameter matrices
    cv::UMat _R1, _R2, _P1, _P2, _Q; // Rectification matrices
    cv::UMat _M1_rec, _M2_rec;

    bool _images_loaded;
    bool _extrinsic_loaded;
    bool _intrinsic_loaded;
    bool _use_rectified;
    QString _last_dir;
    QGraphicsScene* _left_scene;
    QGraphicsScene* _right_scene;
    QGraphicsScene* _disp_scene;
};


#endif /* STEREO_GUI_H */
