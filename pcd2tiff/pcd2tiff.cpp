// Copyright (c) 2015, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include <dirent.h>
#include <boost/algorithm/string/split.hpp>

#include <tiffio.h>

using namespace covis::core;
using namespace covis::calib;

template<typename T>
bool is_infinite( const T &value )
{
    T max_value = std::numeric_limits<T>::max();
    T min_value = - max_value;

    return ! ( min_value <= value && value <= max_value );
}

template<typename T>
bool is_nan( const T &value )
{
    // True if NAN
    return value != value;
}

template<typename T>
bool is_zero( const T &value )
{
    // True if NAN
    return value == 0;
}

template<typename T>
bool is_valid( const T &value )
{
    return ! is_infinite(value) && ! is_nan(value) && ! is_zero(value);
}

void writeTiffImage( cv::Mat_<float> x,  cv::Mat_<float> y,  cv::Mat_<float> z, std::stringstream &path){

    const int width = x.cols;
    const int height = x.rows;

    const int samplesPerPixel = 3;
    const int bitsPerSample = 32;

    TIFF *tif = TIFFOpen(path.str().c_str(), "w");

    TIFFSetField (tif, TIFFTAG_IMAGEDEPTH, 1);
    TIFFSetField (tif, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField (tif, TIFFTAG_IMAGELENGTH, height);

    TIFFSetField (tif, TIFFTAG_SAMPLEFORMAT, 3);


    TIFFSetField (tif, TIFFTAG_RESOLUTIONUNIT, 2);
    TIFFSetField (tif, TIFFTAG_XRESOLUTION, 300.0f);
    TIFFSetField (tif, TIFFTAG_YRESOLUTION, 300.0f);

    TIFFSetField (tif, TIFFTAG_SAMPLESPERPIXEL, 3);
    TIFFSetField (tif, TIFFTAG_BITSPERSAMPLE, bitsPerSample);
    TIFFSetField (tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField (tif, TIFFTAG_PLANARCONFIG, 1);
    TIFFSetField (tif, TIFFTAG_PHOTOMETRIC, 2);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 3);
    TIFFSetField(tif, TIFFTAG_IMAGEDEPTH, 32764);

    int linebytes = samplesPerPixel * width;
    float *buf = NULL;


    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tif, width*samplesPerPixel));

    buf = new float [linebytes] ;


    for (int i = 0; i < x.rows; i++) {
        for (int j = 0; j < x.cols; j++) {
            if (!is_valid<float>(x(i, j)))
                x(i, j) = -1;

            if (!is_valid<float>(y(i, j)))
                y(i, j) = -1;

            if (!is_valid<float>(z(i, j)))
                z(i, j) = -1;
        }
    }


    for (int row = 0; row < height; row++){

        float* _xrow = x.ptr<float>(row);
        float* _yrow = y.ptr<float>(row);
        float* _zrow = z.ptr<float>(row);

        int j = 0;
        for (int i = 0; i < linebytes; i+=3){
            buf[i] = _xrow[j];
            buf[i+1] = _yrow[j];
            buf[i+2] = _zrow[j];
            j ++;
        }


        if (TIFFWriteScanline(tif, buf, row) < 0 )
        {
            std::cout << "Unable to write a row." <<std::endl ;
            break ;
        }
    }

    _TIFFfree(buf);
    TIFFClose(tif);

}



std::string findShortName(std::string path){
    std::string object = path;
    const size_t ii = object.find_last_of("/");
    if(ii != std::string::npos)
    {
        object = (object.substr(ii+1).c_str());
    }

    object = object.substr(0, object.size()-4).c_str();
    return object;
}

int main(int argc, const char** argv) {
    // Setup program options
    ProgramOptions po;
    po.addPositional("pcdfiles", "pcds file");
    po.addOption("n", "totiff", "output folder name");
    po.addFlag('d', "directory", "read all files from directory");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    std::vector<std::string> objectsPath;

    const bool directory = po.getFlag("directory");

    if (directory) {
        const char* path = po.getValue("pcdfiles").c_str();

        DIR *dir;
        class dirent *ent;

        dir = opendir(path);
        std::string full_path;

        while ((ent = readdir(dir)) != NULL) {
            if(ent->d_type == DT_REG)
            {
                full_path = std::string(path);
                full_path.append(ent->d_name);
                objectsPath.push_back(full_path);
            }
        }
        std::cout << "amount of objects: " << objectsPath.size() << std::endl;
    }
    else  {
        objectsPath.push_back(po.getValue("pcdfiles"));
    }

    for (unsigned int i  = 0; i < objectsPath.size(); i++)
    {
        std::string pcd = objectsPath.at(i);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile (pcd, *cloud);
        std::cout << "loaded: " << pcd << std::endl;

        cv::Mat_<float> x(cloud->height, cloud->width);
        cv::Mat_<float> y(cloud->height, cloud->width);
        cv::Mat_<float> z(cloud->height, cloud->width);

        for(size_t r = 0; r < cloud->height; ++r)
        {
            for(size_t c = 0; c < cloud->width; ++c)
            {

                x[r][c] = ((*cloud)(c,r).x);
                y[r][c] = ((*cloud)(c,r).y);
                z[r][c] = ((*cloud)(c,r).z);
            }
        }
        std::stringstream path;
        path << po.getValue("n") << "/";
        mkdir(path.str().c_str(), 0777);
        std::string file_name = findShortName(pcd);
        path << file_name << ".tiff";
        std::cout << "saved in: " << path.str() << std::endl;

        writeTiffImage(x, y, z, path);
    }

    return 0;
}





