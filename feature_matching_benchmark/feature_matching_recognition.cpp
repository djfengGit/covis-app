// CoViS
#include <covis/covis.h>

using namespace covis;

// Boost
#include <boost/filesystem.hpp>
#include <boost/range.hpp>

namespace fs=boost::filesystem;

// PCL
#include "pcl/io/pcd_io.h"
#include "pcl/io/ply_io.h"
#include "pcl/io/vtk_lib_io.h"
#include "pcl/registration/correspondence_rejection_distance.h"
#include "pcl/registration/icp.h"

// OpenMP
#include <omp.h>

// Own
#include "feature_matching_tools.h"
#include "feature_matching_features.h"
#include "feature_matching_metrics.h"

// Meshes, surfaces and feature points for the model database
std::vector<pcl::PolygonMesh::Ptr> meshq;
std::vector<CloudT::Ptr> surfq, query;
double avgModelMeshRes; // Average model mesh resolution
boost::random::mt19937 gen; // Random number generator

/*
 * Main
 */
int main(int argc, const char **argv) {
    /**
     * Get program inputs
     */

    // Setup program options
    core::ProgramOptions po;

    // Positionals
    po.addPositional("query", "query/object model files (PLY)");
    po.addPositional("target", "scene/target model files (PLY)");
    po.addPositional("pose-directory",
                     "path to pose files, file names are resolved as [pose-directory]/[query-basename]-[target-basename].[pose-suffix]");
    po.addPositional("pose-suffix", "pose file suffix (e.g. 'xf' for the UWA dataset)");
    po.addPositional("output-directory", "path to output directory");

    // Features
    po.addOption("features", "all", "select a subset of features - in case of 'all', all features are enabled.");
    po.addOption("decimation", 'd', -1, "if in ]0,1[, perform quadric mesh decimation down to this factor");
    po.addOption("far", -1, "if > 0, remove points far away at this distance in the scenes");
    po.addFlag("remove-table", "remove dominant plane from the scene");
    po.addOption("radius", 'r', 15, "feature radii, given as a multiple of the average query mr, one per feature");
    po.addOption("minimal-radius", 0.1, "set the minimal radius of USC and PPFHist (as a multiple of the radius)");
    po.addOption("resolution", 'e', 5,
                 "resolution of the feature seed points, given as a multiple of the average query mr");
    po.addOption("resolution-query", -1, "if non-zero, apply another resolution to the query models");
    po.addFlag('c', "pca", "use PCA version of a predefined subset of descriptors");
    po.addOption("pca-variation", 99,
                 "if PCA is enabled, use this variation [%] for selecting the number of components");

    // Matching
    po.addOption("threshold", 't', 1,
                 "inlier distance threshold during recognition, given as a multiple of the target mr");
    po.addFlag('f', "fusion", "test all ternary combinations instead of single features");

    // Recognition
    po.addOption("recognition-method", "ransac", "choose recognition method: ransac or voting");
    po.addOption("bandwidth-translation", 0, "absolute translational bandwidth (if <= 0, use 10 x resolution)");
    po.addOption("bandwidth-rotation", 22.5, "absolute rotational bandwidth [deg] between 0 and 180");
    po.addOption("multi-instance", 1, "use multi-instance detection for pose voting");
    po.addOption("ransac-iterations", 1000, "number of RANSAC iterations");
    po.addOption("correspondence-fraction", 1.0,
                 "use this best fraction ]0,1] of feature correspondences during detection");
    po.addOption("inlier-fraction", 0.05, "require this fraction ]0,1] of inliers during detection");
    po.addOption("outlier-fraction", 1, "allow at most this fraction ]0,1] of outliers during detection");
    po.addFlag("pose-priors", "use pose priors for RANSAC");
    po.addOption("kde-threshold", 0, "require at least this value of KDE to accept a detection");
    po.addOption("icp-iterations", 50, "number of ICP iterations");
    po.addOption("translation-tolerance", "maximum allowable translation error in metric units > 0");
    po.addOption("rotation-tolerance", "maximum allowable geodesic rotation error in degrees ]0, 180]");

    // General
    po.addOption("pose-separator", 'p', "-",
                 "pose file separator inserted between scene and model file (e.g. '-' for the UWA dataset)");
    po.addOption("gt-axis", "all", "when checking against GT poses, either use the full rotation or only a single axis (x, y or z)");
    po.addFlag('m', "pose-file-model-first", "pose file order, set this flag for query model name first");
    po.addFlag('v', "verbose", "print debug information");
    po.addFlag('i', "visualize", "show alignments");
    po.addFlag('p', "append", "append data to output files");
    po.addFlag('o', "omp", "use OpenMP, if available");
    po.addFlag('u', "dry-run", "don't save any outputs");

    // Parse
    if (!po.parse(argc, argv))
        return 1;

    const bool verbose = po.getFlag("verbose");

    if (verbose)
        po.print();

    // Get paths
    const std::vector<std::string> queries = po.getVector("query");
    const std::vector<std::string> targets = po.getVector("target");
    const std::string poseDir = po.getValue("pose-directory");
    const std::string poseSuffix = po.getValue("pose-suffix");
    std::string outputDir = po.getValue("output-directory");
    COVIS_ASSERT(!queries.empty() && !targets.empty());
    COVIS_ASSERT(!poseDir.empty() && !poseSuffix.empty() && !outputDir.empty());

    // Get feature name list
    fnames = po.getVector("features");
    if (fnames.empty() || (fnames.size() == 1 && boost::iequals(fnames[0], "all"))) { // No features provided, or "all"
        fnames = std::vector<std::string>(boost::begin(fnamesAll), boost::end(fnamesAll));
    } else { // Features provided
        for (size_t i = 0; i < fnames.size(); ++i) {
            boost::to_lower(fnames[i]);
            COVIS_ASSERT_MSG(
                    std::find(boost::begin(fnamesAll), boost::end(fnamesAll), fnames[i]) != boost::end(fnamesAll),
                    "Unknown feature: " << fnames[i] << "!");
        }
    }
    fnum = fnames.size();

    // Get feature options
    const double decimation = po.getValue<double>("decimation");
    const double farValue = po.getValue<double>("far");
    const bool removeTable = po.getFlag("remove-table");
    std::vector<double> fradMul = po.getVector<double>("radius");
    if (fradMul.size() == 1 && fnum > 1) {
        COVIS_MSG("Only one radius specified - applying to all features!");
        fradMul.resize(boost::size(fnamesAll), fradMul[0]);
    }
    std::vector<double> frad = fradMul;
    const double minrad = po.getValue<double>("minimal-radius");
    double fres = po.getValue<double>("resolution");
    COVIS_ASSERT(fres > 0.0);
    double fresQuery = po.getValue<double>("resolution-query");
    if (fresQuery <= 0)
        fresQuery = fres;

    // PCA case: remove unsuited features
    const bool pca = po.getFlag("pca");
    const float pcaVariation = po.getValue<float>("pca-variation"); // Variation in %

    core::print(fnames, std::cout, "Testing the following features: [", "]");
    if (pca && verbose)
        COVIS_MSG("Using the following PCA variation [%]: " << pcaVariation);

    double thres = (po.getValue<double>("threshold") <= 0.0 ? 1.0 : po.getValue<double>("threshold"));

    const bool fusion = po.getFlag("fusion");
    std::vector<boost::tuple<int, int, int> > fusionCombinations;
    std::vector<std::string> fnamesFusion;
    if (fusion && fnum > 2) {
        if (verbose)
            COVIS_MSG("Feature fusion enabled! Testing the following combinations:");
        for (int i = 0; i < int(fnum) - 2; ++i) {
            for (int j = i + 1; j < int(fnum) - 1; ++j) {
                for (int k = j + 1; k < int(fnum); ++k) {
                    fusionCombinations.push_back(boost::make_tuple(i, j, k));
                    fnamesFusion.push_back(fnames[i] + "-" + fnames[j] + "-" + fnames[k]);
                    if (verbose)
                        COVIS_MSG("\t" << fnamesFusion.back());
                }
            }
        }
    }
    const size_t fnumFusion = fusionCombinations.size();

    // Recognition
    const std::string recMethod = po.getValue("recognition-method");
    double bw = po.getValue<double>("bandwidth-translation");
    double bwrot = po.getValue<double>("bandwidth-rotation");
    COVIS_ASSERT(bwrot > 0 && bwrot <= 180);
    bwrot = bwrot * M_PI / 180.0;
    int multiInstance = po.getValue<int>("multi-instance");
    if(multiInstance < 1)
        multiInstance = 1;
    else if(multiInstance > 1)
        COVIS_ASSERT_MSG(boost::iequals(recMethod, "voting"), "Multi-instance detection only works for pose voting!");
    const size_t ransacIterations = po.getValue<size_t>("ransac-iterations");
    const double corrFraction = po.getValue<double>("correspondence-fraction");
    COVIS_ASSERT(corrFraction > 0.0 && corrFraction <= 1.0);
    const double inlierFraction = po.getValue<double>("inlier-fraction");
    COVIS_ASSERT(inlierFraction > 0.0 && inlierFraction <= 1.0);
    const double outlierFraction = po.getValue<double>("outlier-fraction");
    COVIS_ASSERT(outlierFraction > 0.0 && outlierFraction <= 1.0);
    const bool posePriors = po.getFlag("pose-priors");
    double kdeThreshold = po.getValue<double>("kde-threshold");
    const size_t icpIterations = po.getValue<size_t>("icp-iterations");
    COVIS_ASSERT(ransacIterations > 0 && icpIterations > 0);
    const double translationTol = po.getValue<double>("translation-tolerance");
    const double rotationTol = po.getValue<double>("rotation-tolerance");
    COVIS_ASSERT(translationTol > 0.0 && rotationTol > 0.0 && rotationTol <= 180.0);

    // General
    const std::string poseSeparator = po.getValue("pose-separator");
    const std::string gtAxis = po.getValue("gt-axis");
    COVIS_ASSERT(gtAxis == "all" || gtAxis == "x" || gtAxis == "y" || gtAxis == "z");
    const bool poseFileModelFirst = po.getFlag("pose-file-model-first");
    const bool visualize = po.getFlag("visualize");
    const bool append = po.getFlag("append");
    const bool useOmp = po.getFlag("omp");
    const bool dryrun = po.getFlag("dry-run");

    if (!useOmp)
        omp_set_num_threads(1);

    /*
     * END PROGRAM OPTIONS BLOCK
     */

    // Prepare output directories
    if (!boost::filesystem::is_directory(boost::filesystem::path(outputDir.c_str()))) {
        if (verbose)
            COVIS_MSG("Output directory \"" << outputDir << "\" does not exist, creating it...");
        COVIS_ASSERT(boost::filesystem::create_directory(boost::filesystem::path(outputDir.c_str())));
    }

    /*
     * Load query models
     */
    if (verbose)
        COVIS_MSG_INFO("Loading and preprocessing " << queries.size() << " query models...");
    surfq.resize(queries.size());
    meshq.resize(queries.size());
    std::pair<size_t, size_t> sizes(0, 0); // Stats
    for (size_t i = 0; i < queries.size(); ++i) {
        if (verbose)
            COVIS_MSG("\t" << boost::filesystem::path(queries[i]).stem().string());
        surfq[i].reset(new CloudT);
        meshq[i].reset(new pcl::PolygonMesh);
        pcl::io::loadPLYFile(queries[i], *meshq[i]);
        pcl::fromPCLPointCloud2(meshq[i]->cloud, *surfq[i]);
        COVIS_ASSERT_MSG(!meshq[i]->polygons.empty() && !surfq[i]->empty(), "Empty object polygon or vertex set!");
        sizes.first += surfq[i]->size();
        sizes.second += meshq[i]->polygons.size();

        // Normalize the normals
        size_t invalidPoints = 0;
        size_t invalidNormals = 0;
        for (size_t j = 0; j < surfq[i]->size(); ++j) {
            Eigen::Vector3f p = surfq[i]->points[j].getVector3fMap();
            if(p.hasNaN())
                ++invalidPoints;
            Eigen::Vector3f n = surfq[i]->points[j].getNormalVector3fMap();
            if (!n.hasNaN() && n.norm() > 1e-5f) {
                n.normalize();
            } else {
                ++invalidNormals;
                n.setZero();
            }
            surfq[i]->points[j].getNormalVector3fMap() = n;
        }

        if (invalidPoints > 0)
            COVIS_MSG_WARN("Warning: mesh has " << invalidPoints << "/" << surfq[i]->size() << " invalid points!");
        if (invalidNormals > 0)
            COVIS_MSG_WARN("Warning: mesh has " << invalidNormals << "/" << surfq[i]->size() << " invalid normals!");

        // Copy normalized normals points back to mesh
        pcl::toPCLPointCloud2(*surfq[i], meshq[i]->cloud);
    }

    COVIS_MSG_INFO("Got " << queries.size() << " surface models with an average of " <<
                          sizes.first / queries.size() << " vertices and " <<
                          sizes.second / queries.size() << " polygons");

    /*
     * Decimate queries
     */
    sizes = std::make_pair(0, 0);
    if (decimation > 0.0 && decimation < 1.0) {
        if (verbose)
            COVIS_MSG_INFO("Performing mesh decimation with a factor of " << decimation << "...");
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for (size_t i = 0; i < queries.size(); ++i) {
            const boost::filesystem::path qpath(queries[i]);
#ifdef _OPENMP
#pragma omp critical
#endif
            if (verbose)
                COVIS_MSG("\t" << qpath.stem().string());

            decimate(*meshq[i], decimation);
            pcl::fromPCLPointCloud2(meshq[i]->cloud, *surfq[i]);

#ifdef _OPENMP
#pragma omp critical
#endif
            {
                sizes.first += surfq[i]->size();
                sizes.second += meshq[i]->polygons.size();
            }
        }

        COVIS_MSG_INFO("Got " << queries.size() << " decimated surface models with an average of " <<
                              sizes.first / queries.size() << " vertices and " <<
                              sizes.second / queries.size() << " polygons");
    }

    // Estimate average model resolution, used for setting inlier threshold, feature radius and feature resolution
    if (verbose)
        COVIS_MSG_INFO("Estimating average object mesh resolution...");
    Eigen::VectorXd resolutions(queries.size());
    for (size_t i = 0; i < queries.size(); ++i)
        resolutions[i] = resolution(meshq[i], surfq[i]);

    avgModelMeshRes = resolutions.mean();
    for (size_t i = 0; i < frad.size(); ++i)
        frad[i] *= avgModelMeshRes;
    fresQuery *= avgModelMeshRes;
    fres *= avgModelMeshRes;
    thres *= avgModelMeshRes;
    if(bw <= 0)
        bw = avgModelMeshRes * 10;

    if (verbose) {
        COVIS_MSG("\t" << avgModelMeshRes);
        core::print(frad, std::cout, "Setting feature radii to: [", "]");
        COVIS_MSG_INFO("Setting query/target feature resolution to: " << fresQuery << "/" << fres);
        COVIS_MSG_INFO("Setting RANSAC+ICP inlier threshold to: " << thres);
    }

    /*
     * Seed the queries
     */
    if (verbose)
        COVIS_MSG_INFO("Selecting seed points for description using object feature resolution...");

    query.resize(queries.size());
    size_t size = 0;
    for (size_t i = 0; i < queries.size(); ++i) {
        seed(surfq[i], fresQuery, query[i]);

        size += query[i]->size();
//        show(meshq[i], query[i]);
    }

    COVIS_MSG_INFO("Got " << queries.size() << " object models with an average of " <<
                          size / queries.size() << " points");


    /**
     * Compute features for model library
     */
    if (verbose)
        COVIS_MSG_INFO("Computing features for query model(s)...");

    // Each entry becomes a vector of length fnum
    std::vector<std::vector<MatrixT> > featq(queries.size());

#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (size_t i = 0; i < queries.size(); ++i) {
#ifdef _OPENMP
#pragma omp critical
#endif
        if (verbose)
            COVIS_MSG("\t" << boost::filesystem::path(queries[i]).stem().string() << " (" << surfq[i]->size() << " surface points, " << query[i]->size() << " feature points)");
        featq[i].resize(fnum);
        Eigen::VectorXf dummy(fnum);
        features(meshq[i], surfq[i], query[i], frad, minrad, avgModelMeshRes,
                 featq[i],
                 dummy);
    }

    /**
     * Compute PCA
     */
    std::vector<cv::PCA> pcas;
    std::vector<std::vector<size_t> > components;
    if (pca)
        pcaTrain(featq, fnames, std::vector<float>(1, pcaVariation), pcas, components, verbose);

    // Create a library of all object features
    std::vector<MatrixT> featqAll(fnum);
    for (size_t j = 0; j < queries.size(); ++j) {
        // Add features to library
        for (size_t k = 0; k < fnum; ++k) {
            const size_t rowsFirst = featqAll[k].rows();
            const size_t rowsSecond = featq[j][k].rows();
            const size_t cols = featq[j][k].cols();
            COVIS_ASSERT(rowsFirst + rowsSecond > 0 && cols > 0);
            featqAll[k].conservativeResize(rowsFirst + rowsSecond, cols);
            featqAll[k].block(rowsFirst, 0, rowsSecond, cols) = featq[j][k];
        }
    }

    // Create a map from global feature index to <object, feature>
    std::vector<std::pair<size_t, size_t> > mapFeatqAll(featqAll[0].rows());
    size_t cnt = 0; // Global counter
    for (size_t j = 0; j < featq.size(); ++j) // Number of objects
        for (int k = 0; k < featq[j][0].rows(); ++k) // Number of features
            mapFeatqAll[cnt++] = std::make_pair(j, k);
    COVIS_ASSERT(cnt == mapFeatqAll.size());

    // Project object features to PCA subspaces
    if (pca) {
        // Now update the features
        if (verbose)
            COVIS_MSG("Projecting all query features to PCA subspaces...");
        Eigen::VectorXf dummy(fnum);
        pcaProject(featqAll, fnames, pcas, components, dummy, verbose);
    }

    // Create a search index
    if (verbose)
        COVIS_MSG_INFO("Creating search structures for " << featqAll[0].rows() << " object library features...");

    typedef Matcher<L2_RATIO> MatcherT;
    typedef Matcher<CHISQ_RATIO> MatcherTPPFHist;
    MatcherT::KDTreePtr trees[fnum];
    MatcherTPPFHist::KDTreePtr treeppfh;
    for (size_t j = 0; j < fnum; ++j) {
        if (verbose)
            COVIS_MSG("\t" << fnames[j] << "...");
        if(fnames[j] == "ppfhist")
            treeppfh = MatcherTPPFHist::index(featqAll[j], 4);
        else
            trees[j] = MatcherT::index(featqAll[j], 4);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Start loop over all scenes
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const size_t &fnumDetection = (fusion ? fnumFusion : fnum);
    const std::vector<std::string> &fnamesDetection = (fusion ? fnamesFusion : fnames);

    // For each feature there is a matrix with detection/timing information, one row per scene
    MatrixT detectionOutputs[fnumDetection];
    MatrixT detectionTimings[fnumDetection];

    for (size_t i = 0; i < targets.size(); ++i) {
        const boost::filesystem::path tpath(targets[i]);
        COVIS_ASSERT(boost::filesystem::is_regular_file(tpath));
        const std::string pscn = tpath.stem().string();

        if (verbose)
            COVIS_MSG_INFO("---------- PROCESSING SCENE " << i + 1 << "/" << targets.size() <<
                                                          " (" << pscn << ") ----------");

        CloudT::Ptr surft(new CloudT);
        pcl::PolygonMesh::Ptr mesht(new pcl::PolygonMesh);
        pcl::io::loadPLYFile(targets[i], *mesht);
        pcl::fromPCLPointCloud2(mesht->cloud, *surft);
        COVIS_ASSERT_MSG(!mesht->polygons.empty() && !surft->empty(), "Empty scene polygon or vertex set!");

        if(verbose)
            COVIS_MSG_INFO("Loaded scene with " << surft->size() << " vertices and " << mesht->polygons.size() << " polygons");

        // Remove far points
        if(farValue > 0){
            if(verbose)
                COVIS_MSG_INFO("Removing far points from scene (distance > " << farValue << ")...");
            far(*mesht, farValue);
            pcl::fromPCLPointCloud2(mesht->cloud, *surft);
        }

        // Normalize the normals
        size_t invalidNormals = 0;
        for (size_t j = 0; j < surft->size(); ++j) {
            Eigen::Vector3f n = surft->points[j].getNormalVector3fMap();
            if (!n.hasNaN() && n.norm() > 1e-5f) {
                n.normalize();
            } else {
                ++invalidNormals;
                n.setZero();
            }
            surft->points[j].getNormalVector3fMap() = n;
        }

        if (invalidNormals > 0)
            COVIS_MSG_WARN("Warning: mesh has " << invalidNormals << "/" << surft->size() << " invalid normals!");

        // Copy normalized normals back to mesh
        pcl::toPCLPointCloud2(*surft, mesht->cloud);

        // Decimate mesh, copy back to surface
        float decimationTiming = 0.0;
        if (decimation > 0.0 && decimation < 1.0) {
            if (verbose)
                COVIS_MSG_INFO("Decimating scene...");
            TIME(\
                    decimate(*mesht, decimation);,
                    decimationTiming
            );
            pcl::fromPCLPointCloud2(mesht->cloud, *surft);

            if (verbose)
                COVIS_MSG("\tDecimation time: " << decimationTiming << " s");
        }

        // Remove table
        if(removeTable){
            if(verbose)
                COVIS_MSG_INFO("Removing table from scene...");
            table(*mesht, avgModelMeshRes);
            pcl::fromPCLPointCloud2(mesht->cloud, *surft);
        }

        if(verbose)
            COVIS_MSG_INFO("Got preprocessed scene with " << surft->size() << " vertices and " << mesht->polygons.size() << " polygons");

        // Now compute inlier threshold using scene resolution
        if (verbose) {
            COVIS_MSG_INFO("Estimating scene mesh resolution...");
            const double sceneRes = resolution(mesht, surft);
            COVIS_MSG("\t" << sceneRes);
            COVIS_MSG("\tAverage model to scene resolution ratio: " << avgModelMeshRes/sceneRes);
        }
//        const double sceneResSq = sceneRes * sceneRes;

        // Generate search, only used for checking validity of GT poses
        detect::PointSearch<PointT> surftSearch;
        surftSearch.setTarget(surft);

        // Find out which objects are present in the scene, how many times, and their pose(s)
        size_t queryMask[queries.size()];
        Eigen::MatrixXf queryPoses[queries.size()];
        for (size_t j = 0; j < queries.size(); ++j) {
            // Resolve pose file
            const std::string pobj = fs::path(queries[j]).stem().string();
            const std::string poseFile = (poseFileModelFirst ?
                                          poseDir + "/" + pobj + poseSeparator + pscn + "." + poseSuffix :
                                          poseDir + "/" + pscn + poseSeparator + pobj + "." + poseSuffix);

            // Load pose
            try {
                if(verbose)
                    COVIS_MSG_INFO("Loading GT pose file " << poseFile << "...");
                util::loadEigen(poseFile, queryPoses[j]);
            } catch (const std::exception &e) {
                COVIS_MSG_ERROR("GT pose for object \"" << pobj << "\" in scene \"" << pscn << "\" not found!");
                queryMask[j] = 0;
                continue;
            }

            if(queryPoses[j].cols() != 4 || queryPoses[j].rows() % 4 != 0) {
                COVIS_MSG_ERROR("Pose file must have 4 columns and an integer multiple of 4 rows!");
                queryMask[j] = 0;
                continue;
            }

            const int numPoses = queryPoses[j].rows()/4;
            queryMask[j] = 0;
            for(int k = 0; k < numPoses; ++k) {
                if (verbose)
                    COVIS_MSG("Checking GT pose " << k + 1 << "/" << numPoses << "...");
                const Eigen::Matrix4f posejk = queryPoses[j].block<4, 4>(k * 4, 0);

                // Transform object
                CloudT::Ptr queryTransformed(new CloudT);
                pcl::transformPointCloud<PointT>(*query[j], *queryTransformed, posejk);

                // TODO: Check that the pose is actually valid
                core::Correspondence::VecPtr pointCorr = surftSearch.knn(queryTransformed, 1);
                size_t numInliers = 0;
                for (core::Correspondence::Vec::const_iterator it = pointCorr->begin(); it != pointCorr->end(); ++it)
                    if (it->distance[0] <= avgModelMeshRes*avgModelMeshRes)
                        ++numInliers;

                const bool queryMaskJ = (numInliers >= size_t(0.01 * queryTransformed->size()));
                if (!queryMaskJ) {
                    COVIS_MSG_WARN("Too few valid points (" << numInliers << "/" << queryTransformed->size() <<
                                                            ") for object \"" << pobj << "\" in scene \"" << pscn
                                                            << "\""
                                                            << "!");
                }

                queryMask[j] += queryMaskJ;
            }

            // In this case, the object is non-existent
            if (!queryMask[j])
                COVIS_MSG_ERROR("Ground truth pose(s) from dataset invalid for current scene!");
        } // End local loop over queries for finding mask and poses (j)

        // TODO: No objects with GT information in scene
        if (std::accumulate(queryMask, queryMask + queries.size(), int(0)) == 0) {
            COVIS_MSG_ERROR("No object with GT information found for scene! Skipping...");
            continue;
        }

        /**
         * Find scene seed points
         */
        // Select seed points on the scene surface
        float seedTiming;
        CloudT::Ptr target(new CloudT);
        TIME(\
                seed(surft, fres, target);,
                seedTiming
        );

        if (verbose)
            COVIS_MSG("\tSeed computation time: " << seedTiming << " s");

        /**
         * Compute scene features
         */
        if (verbose)
            COVIS_MSG_INFO("Computing " << target->size() << " target features...");
        std::vector<MatrixT> featt(fnum);
        Eigen::VectorXf featureTiming(fnum);
        features(mesht, surft, target, frad, minrad, avgModelMeshRes,
                 featt,
                 featureTiming);

        // Print the computation times
        if (verbose) {
            for (size_t j = 0; j < fnum; ++j)
                COVIS_MSG("\t" << fnames[j] << ": " << featureTiming(j) << " s");
        }

        /**
         * Compute PCA subspaces for the target features
         */
        Eigen::VectorXf projectionTiming(fnum);
        projectionTiming.setZero();
        if (pca) {
            // Now update the features
            if (verbose)
                COVIS_MSG("Projecting all target features to PCA subspaces...");
            pcaProject(featt, fnames, pcas, components, projectionTiming, verbose);
        }

        /**
         * Perform feature matching
         */
        if (verbose)
            COVIS_MSG_INFO("Matching " << featt[0].rows() << " scene features with object library (" << featqAll[0].rows() << " features)...");
//        if (verbose)
//            COVIS_MSG_INFO("Matching " << featt[0].rows() << " scene features with " << featqAll[0].rows() << " object features...");

        const int checks = 512; // TODO: Promote to cmdline
        // This contains (for each feature) a complete set of correspondences scene feature --> stacked object features
        std::vector<core::Correspondence::VecPtr> featureCorr(fnum);
        Eigen::VectorXf matchTiming(fnum);
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for (size_t j = 0; j < fnum; ++j) {
            TIME(\
                    if(fnames[j] == "ppfhist")
                        featureCorr[j] = MatcherTPPFHist::knn(*treeppfh, featt[j], checks);
                    else
                        featureCorr[j] = MatcherT::knn(*trees[j], featt[j], checks);,//TODO
//                    featureCorr[j] = match(featt[j], featqAll[j], (fnames[j] == "ppfhist" ? CHISQ_RATIO : L2_RATIO));,
                    matchTiming[j]
            );
            featureCorr[j] = core::flatten(*featureCorr[j]);//TODO

#ifdef _OPENMP
#pragma omp critical
#endif
            if (verbose)
                COVIS_MSG("\t" << fnames[j] << ": " << matchTiming[j] << " s");
        }

        /**
         * Fusion
         */
        // Conservatively, we measure the full fusion time of all combinations, since it is tough to time each combination independently
        Eigen::VectorXf fusionTiming(fnumDetection);
        fusionTiming.setZero();
        if (fusion)
            fuse(featureCorr, fusionCombinations, fusionTiming);


        /**
         * Perform object detection
         */
        core::Correspondence::VecPtr corrQuerytarget[fnumDetection][featq.size()];

        Eigen::VectorXf detectionTiming = Eigen::VectorXf::Zero(fnumDetection);
        Eigen::VectorXf refinementTiming = Eigen::VectorXf::Zero(fnumDetection);
        Eigen::VectorXf segmentationTiming = Eigen::VectorXf::Zero(fnumDetection);

        // Loop over number of features
        for (size_t j = 0; j < fnumDetection; ++j) {
            // Create a timer for this feature to measure the total detection time of all objects in current scene
            core::ScopedTimer::Ptr t(
                    new core::ScopedTimer("Object detection+refinement+segmentation, " + fnamesDetection[j]));

            // Loop over query objects and allocate correspondences object --> scene per object
            for (size_t k = 0; k < featq.size(); ++k)
                corrQuerytarget[j][k].reset(new core::Correspondence::Vec);

            // Reverse all correspondences (object --> scene), and spread them out on all object models
            for (size_t k = 0; k < featureCorr[j]->size(); ++k) {
                const size_t idxMatch = (*featureCorr[j])[k].match[0]; // Index into object library
                const std::pair<size_t, size_t> &idxFeatq = mapFeatqAll[idxMatch]; // Index for <object, feature>
                corrQuerytarget[j][idxFeatq.first]->push_back(
                        core::Correspondence(idxFeatq.second, (*featureCorr[j])[k].query,
                                             (*featureCorr[j])[k].distance[0]));
            }

            // Take a subset of best correspondences for each object
            std::vector<size_t> sizes(featq.size());
            for (size_t k = 0; k < featq.size(); ++k) {
                core::sort(*corrQuerytarget[j][k]);
                corrQuerytarget[j][k]->resize(corrFraction * corrQuerytarget[j][k]->size());
                sizes[k] = corrQuerytarget[j][k]->size();
            }

            // Sort the object list by descending number of correspondences
            const std::vector<size_t> order = core::sort<size_t>(sizes, false);

            // For evaluating poses
            detect::FitEvaluation<PointT>::Ptr eval(new detect::FitEvaluation<PointT>(target));
            eval->setInlierThreshold(thres);
            const bool fullEval = false; // Used for the Ransac object below
            if (fullEval) {
                eval->setOcclusionReasoning(true);
                eval->setPenaltyType(detect::FitEvaluation<PointT>::INLIERS_OUTLIERS_RMSE);
            } else {
                eval->setOcclusionReasoning(false);
                eval->setPenaltyType(detect::FitEvaluation<PointT>::INLIERS);
            }

            // For doing segmentation
            detect::PointSearch<PointT> targetSearch;
            targetSearch.setTarget(target);

            // Scene detections (all and accepted by inlier/voting thresholds)
            core::Detection::Vec detections, detectionsAccepted;
            std::vector<bool> targetMask(target->size(), true); // Will be filled with false for each segmented point

            // Loop over all objects and run estimation in the above order
            for (size_t k = 0; k < queries.size(); ++k) {
                // Current object index
                const size_t kidx = order[k];
                const std::string queryName = fs::path(queries[kidx]).stem().string();

//                if(queryMask[kidx] == 0) {
//                    if (verbose)
//                        COVIS_MSG_WARN("Skipping non-present object " << queryName << "!");
//                    continue;
//                }

                // Take correspondences only for remaining target feature points
                core::Correspondence::VecPtr corrRemain(new core::Correspondence::Vec);
                for (size_t l = 0; l < corrQuerytarget[j][kidx]->size(); ++l)
                    if (targetMask[(*corrQuerytarget[j][kidx])[l].match[0]])
                        corrRemain->push_back((*corrQuerytarget[j][kidx])[l]);

                // If this object has no correspondences in remaining scene data, continue to next
                if (corrRemain->size() < 3) {
                    if (verbose)
                        COVIS_MSG("Not enough correspondences for object " << queryName << " - not found!");
                    continue;
                }

                if(verbose)
                    COVIS_MSG_INFO("Running estimator for object " << queryName << " using " << corrRemain->size() << " correspondences...");

//                if(visualize)
//                    visu::showCorrespondences<PointT>(query[kidx], target, *corrRemain, 2);

//                core::Detection d;
                core::Detection::Vec dquery;
                if(boost::iequals(recMethod, "voting")) {
                    detect::PoseVoting<PointT> nv;
                    nv.setSource(query[kidx]);
                    nv.setTarget(target);
                    nv.setCorrespondences(corrRemain);
                    nv.setTranslationBandwidth(bw);
                    nv.setRotationBandwidth(bwrot);
                    nv.setTessellation(2.0 * M_PI / 60.0);
                    nv.setVerbose(verbose);

                    if(multiInstance == 1) {
                        core::Detection d = nv.estimate();
                        eval->update(query[kidx], d.pose);
                        d.rmse = eval->rmse();
                        d.inlierfrac = eval->inlierFraction();
                        d.outlierfrac = eval->outlierFraction();
                        d.penalty = eval->penalty();
                        dquery.push_back(d);
                    } else {
                        /// TODO: Multi-instance
                        nv.estimate();
                        dquery = nv.getAllDetections();
                        core::sort(dquery, "kde", true);
                        if (int(dquery.size()) > multiInstance)
                            dquery.resize(multiInstance);
                    }
//
//                    visu::DetectionVisu<PointT> dvisu;
//                    dvisu.setBackgroundColor(255, 255, 255);
//                    dvisu.setTitle("Detections for " + fnamesDetection[j]);
//                    dvisu.setQueries(meshq);
//                    dvisu.setTarget(mesht);
//                    dvisu.setDetections(dall);
//                    dvisu.show();


                    // Visualize stuff
                    if(false && visualize) {
                        if(verbose)
                            COVIS_MSG_INFO("Visualizing stuff from pose voting...");
                        // Get a centered version of the query mesh
                        CloudT querySurfC;
                        pcl::PolygonMeshPtr mtmp(new pcl::PolygonMesh(*meshq[kidx]));
                        Eigen::Vector4f C;
                        pcl::fromPCLPointCloud2(meshq[kidx]->cloud, querySurfC);
                        pcl::compute3DCentroid(querySurfC, C);
                        pcl::demeanPointCloud(querySurfC, C, querySurfC);
                        pcl::toPCLPointCloud2(querySurfC, mtmp->cloud);

                        // Show the correspondences
                        CloudT::Ptr queryC(new CloudT);
                        pcl::compute3DCentroid(*query[kidx], C);
                        pcl::demeanPointCloud(*query[kidx], C, *queryC);

                        visu::CorrVisu<PointT> cv;
                        cv.setBackgroundColor(255, 255, 255);
                        cv.setSeparate(false);
                        cv.setShowPoints(false);
                        cv.setLevel(10);
                        cv.addMesh(mtmp, "meshq");
                        cv.addMesh(mesht, "mesht");
                        cv.setQuery(queryC);
                        cv.setTarget(target);
                        cv.setCorr(*corrRemain);
                        cv.show();

                        // Show the votes
                        visu::Visu3D v("Translation votes");
                        v.setBackgroundColor(255, 255, 255);
                        v.addMesh(mtmp, "meshq");
                        v.getMeshActor("meshq")->GetProperty()->SetOpacity(0.5);
                        v.visu.addCoordinateSystem(25*avgModelMeshRes);
                        v.addMesh(mesht, "mesht");
                        v.addScalarField<PointT,float>(nv.getTranslationVotes(), nv.getDensities(), "votes");
                        v.visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.25, "votes");
                        v.show();

//                        // Put XYZ and field values together
//                        pcl::PointCloud<pcl::PointXYZI>::Ptr cfield(
//                                new pcl::PointCloud<pcl::PointXYZI>(nv.getTranslationVotes()->width, nv.getTranslationVotes()->height));
//                        for(size_t i = 0; i < cfield->size(); ++i) {
//                            (*cfield)[i].intensity = nv.getDensities()[i];
//                            (*cfield)[i].x = (*nv.getTranslationVotes())[i].x;
//                            (*cfield)[i].y = (*nv.getTranslationVotes())[i].y;
//                            (*cfield)[i].z = (*nv.getTranslationVotes())[i].z;
//                        }
//                        pcl::io::savePCDFile("votes-"+queryName+"-"+pscn+".pcd", *cfield);
//
//                        pcl::PointCloud<pcl::PointXYZ> tmp;
//                        pcl::copyPointCloud(*surft, tmp);
//
//                        pcl::io::savePCDFile(pscn+".pcd", tmp);
//                        pcl::io::savePCDFile<PointT>(queryName+".pcd", querySurfC);
                    }
                } else if(boost::iequals(recMethod, "ransac")) {

                    // Initialize RANSAC
                    detect::Ransac<PointT> ransac;
                    ransac.setSource(query[kidx]);
                    ransac.setTarget(target);
                    ransac.setCorrespondences(corrRemain);
                    ransac.setFitEvaluation(eval);

                    ransac.setIterations(ransacIterations);
                    ransac.setWeightedSampling(true); // Sample correspondences with a probability proportional to their quality
                    ransac.setInlierThreshold(thres); // Inlier threshold
                    ransac.setInlierFraction(inlierFraction); // Inlier fraction
                    ransac.setFullEvaluation(fullEval); // Evaluation using only feature point matches if false
                    ransac.setPrerejection(false); // TODO: Later...
                    ransac.setPrerejectionSimilarity(0.9); // TODO: Later...

                    ransac.setVerbose(verbose);

                    // Perform pose estimation
                    core::Detection d = ransac.estimate();


                    /// TODO: Pose prior stuff
                    if(posePriors) {
                        COVIS_MSG_ERROR("Running pose prior stuff!");
                        // Trivial rotation constraints: object axis must align to scene axis
                        typedef std::pair<Eigen::Vector3f,Eigen::Vector3f> VecPair;
                        std::vector<VecPair> rotConstraints;
                        rotConstraints.push_back(std::make_pair(Eigen::Vector3f(1,0,0), Eigen::Vector3f(1,0,0))); // Object x-axis parallel w. scene x-axis
                        rotConstraints.push_back(std::make_pair(Eigen::Vector3f(0,1,0), Eigen::Vector3f(-1,0,0))); // Object y-axis anti-parallel w. scene x-axis
                        rotConstraints.push_back(std::make_pair(Eigen::Vector3f(1,-1,0), Eigen::Vector3f(1,0,0))); // Object axis between x and y parallel w. scene x-axis
//                        const Eigen::Vector3f xalignment(1, 0, 0);
//                        const Eigen::Vector3f yalignment(-1, 0, 0);
                        const float tol = 10 * M_PI / 180.0;
                        const core::Detection::Vec& all = ransac.getAllDetections();
                        core::Detection::Vec constrained;
                        for (size_t i = 0; i < all.size(); ++i) {
                            bool rotConstraintsOk = false;
                            for(size_t j = 0; j < rotConstraints.size(); ++j) {
                                // Place the object axis in the scene
                                const Eigen::Vector3f& alignedObjectAxis = all[i].pose.topLeftCorner(3,3) * rotConstraints[j].first;
                                // Get the associated scene axis
                                const Eigen::Vector3f& sceneAxis = rotConstraints[j].second;
                                // Do the check
                                const float dot = alignedObjectAxis.dot(sceneAxis);
                                const float angle = ( dot <= -1 ? M_PI : (dot >= 1 ? 0 : acosf(dot)) );
                                rotConstraintsOk |= (angle <= tol);
                            }
                            if(rotConstraintsOk)
                                constrained.push_back(all[i]);
//
//                            const Eigen::Vector3f xaxis = all[i].pose.block(0, 0, 3, 1);
//                            const Eigen::Vector3f yaxis = all[i].pose.block(0, 1, 3, 1);
//                            const float xdot = xalignment.dot(xaxis);
//                            const float xangle = xdot <= -1 ? M_PI : (xdot >= 1 ? 0 : acosf(xdot));
//                            // M2 for T-rex
//                            const float ydot = yalignment.dot(yaxis);
//                            const float yangle = ydot <= -1 ? M_PI : (ydot >= 1 ? 0 : acosf(ydot));
//                            if (xangle <= tol || yangle <= tol)
//                                constrained.push_back(all[i]);
                        }

                        if (constrained.empty()) {
                            COVIS_MSG_WARN("Got zero constrained candidates - reverting to baseline RANSAC output!");
                        } else {
                            COVIS_MSG_INFO("Got " << constrained.size() << " constrained candidates");
                            core::sort(constrained);
                            d = constrained[0];
                            if (visualize)
                                visu::showDetection(meshq[kidx], mesht, d.pose);
                        }
                        /// TODO: End pose prior stuff
                    }

                    dquery.push_back(d);

                } else {
                    COVIS_THROW("Unknown recognition method: " << recMethod);
                }

                // Increment total detection time for current feature
                detectionTiming[j] += t->seconds();

                // Prepare detection outputs for current scene (row): [detected present translation_error rotation_error]
//                const int numPoses = queryPoses[kidx].rows() / 4; // Number of given GT poses for this object in this scene
                const int numGTPoses = queryMask[kidx]; // Number of given GT poses for this object in this scene
                const int numPoses = MAX(int(dquery.size()), numGTPoses); // In come cases the detector produces more poses than there are GT Poses
                int rowidx = detectionOutputs[j].rows(); // Row index, will be traversed in the loop below
                detectionOutputs[j].conservativeResize(rowidx + numPoses, 4); // Add new rows
                detectionOutputs[j].block(rowidx, 0, numPoses, 1).setZero(); // Detected instances, to be modified below
//                detectionOutputs[j].block(rowidx, 1, numPoses, 1).setZero();
//                detectionOutputs[j].block(rowidx, 1, numGTPoses, 1).setOnes(); // Present instances, can be less than the true number of instances
                detectionOutputs[j].block(rowidx, 1, numPoses, 1).setConstant(MIN(1,numGTPoses/float(numPoses)));
                detectionOutputs[j].block(rowidx, 2, numPoses, 1).setConstant(FLT_MAX);
                detectionOutputs[j].block(rowidx, 3, numPoses, 1).setConstant(FLT_MAX); // Initialize pose errors to invalids



//                    const int rowidx = detectionOutputs[j].rows();
//                    detectionOutputs[j].conservativeResize(rowidx + 1, 4);
//                    detectionOutputs[j](rowidx, 0) = accepted; // 1 if detected
//                    detectionOutputs[j](rowidx, 1) = queryMask[kidx]; // 1 if present
//                    detectionOutputs[j](rowidx, 2) = detectionOutputs[j](rowidx, 3) = -1; // Initialize pose errors to invalids

                std::vector<bool> gtPosesMarked(queryPoses[kidx].rows()/4, false);
                for(size_t l = 0; l < dquery.size(); ++l, ++rowidx) {
                    // If detector produced a result
                    core::Detection& d = dquery[l];
                    if (d) {
                        // Refine
                        if (verbose)
                            COVIS_MSG("Object " << queryName << " detected with " << d.inlierfrac*100 << " % inliers! Refining pose...");
                        core::ScopedTimer::Ptr ticp(new core::ScopedTimer("Refinement"));
                        pcl::IterativeClosestPoint<PointT, PointT> icp;
                        icp.setInputSource(query[kidx]);
                        icp.setInputTarget(target);
                        //                    icp.setInputSource(surfq[kidx]);
                        //                    icp.setInputTarget(surft);
                        icp.setMaximumIterations(icpIterations);
                        icp.setMaxCorrespondenceDistance(thres); // Set inlier threshold to lowest resolution
                        pcl::PointCloud<PointT> tmp;
                        icp.align(tmp, d.pose);
                        if (icp.hasConverged()) {
                            d.pose = icp.getFinalTransformation();
                            eval->update(query[kidx], d.pose);
                            d.rmse = eval->rmse();
                            d.inlierfrac = eval->inlierFraction();
                            d.outlierfrac = eval->outlierFraction();
                            d.penalty = eval->penalty();
                        } else {
                            if (verbose)
                                COVIS_MSG("\tICP failed!");
                        }

                        // Increment total refinement time for current feature
                        refinementTiming[j] += ticp->seconds();
                        ticp.reset();

                        // Correct the object index before storing
                        d.idx = kidx;
                        detections.push_back(d);

//                        // Mask out the segment in the scene
//                        if (verbose)
//                            COVIS_MSG("Segmenting object " << queryName << " from scene...");
//
//                        core::ScopedTimer::Ptr tseg(new core::ScopedTimer("Segmentation"));
//                        CloudT queryT;
//                        pcl::transformPointCloud<PointT>(*query[kidx], queryT, d.pose);
//                        core::Correspondence::VecPtr corrQueryTTarget = targetSearch.radius(queryT,
//                                                                                            fres); // TODO: Radius
//                        size_t cnt = 0;
//                        for (size_t l = 0; l < corrQueryTTarget->size(); ++l) {
//                            for(size_t m = 0; m < (*corrQueryTTarget)[l].size(); ++m) {
//                                if(targetMask[(*corrQueryTTarget)[l].match[m]]) {
//                                    targetMask[(*corrQueryTTarget)[l].match[m]] = false;
//                                    ++cnt;
//                                }
//                            }
//                        }
//
//                        if (verbose)
//                            COVIS_MSG("\tSegmented " << cnt << "/" << target->size() << " scene points");
//
//                        // Increment total refinement time for current feature
//                        segmentationTiming[j] += tseg->seconds();
                    }

                    // TODO: Accept or reject final detection
                    //                const bool accepted = (d && d.inlierfrac >= inlierFraction && d.params["kde"] >= 50);
                    bool accepted = (d &&
                                    d.inlierfrac >= inlierFraction &&
                                    d.outlierfrac <= outlierFraction);
                    if(boost::iequals(recMethod, "voting"))
                        accepted = accepted && (d.params["kde"] >= kdeThreshold);

                    if (accepted)
                        detectionsAccepted.push_back(d);

                    detectionOutputs[j](rowidx, 0) = accepted; // Detected



                    /*
                     * Detection is done, now store results in output files
                     */

                    if (accepted) { // Detected with enough inliers
                        COVIS_MSG_INFO(d);
                        const core::Correspondence::VecPtr corrPositives = detect::filterCorrespondencesTransform<PointT>(
                                *corrRemain, query[kidx], target, d.pose, thres);
                        COVIS_MSG_INFO("Number of correct feature matches under estimated pose: " <<
                                                                                                  corrPositives->size()
                                                                                                  << "/"
                                                                                                  << corrRemain->size()
                                                                                                  <<
                                                                                                  " (" << 100 *
                                                                                                          double(corrPositives->size()) /
                                                                                                          double(corrRemain->size())
                                                                                                  << " %)");

                        // Compare the pose with GT (if existent), update detection outputs
                        if (queryMask[kidx]) { // Detected and present
                            if (verbose)
                                COVIS_MSG_INFO("TRUE POSITIVE for object " << queryName << "!");
                            // Translation
                            const Eigen::Vector3f tdet = d.pose.block<3, 1>(0, 3);
                            // Rotation
                            const Eigen::Matrix3f Rdet = d.pose.block<3, 3>(0, 0);
                            // Check against all GT poses
                            float terr = FLT_MAX;
                            float Rerr = FLT_MAX;
                            int idxmin = -1;
                            for (int m = 0; m < queryPoses[kidx].rows() / 4; ++m) {
                                const Eigen::Vector3f tgt = queryPoses[kidx].block<3, 1>(m * 4, 3);
                                const Eigen::Matrix3f Rgt = queryPoses[kidx].block<3, 3>(m * 4, 0);
                                // Pose errors
                                const float terrl = (tdet - tgt).norm();
                                float acosarg;
                                if (gtAxis == "x")
                                    acosarg = Rdet.col(0).dot(Rgt.col(0));
                                else if (gtAxis == "y")
                                    acosarg = Rdet.col(1).dot(Rgt.col(1));
                                else if (gtAxis == "z")
                                    acosarg = Rdet.col(2).dot(Rgt.col(2));
                                else
                                    acosarg = 0.5f * ((Rdet.transpose() * Rgt).trace() - 1.0f); // In [-1,1]
                                float Rerrl = (acosarg <= -1 ? M_PI : (acosarg >= 1 ? 0 : acos(acosarg)));
                                Rerrl *= 180.0f / M_PI;
                                if (!gtPosesMarked[m] && terrl < terr && Rerrl < Rerr) {
                                    terr = terrl;
                                    Rerr = Rerrl;
                                    idxmin = m;
                                }
                            }
                            if(idxmin >= 0)
                                gtPosesMarked[idxmin] = true;

                            // Report pose errors
                            if (terr <= translationTol && Rerr <= rotationTol) { // Pose errors small, we're good
                                if (verbose)
                                    COVIS_MSG("\t- and pose is good! Translation/rotation error: [metric/deg]: " <<
                                                                                                                 terr
                                                                                                                 << "/"
                                                                                                                 << Rerr);
                            } else {
                                if (verbose)
                                    COVIS_MSG_WARN(
                                            "\t- but with bad pose! Translation/rotation error: [metric/deg]: " <<
                                                                                                                terr
                                                                                                                << "/"
                                                                                                                << Rerr);
                            }
                            // Update pose errors in detection output
                            detectionOutputs[j](rowidx, 2) = terr;
                            detectionOutputs[j](rowidx, 3) = Rerr;
                        } else { // Detected, but not present
                            if (verbose)
                                COVIS_MSG_WARN("FALSE POSITIVE for object " << queryName << "!");
                        }
                    } else { // Not detected
                        if (d)
                            COVIS_MSG_WARN(d);
                        if (queryMask[kidx]) { // Not detected, but present
                            if (verbose)
                                COVIS_MSG_WARN("FALSE NEGATIVE for object " << queryName << "!");
                        } else { // Not detected and not present
                            if (verbose)
                                COVIS_MSG_INFO("TRUE NEGATIVE for object " << queryName << "!");
                        }
                    }
                } // End loop over all detections of current object (l, rowidx)

                if (verbose)
                    t->intermediate(queryName);
            } // End loop over objects for detection (k)

            // Store timings for scene: [decimation seeds features projection matching fusion detection refinement segmentation]
            const int rowidx = detectionTimings[j].rows();
            detectionTimings[j].conservativeResize(rowidx + 1, 9);
            detectionTimings[j](rowidx, 0) = decimationTiming;
            detectionTimings[j](rowidx, 1) = seedTiming;
            if (fusion) {
#define MAX3(a, b, c) (a > b ? a : (a > c ? a : c)) // Max of 3 values
#define MAX3V(v, idx1, idx2, idx3) MAX3(v[idx1], v[idx2], v[idx3]) // Max of three indexed elements in v
                const size_t idx1 = fusionCombinations[j].get<0>();
                const size_t idx2 = fusionCombinations[j].get<1>();
                const size_t idx3 = fusionCombinations[j].get<2>();
                detectionTimings[j](rowidx, 2) = MAX3V(featureTiming, idx1, idx2, idx3);
                detectionTimings[j](rowidx, 3) = MAX3V(projectionTiming, idx1, idx2, idx3);
                detectionTimings[j](rowidx, 4) = MAX3V(matchTiming, idx1, idx2, idx3);
            } else {
                detectionTimings[j](rowidx, 2) = featureTiming[j];
                detectionTimings[j](rowidx, 3) = projectionTiming[j];
                detectionTimings[j](rowidx, 4) = matchTiming[j];
            }
            detectionTimings[j](rowidx, 5) = fusionTiming[j];
            detectionTimings[j](rowidx, 6) = detectionTiming[j];
            detectionTimings[j](rowidx, 7) = refinementTiming[j];
            detectionTimings[j](rowidx, 8) = segmentationTiming[j];

            // Stop timer before visualization below
            t.reset();

            // Show detections
            if (detectionsAccepted.empty()) {
                if (verbose)
                    COVIS_MSG_ERROR("No accepted detections for feature " << fnamesDetection[j] << "!");
            } else {
                if (visualize) {
                    visu::DetectionVisu<PointT> dvisu;
                    dvisu.setBackgroundColor(255, 255, 255);
                    dvisu.setTitle("Detections for " + fnamesDetection[j] + " in scene " + pscn);
                    dvisu.setQueries(meshq);
                    dvisu.setTarget(mesht);
                    dvisu.setDetections(detectionsAccepted);
                    if (fs::exists("camera_parameters.cam"))
                        dvisu.visu.loadCameraParameters("camera_parameters.cam");
                    else
                        dvisu.visu.saveCameraParameters("camera_parameters.cam");
                    dvisu.show();
                } else {
                    if (verbose)
                        COVIS_MSG_INFO(detectionsAccepted.size() << " accepted detections for feature " << fnamesDetection[j]);
                }
            }
        } // End loop over feature types to use for detection (j)

        // Report precision/recall so far
        if (verbose) {
            COVIS_MSG_INFO("PRECISION/RECALL/F1 SO FAR");
            for (size_t j = 0; j < fnumDetection; ++j) {
                const size_t positives = detectionOutputs[j].col(1).sum();
                size_t truePositives = 0;
                size_t falsePositives = 0;
                for (int r = 0; r < detectionOutputs[j].rows(); ++r) {
                    if (detectionOutputs[j](r, 0) > 0) { // 1 if detected
                        if (detectionOutputs[j](r, 1) > 0 && // 1 if present
                            detectionOutputs[j](r, 2) <= translationTol &&
                            detectionOutputs[j](r, 3) <= rotationTol)
                            ++truePositives;
                        else
                            ++falsePositives;
                    }
                }
                const float precision = float(truePositives) / float(truePositives + falsePositives);
                const float recall = (positives > 0 ? float(truePositives) / float(positives) : 0);
                COVIS_MSG("\t" << fnamesDetection[j] << ":\t"
                               << std::setprecision(3) << precision << "/"
                               << std::setprecision(3) << recall << "/"
                               << std::setprecision(3) << 2*precision*recall/(precision+recall) << "\t"
                               "(TPs: " << truePositives << ", FPs: " << falsePositives << ", positives: " << positives << ")"
                );
            }
        }

    } // End loop over all scenes (i)

    /*
     * Report overall recognition results
     */
    if (verbose) {
        COVIS_MSG_INFO("OVERALL PRECISION/RECALL/F1");

        for (size_t j = 0; j < fnumDetection; ++j) {
            const size_t positives = detectionOutputs[j].col(1).sum();
            size_t truePositives = 0;
            size_t falsePositives = 0;
            for (int r = 0; r < detectionOutputs[j].rows(); ++r) {
                if (detectionOutputs[j](r, 0) > 0) { // 1 if detected
                    if (detectionOutputs[j](r, 1) > 0 && // 1 if present
                        detectionOutputs[j](r, 2) <= translationTol &&
                        detectionOutputs[j](r, 3) <= rotationTol)
                        ++truePositives;
                    else
                        ++falsePositives;
                }
            }

            const float precision = float(truePositives) / float(truePositives + falsePositives);
            const float recall = (positives > 0 ? float(truePositives) / float(positives) : 0);
            COVIS_MSG("\t" << fnamesDetection[j] << ": "
                           << std::setprecision(3) << precision << "/"
                           << std::setprecision(3) << recall << "/"
                           << std::setprecision(3) << 2*precision*recall/(precision+recall));
        }
    }

    /*
     * Store output files
     */
    if (!dryrun) {
        const std::string suffix = ".txt";
        for (size_t j = 0; j < fnumDetection; ++j) {
            util::saveEigen(outputDir + "/detection_output_" + fnamesDetection[j] + suffix, detectionOutputs[j], true,
                        false, append);
            util::saveEigen(outputDir + "/meta_detection_timings_" + fnamesDetection[j] + suffix, detectionTimings[j], true,
                        false, append);
        }
    }

    return 0;
}
