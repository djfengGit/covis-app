#!/bin/bash

########################################################################
# Test script for the UWA dataset, all models are in [mm] in this case #
########################################################################

# General
dec=0.125 # Decimation

# Features
features=all
rad=10,7.5,20,12.5,10,17.5,12.5,25 # Feature radius multiplier

res=2.5 # Feature resolution multiplier, scenes
resq=2.5 # Feature resolution multiplier, objects
#res=1.5 # Feature resolution multiplier, scenes
#resq=1.5 # Feature resolution multiplier, objects

# Recognition
#rec_method=voting
rec_method=ransac
bw=10; # Absolute translation bandwidth
bwrot=22.5; # Absolute rotation bandwidth in ]0,360]
ransac_iter=1000 # RANSAC iterations
corr_frac=1 # Fraction of best correspondences to include in RANSAC
inlier_frac=0.05 # Minimal required inlier fraction for detections
outlier_frac=1 # Maximal allowed outlier fraction for detections
kde_thres=0 # Required KDE for voting
icp_iter=50 # ICP iterations
thres=5 # Inlier threshold multiplier, RANSAC+ICP, set to <= 0 for using 1 x mesh resolution

# Pose tolerances (metric and degrees)
trans_tol=50
rot_tol=10

# Paths
exec_dir=build
exec_name=feature_matching_recognition
data_dir=~/workspace/datasets/uwa
output_dir=output/uwa_rec

# Positionals
objects=`ls $data_dir/models/*.ply -v1 | grep -v rhino`
scenes=`ls $data_dir/scenes/*.ply -v1`
pose_dir=$data_dir/GroundTruth_3Dscenes
pose_separator="-"
pose_suffix=xf

# Options and flags
options="--pose-separator=$pose_separator --decimation=$dec --radius=$rad --resolution=$res --resolution-query=$resq --threshold=$thres --features=$features"
options_rec="--recognition-method=$rec_method --ransac-iterations=$ransac_iter --bandwidth-translation=$bw --bandwidth-rotation=$bwrot --inlier-fraction=$inlier_frac --outlier-fraction=$outlier_frac --kde-threshold=$kde_thres --icp-iterations=$icp_iter --correspondence-fraction=$corr_frac --translation-tolerance=$trans_tol --rotation-tolerance=$rot_tol"
flags="--pose-file-model-first --verbose"

# Start
$exec_dir/$exec_name "$objects" "$scenes" $pose_dir $pose_suffix $output_dir $options $options_rec $flags $* 2> /dev/null

#gdb -ex r --args $exec_dir/$exec_name "$objects" "$scenes" $pose_dir $pose_suffix $output_dir $options $options_rec $flags $*
