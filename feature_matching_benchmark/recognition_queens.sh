#!/bin/bash

#######################################
# Test script for the Queen's dataset #
#######################################

# General
dec=0.75 # Decimation

# Features
features=all
rad=10,7.5,22.5,12.5,12.5,20,12.5,22.5 # Feature radius multiplier

res=2.5 # Feature resolution multiplier, scenes
resq=2.5 # Feature resolution multiplier, objects
#res=1 # Feature resolution multiplier, scenes
#resq=1 # Feature resolution multiplier, objects


# Recognition
rec_method=ransac
bw=0.01; # Absolute translation bandwidth
bwrot=22.5; # Absolute rotation bandwidth in ]0,360]
ransac_iter=1000 # RANSAC iterations
corr_frac=1 # Fraction of best correspondences to include in RANSAC
inlier_frac=0.05 # Minimal required inlier fraction for detections
outlier_frac=1 # Maximal allowed outlier fraction for detections
kde_thres=10 # Required KDE for voting, multiplicative
icp_iter=50 # ICP iterations
thres=5 # Inlier threshold multiplier, RANSAC+ICP, set to <= 0 for using 1 x mesh resolution

# Pose tolerances (metric and degrees)
trans_tol=0.05
rot_tol=10

# Paths
exec_dir=build
exec_name=feature_matching_recognition
data_dir=~/workspace/datasets/queens
output_dir=output/queens_rec

# Positionals
objects=`ls $data_dir/models_reconstructed/*.ply -v1`
scenes=`ls $data_dir/lidar_point_clouds_reconstructed/*.ply -v1`
pose_dir=$data_dir/ground_truth_poses
#pose_dir=$data_dir/aldoma_ground_truth_poses_converted
pose_separator="_"
pose_suffix=txt

# Options and flags
options="--pose-separator=$pose_separator --decimation=$dec --radius=$rad --resolution=$res --resolution-query=$resq --threshold=$thres --features=$features"
options_rec="--recognition-method=$rec_method --ransac-iterations=$ransac_iter --bandwidth-translation=$bw --bandwidth-rotation=$bwrot --inlier-fraction=$inlier_frac --outlier-fraction=$outlier_frac --kde-threshold=$kde_thres --icp-iterations=$icp_iter --correspondence-fraction=$corr_frac --translation-tolerance=$trans_tol --rotation-tolerance=$rot_tol"
flags="--verbose"

# Start
$exec_dir/$exec_name "$objects" "$scenes" $pose_dir $pose_suffix $output_dir $options $options_rec $flags $* 2> /dev/null

#gdb -ex r --args $exec_dir/$exec_name "$objects" "$scenes" $pose_dir $pose_suffix $output_dir $options $options_rec $flags $* 2> /dev/null
