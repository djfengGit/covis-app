#!/bin/bash

########################################################################
# Test script for the 3DV dataset
########################################################################

# Overall 3DV settings
#object="" # All
object=Angel
#object=Birds
#object=Rabbit

# Test parameters
dec=0.25 # Decimation
#feat=all
## UWA
#rad=10,7.5,20,12.5,10,17.5,12.5,25 # Feature radius multiplier - ecsad,fpfh,ndhist,rops,shot,si,usc,ppfhist
## Tuning
#feat=ecsad,ndhist,shot,si
#rad=7.5,15,10,10
# Post tuning (Angel)
feat=all
rad=7.5,7.5,15,10,10,10,10,15
metric=L2_RATIO
res=5 # Feature resolution multiplier
thres=1 # Inlier threshold multipler, set to <= 0 for using 1 x mesh resolution

# Paths
exec_dir=build
exec_name=feature_matching_dataset
data_dir=~/workspace/datasets/3dv
output_dir=output/3dv/$object
if [ ! -d $output_dir ]; then mkdir -p $output_dir; fi

# Positionals
objects=`ls $data_dir/models/decimated_0.05/*.ply -v1 | grep $object`

scenes_all=`find $data_dir/scenes/decimated_0.25/ -type f | sort`
scenes_all_file=$output_dir/scenes_all.txt
echo "$scenes_all" > $scenes_all_file

pose_dir=$data_dir/ground_truth
pose_separator="-"
pose_suffix=xf

# Options and flags
options="--features=$feat --pose-separator=$pose_separator --decimation=$dec --radius=$rad --resolution=$res --threshold=$thres --metrics=$metric"
flags="--no-decimate-scenes --match-scene-objects --verbose"
#flags="--verbose"

# Start
$exec_dir/$exec_name "$objects" "$scenes_all_file" $pose_dir $pose_suffix $output_dir $options $flags $* #2> /dev/null

#gdb --args $exec_dir/$exec_name "$objects" "$scenes" $pose_dir $pose_suffix $output_dir $options $flags $*
