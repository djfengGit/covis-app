#ifndef FEATURE_MATCHING_METRICS_H
#define FEATURE_MATCHING_METRICS_H

// Own
#include "feature_matching_features.h"

// Boost
#include <boost/assign/list_of.hpp>
#include <boost/bimap.hpp>

// Covis
#include <covis/core/correspondence.h>

// FLANN
#include <flann/flann.hpp>

namespace flann {
    template<class T>
    struct ChiSquareDistance1 {
        typedef bool is_kdtree_distance;

        typedef T ElementType;
        typedef typename Accumulator<T>::Type ResultType;

        template<typename Iterator1, typename Iterator2>
        ResultType operator()(Iterator1 a, Iterator2 b, size_t size, ResultType worst_dist = -1) const {
            ResultType result = ResultType();
            ResultType sum, diff;
            Iterator1 last = a + size;

            while(a < last) {
                sum = (ResultType) (*b);
                if(sum > 0) {
                    diff = (ResultType) (*a - *b);
                    result += diff * diff / sum;
                }
                ++a;
                ++b;

                if((worst_dist > 0) && (result > worst_dist)) {
                    return result;
                }
            }
            return result;
        }

        /**
         * Partial distance, used by the kd-tree.
         */
        template<typename U, typename V>
        inline ResultType accum_dist(const U &a, const V &b, int) const {
            ResultType result = ResultType();
            ResultType sum, diff;

            sum = (ResultType) (b);
            if(sum > 0) {
                diff = (ResultType) (a - b);
                result = diff * diff / sum;
            }
            return result;
        }
    };

    template<class T>
    struct KLSymmetric
    {
        typedef bool is_kdtree_distance;

        typedef T ElementType;
        typedef typename Accumulator<T>::Type ResultType;

        /**
         *  Compute the Kullback–Leibler divergence
         */
        template <typename Iterator1, typename Iterator2>
        ResultType operator()(Iterator1 a, Iterator2 b, size_t size, ResultType worst_dist = -1) const
        {
            ResultType result = ResultType();
            Iterator1 last = a + size;

            while (a < last) {
                if (*a>0 && *b>0) {
                    ResultType loga = logf(*a);
                    ResultType logb = logf(*b);
                    result += *a * (loga-logb) + *b * (logb - loga);
                }

                ++a;
                ++b;

                if ((worst_dist>0)&&(result>worst_dist)) {
                    return result;
                }
            }
            return result;
        }

        /**
         * Partial distance, used by the kd-tree.
         */
        template <typename U, typename V>
        inline ResultType accum_dist(const U& a, const V& b, int) const
        {
            ResultType result = ResultType();
            if (a>0 && b>0) {
                ResultType loga = logf(a);
                ResultType logb = logf(b);
                result = a * (loga - logb) + b * (logb - loga);
            }

            return result;
        }
    };

    template<class T>
    struct Hist
    {
        typedef T ElementType;
        typedef typename Accumulator<T>::Type ResultType;

        /**
         *  Compute the histogram intersection distance
         */
        template <typename Iterator1, typename Iterator2>
        ResultType operator()(Iterator1 a, Iterator2 b, size_t size, ResultType worst_dist = -1) const
        {
            ResultType result = ResultType();
            ResultType min0, min1, min2, min3;
            Iterator1 last = a + size;
            Iterator1 lastgroup = last - 3;

            /* Process 4 items with each loop for efficiency. */
            while (a < lastgroup) {
                min0 = (ResultType)(a[0] < b[0] ? a[0] : b[0]);
                min1 = (ResultType)(a[1] < b[1] ? a[1] : b[1]);
                min2 = (ResultType)(a[2] < b[2] ? a[2] : b[2]);
                min3 = (ResultType)(a[3] < b[3] ? a[3] : b[3]);
                result += min0 + min1 + min2 + min3;
                a += 4;
                b += 4;
                if ((worst_dist>0)&&(result>worst_dist)) {
                    return result;
                }
            }
            /* Process last 0-3 pixels.  Not needed for standard vector lengths. */
            while (a < last) {
                min0 = (ResultType)(*a < *b ? *a : *b);
                result += min0;
                ++a;
                ++b;
            }

            assert(result <=1);

            return 1-result;
        }
    };
}

// Metric list shared among the compilation units
extern std::vector<std::string> mnames;
extern size_t mnum;

// All metrics
enum DISTANCE_METRIC {
    L1,
    L1_RATIO,
    L2,
    L2_RATIO,
    LINF,
    LINF_RATIO,
    HELLINGER,
    CHISQ,
    CHISQ_RATIO,
    CHISQ1,
    CHISQ1_RATIO,
    KL,
    KL_RATIO,
    HIST,
    HIST_RATIO,
    EMD
};

// All metric names
const std::string mnamesAll[] = {
    "L1",
    "L1_RATIO",
    "L2",
    "L2_RATIO",
    "LINF",
    "LINF_RATIO",
    "HELLINGER",
    "CHISQ",
    "CHISQ_RATIO",
    "CHISQ1",
    "CHISQ1_RATIO",
    "KL",
    "KL_RATIO",
    "HIST",
    "HIST_RATIO",
    "EMD"
};

typedef boost::bimap<std::string,DISTANCE_METRIC> MetricMap;
const MetricMap mbimap = boost::assign::list_of<MetricMap::relation>
    ("L1", L1)
    ("L1_RATIO", L1_RATIO)
    ("L2", L2)
    ("L2_RATIO", L2_RATIO)
    ("LINF", LINF)
    ("LINF_RATIO", LINF_RATIO)
    ("HELLINGER", HELLINGER)
    ("CHISQ", CHISQ)
    ("CHISQ_RATIO", CHISQ_RATIO)
    ("CHISQ1", CHISQ1)
    ("CHISQ1_RATIO", CHISQ1_RATIO)
    ("KL", KL)
    ("KL_RATIO", KL_RATIO)
        ("HIST", HIST)
        ("HIST_RATIO", HIST_RATIO)
        ("EMD", EMD)
    ;

// Metric to string
std::string m2str(DISTANCE_METRIC distanceMetric);

// String to metric
DISTANCE_METRIC str2m(std::string str);

// Match two sets of descriptors featt --> featq
covis::core::Correspondence::VecPtr match(const MatrixT& featQuery,
        const MatrixT& featTarget,
        DISTANCE_METRIC distanceMetric,
        int k = 1);

template<int DistanceMetric, typename RealT> struct MetricTraits {};
template<typename RealT> struct MetricTraits<L1,RealT> { typedef typename flann::L1<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<L1_RATIO,RealT> { typedef typename flann::L1<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<L2,RealT> { typedef typename flann::L2<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<L2_RATIO,RealT> { typedef typename flann::L2<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<LINF,RealT> { typedef typename flann::MaxDistance<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<LINF_RATIO,RealT> { typedef typename flann::MaxDistance<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<HELLINGER,RealT> { typedef typename flann::L2<RealT> FlannMetric; static const bool IsRatio = false; }; // See match() in source file
template<typename RealT> struct MetricTraits<CHISQ,RealT> { typedef typename flann::ChiSquareDistance<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<CHISQ_RATIO,RealT> { typedef typename flann::ChiSquareDistance<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<CHISQ1,RealT> { typedef typename flann::ChiSquareDistance1<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<CHISQ1_RATIO,RealT> { typedef typename flann::ChiSquareDistance1<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<KL,RealT> { typedef typename flann::KLSymmetric<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<KL_RATIO,RealT> { typedef typename flann::KLSymmetric<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<HIST,RealT> { typedef typename flann::Hist<RealT> FlannMetric; static const bool IsRatio = false; };
template<typename RealT> struct MetricTraits<HIST_RATIO,RealT> { typedef typename flann::Hist<RealT> FlannMetric; static const bool IsRatio = true; };
template<typename RealT> struct MetricTraits<EMD,RealT> { typedef typename flann::L1<RealT> FlannMetric; static const bool IsRatio = false; }; // See match() in source file

template<int DistanceMetric>
struct Matcher {
    // Promote the typedefs for the distance metric
    typedef typename MetricTraits<DistanceMetric, float>::FlannMetric Metric;
    static const bool IsRatio = MetricTraits<DistanceMetric, float>::IsRatio;
    
    // Multiple randomized trees
    typedef typename flann::KDTreeIndex<Metric> KDTree;
    typedef typename boost::shared_ptr<KDTree> KDTreePtr;
    
    // Create an index with specified number of randomized trees
    static KDTreePtr index(const MatrixT& feat, int trees = 4) {
        flann::Matrix<float> mfeat(const_cast<float*>(feat.data()), feat.rows(), feat.cols());
        KDTreePtr result(new KDTree(mfeat, flann::KDTreeIndexParams(trees)));
        result->buildIndex();
        
        return result;
    }
    
    // Linear matching
    static covis::core::Correspondence::VecPtr match(const MatrixT& featQuery, const MatrixT& featTarget, int k = 1) {
        COVIS_ASSERT(featQuery.cols() == featTarget.cols());
        COVIS_ASSERT(k > 0);
        if(IsRatio)
            COVIS_ASSERT_MSG(k < 3, "Searching for k > 2 NNs makes no sense for ratio matching!");

        flann::Matrix<float> mquery(const_cast<float*>(featQuery.data()), featQuery.rows(), featQuery.cols());
        flann::Matrix<float> mtarget(const_cast<float*>(featTarget.data()), featTarget.rows(), featTarget.cols());
        flann::LinearIndex<typename MetricTraits<DistanceMetric, float>::FlannMetric> index(mtarget);
        
        std::vector<std::vector<size_t> > idx;
        std::vector<std::vector<float> > dist;
        
        flann::SearchParams param;
        param.cores = 0;
        index.knnSearch(mquery, idx, dist, (IsRatio ? 2 : k), param);

        covis::core::Correspondence::VecPtr result(
                new covis::core::Correspondence::Vec(mquery.rows, covis::core::Correspondence((IsRatio ? 1 : k))));
        for(size_t i = 0; i < mquery.rows; ++i) {
            (*result)[i].query = i;
            if(IsRatio) {
                (*result)[i].match[0] = idx[i][0];
                (*result)[i].distance[0] = dist[i][0] / dist[i][1];
            } else {
                for(int j = 0; j < k; ++j) {
                    (*result)[i].match[j] = idx[i][j];
                    (*result)[i].distance[j] = dist[i][j];
                }
            }
        }
        
        return result;
    }

    // Find approximate NNs using an existing index
    static covis::core::Correspondence::VecPtr knn(const KDTree& index, const MatrixT& featQuery, int checks = 512, int k = 1) {
        COVIS_ASSERT(k > 0);
        if(IsRatio)
            COVIS_ASSERT_MSG(k < 3, "Searching for k > 2 NNs makes no sense for ratio matching!");
        // Map to FLANN matrix
        flann::Matrix<float> mquery(const_cast<float*>(featQuery.data()), featQuery.rows(), featQuery.cols());
        // Prepare output
        std::vector<std::vector<size_t> > idx;
        std::vector<std::vector<float> > dist;
        // Search
        index.knnSearch(mquery, idx, dist, (IsRatio ? 2 : k), flann::SearchParams(checks <= 0 ? -1 : checks));
        // Convert
        covis::core::Correspondence::VecPtr result(
                new covis::core::Correspondence::Vec(mquery.rows, covis::core::Correspondence(IsRatio ? 1 : k)));
        for(size_t i = 0; i < mquery.rows; ++i) {
            (*result)[i].query = i;
            if(IsRatio) {
                (*result)[i].match[0] = idx[i][0];
                (*result)[i].distance[0] = dist[i][0] / dist[i][1];
            } else {
                for(int j = 0; j < k; ++j) {
                    (*result)[i].match[j] = idx[i][j];
                    (*result)[i].distance[j] = dist[i][j];
                }
            }
        }

        return result;
    }
};

#endif
