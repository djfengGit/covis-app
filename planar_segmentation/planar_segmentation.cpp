// STL
#include <sstream>

// PCL
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

using namespace pcl;
using namespace std;

int main(int argc, char** argv) {
    // Get inputs
    if(argc != 4) {
        cout << "Usage: " << argv[0] << " <input.pcd> <output.pcd> <distance threshold>" << endl;
        return 0;
    }
    
    // Get input point
    cout << "Loading point cloud " << argv[1] << "..." << endl;
    PCLPointCloud2Ptr cloudIn(new PCLPointCloud2);
    io::loadPCDFile(argv[1], *cloudIn);
    
    // Get distance threshold
    const double thres = atof(argv[3]);
    assert(thres >= 0.0);
    
    cout << "Using distance threshold: " << thres << endl;
    
    // A PointT cloud is needed for SACSegmentation
    PointCloud<PointXYZ>::Ptr tmp(new PointCloud<PointXYZ>);
    fromPCLPointCloud2(*cloudIn, *tmp);
    
    // Plane coefficients
    ModelCoefficients::Ptr coefficients (new ModelCoefficients);
    PointIndices::Ptr inliers(new PointIndices);
    // Create the segmentation object
    SACSegmentation<PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType(SACMODEL_PLANE);
    seg.setMethodType(SAC_RANSAC);
    seg.setDistanceThreshold(thres);
    // Find plane
    seg.setInputCloud(tmp);
    seg.segment(*inliers, *coefficients);

    if(inliers->indices.empty()) {
        cerr << "Could not estimate a planar model for the given dataset!" << endl;
        return -1;
    }
    
    // Output point cloud
    PCLPointCloud2Ptr cloudOut(new PCLPointCloud2);
    
    // Extract all non-plane points
    ExtractIndices<PCLPointCloud2> ei;
    ei.setInputCloud(cloudIn);
    ei.setIndices(inliers);
    ei.setNegative(true);
    ei.filter(*cloudOut);
    
    // Save output
    cout << "Saving filtered point cloud to " << argv[2] << "..." << endl;
    io::savePCDFile(argv[2], *cloudOut, Eigen::Vector4f::Zero(), Eigen::Quaternionf::Identity(), true);
    
    return 0;
}
