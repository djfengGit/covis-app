##
# Require CMake
##
cmake_minimum_required(VERSION 2.8)

##
# Project
##
project(planar_segmentation)

##
# Find dependencies
##
find_package(PCL)
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

##
# Add target
##
add_executable(planar_segmentation planar_segmentation.cpp)
target_link_libraries(planar_segmentation ${PCL_LIBRARIES})

