import h5py
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import LinearSVC, SVC
import timeit

from plotconfusion import plotconfusion

infile = 'training-data/templates0.2Balanced.h5'

# Load
print('Loading...')
h5fx = h5py.File(infile, 'r')
X = h5fx['X'][:]  # Features, row-major
Y = h5fx['Y'][:]  # One-hot labels, row-major
assert X.shape[0] == Y.shape[0]

# Normalization
print('Scaling...')
scaler = MinMaxScaler()
scaler.fit(X)
X = scaler.transform(X)

# Integer labels
Ytrue = np.argmax(Y, axis=1)

print('Splitting...')
Xtrain, Xval, Ytrain, Yval = train_test_split(X, Ytrue, test_size=0.1, random_state=0)

# Train
print('Training...')
startTime = timeit.default_timer()

print('\tRF classifier')
clf = RandomForestClassifier(n_estimators=32,
                             max_depth=None,
                             random_state=0,
                             n_jobs=-1,
                             verbose=2)

# print('\tLinear SVM classifier')
# clf = LinearSVC(random_state=0,
#                 verbose=2)
#
# print('\tNon-linear one-vs-one classifier')
# clf = SVC(decision_function_shape='ovo',
#           max_iter=100,
#           random_state=0,
#           cache_size=1000,
#           verbose=2)
#
# print('\tNon-linear one-vs-rest classifier')
# clf = SVC(decision_function_shape='ovr',
#           max_iter=100,
#           random_state=0,
#           verbose=2)

# print('\tMLP classifier')
# clf = MLPClassifier(hidden_layer_sizes=(1024,128,16),
#                     max_iter=100,
#                     tol=1e-4,
#                     random_state=0,
#                     verbose=True)



clf.fit(Xtrain, Ytrain)
#joblib.dump(clf, 'classifier.pkl')
print('\tTraining time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

# Test
print('Predicting...')
startTime = timeit.default_timer()
Ypred = clf.predict(Xval)
elapsed = timeit.default_timer() - startTime
print('\tPrediction time: {:.3f} seconds ({:e} seconds per sample)'.format(elapsed, elapsed / Xval.shape[0]))
plotconfusion(confusion_matrix(Yval, Ypred), title='Val')

# Show also train error
plotconfusion(confusion_matrix(Ytrain, clf.predict(Xtrain)), title='Train')
