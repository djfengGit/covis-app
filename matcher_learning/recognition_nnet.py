#!/usr/bin/env python

import argparse, os, timeit, numpy, re
import math
import matplotlib.cm
from sklearn.externals import joblib
import yaml

import sys
sys.path.append('../../covis/build/lib')

from covis import *


# Setup program options
po = argparse.ArgumentParser()

po.add_argument("root", type=str, help="root path of your dataset")
po.add_argument("--object-dir", '-o', default="", type=str, help="subdirectory for the object models")
po.add_argument("--scene-dir", '-s', default="", type=str, help="subdirectory for the scene models")
po.add_argument("--pose-dir", '-p', default="", type=str, help="subdirectory for the ground truth pose models")
po.add_argument("--object-ext", default="", type=str, help="object file extension")
po.add_argument("--scene-ext", default="", type=str, help="scene file extension")
po.add_argument("--pose-ext", default="", type=str, help="pose file extension")
po.add_argument("--pose-sep", default="", type=str, help="pose file separator")
po.add_argument("--object-regex", default="", type=str, help="set this option to use a regular expression search when collecting object files")
po.add_argument("--scene-regex", default="", type=str, help="set this option to use a regular expression search when collecting scene files")
po.add_argument("--scene-nth", default=1, type=int, help="set this value to > 1 to process every n'th scene")
po.add_argument("--scene-offset", default=0, type=int, help="set the start scene index")

po.add_argument("--query-scale", default=1, type=float, help="sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates")
po.add_argument("--resolution-surface", '-r', default=0, type=float, help="downsample point clouds to this resolution (<= 0 for the average object resolution)")
po.add_argument("--far", default=0, type=float, help="do not consider target points beyond this depth (0 for disabled)")
po.add_argument("--radius-normal", default=10, type=float, help="normal estimation radius as a multiplum of the resolution")
po.add_argument("--resolution-feature", default=5, type=float, help="resolution of features in mr")
po.add_argument("--feature", '-f', default="ppfhistfull", type=str, help="name the feature to compute - possible names are: ecsad,fpfh,ndhist,ppfhist,ppfhistfull,si,usc")
po.add_argument("--radius-feature", default=0.25, type=float, help="feature estimation radius - if > 1 as a multiplum of the resolution, else if < 1 as a multiplum of the average object diagonal")
po.add_argument("--pca", default=1, type=float, help="set to a number > 0 and < 1 to enable PCA compression with this energy")

po.add_argument("--knn", '-k', default=1, type=int, help="specifiy number of neighbors to search for during matching")
po.add_argument("--trees", default=4, type=int, help="set the number of trees for approximate high-dimensional feature search")
po.add_argument("--checks", default=512, type=int, help="set the number of checks for approximate high-dimensional feature search")

po.add_argument("--net-classifier", default="", type=str, help="specify filename for point classifier network - this net can be used both in combination with baseline k-NN search and network-based matching")
po.add_argument("--labels", default="", type=str, help="specify order of object labels (file name stems) produced by the classifier, e.g. T-rex,chef,chicken,parasaurolophus,rhino")

po.add_argument("--use-soft-predictions", action='store_true', help="set to true to use soft predictions (see also --soft-label)")
po.add_argument("--soft-label", default='', type=str, help="specify label for doing soft net predictions, leave empty to use first label")
po.add_argument("--prediction-threshold", '-t', default=0.1, type=float, help="specify lower net prediction threshold in range [0,1]")
po.add_argument("--use-gpu", '-u', action='store_true', help="set to true to use GPU for the matcher network")

po.add_argument("--use-ransac", action='store_true', help="set to true to use RANSAC instead of pose voting")
po.add_argument("--translation-bandwidth", default=10, type=float, help="set translation bandwidth in mr")
po.add_argument("--multi-instance", default=1, type=int, help="specifiy number of instances to search for during recognition")
po.add_argument("--iterations-icp", default=50, type=int, help="specifiy number of ICP refinement iterations")

po.add_argument("--visualize", '-z', action='store_true', help="set to true to show detections")
po.add_argument("--save-poses", action='store_true', help="set to true to save poses")
po.add_argument("--save-scores", action='store_true', help="set to true to save scores")
po.add_argument("--save-sixdb-output", action='store_true', help="set to true to save poses/scores in sixdb format")
po.add_argument("--output-dir", default='.', type=str, help="output directory for poses/scores")

args = po.parse_args()

print('Loading dataset from root {}...'.format(args.root))
dataset = util.loadDataset(args.root, args.object_dir, args.scene_dir, args.pose_dir, args.object_ext, args.scene_ext, args.pose_ext, args.pose_sep, args.object_regex, args.scene_regex)
print('\tGot {} model(s), {} scenes and {} poses'.format(len(dataset.objectLabels), len(dataset.sceneLabels), len(dataset.poseLabels)))
objects = dataset.objects

if len(args.net_classifier):
    sys.path.append('keras-code')
    from LoadKerasModelAndPredictEssentials import load

if len(args.net_classifier):
    labelsOrdered = re.split(' |,|;|\t|\n', args.labels) if args.labels else []
    numObjectLabels = len(dataset.objectLabels)
    if len(labelsOrdered) == 0 and numObjectLabels == 1:
        labelsOrdered = [dataset.objectLabels[0], 'background']
    assert numObjectLabels <= len(labelsOrdered)
    # Mapping from net index --> loaded index, so labelMap(net_index) = loaded_index
    labelMap = [None] * len(labelsOrdered)
    cnt = len(dataset.objectLabels)
    for j in range(len(labelsOrdered)):
        if dataset.objectLabels.count(labelsOrdered[j]):
            labelMap[j] = dataset.objectLabels.index(labelsOrdered[j])
        else:
            labelMap[j] = cnt
            cnt += 1
    # Mapping from loaded index --> net index
    labelMapInv = [None] * len(labelMap)
    for j in range(len(labelMap)):
        # if labelMap[j] is not None:
        labelMapInv[labelMap[j]] = j

    print('Interpreting output classifications as follows:')
    for j in range(len(dataset.objectLabels)):
        print('\tIndex {}: {}'.format(labelMapInv[j], dataset.objectLabels[j]))

# Surface scaling
queryScale = args.query_scale
resolution = args.resolution_surface
resolutionInput = resolution > 0
if not resolutionInput:
    resolution = 0
diag = 0
for i in range(len(objects)):
    if not resolutionInput:
        resolution += detect.computeResolution(objects[i]) * queryScale
    diag += detect.computeDiagonal(objects[i]) * queryScale
if not resolutionInput:
    resolution /= len(objects)
diag /= len(objects)
frad = args.radius_feature * resolution if args.radius_feature > 1 else args.radius_feature * diag

objectSurf = []
objectCloud = []
sys.stdout.write('Preprocessing '),sys.stdout.flush()
for i in range(len(objects)):
    sys.stdout.write(dataset.objectLabels[i] + ' '),sys.stdout.flush()
    objectSurf.append(filter.preprocess(mesh=objects[i],
                                        scale=queryScale,
                                        resolution=resolution,
                                        normalRadius=args.radius_normal * resolution,
                                        orientNormals=True))

    # Generate feature points
    objectCloud.append(filter.downsample(cloud=objectSurf[i], resolution=args.resolution_feature * resolution))
sys.stdout.write('\n'),sys.stdout.flush()

# Compute features
print('Computing {} object features with a radius of {:.3f}...'.format(sum([c.width * c.height for c in objectCloud]), frad))
objectFeat = feature.computeFeature(name=args.feature,
                                    clouds=objectCloud,
                                    surfaces=objectSurf,
                                    radius=frad)
def pcaTrain(m, energy):
    c = numpy.mean(m, axis=1) # A column vector of row means
    c = c[:,numpy.newaxis]
    mc = m - c
    mcov = numpy.matmul(mc, numpy.transpose(mc))
    values, vectors = numpy.linalg.eig(mcov)
    values /= numpy.sum(numpy.real(values))
    ascend = values.argsort()
    values = values[ascend]
    vectors = numpy.real(vectors[:,ascend])

    if energy > 1:
        return (c, vectors[:, -int(round(energy)):])
    else:
        i = -1
        sum = values[i]
        while sum <= energy:
            i -= 1
            sum += values[i]
        i += 1
        return (c, vectors[:, i:])
        
def pcaProject(m, pca):
    return numpy.matmul(numpy.transpose(pca[1]), (m - pca[0]))

if 0 < args.pca < 1:
    print('\tLearning a PCA compression from {} features...'.format(objectFeat.shape[1]))
    pca = pcaTrain(objectFeat, args.pca)
    objectFeat = pcaProject(objectFeat, pca)
    

# Create search and recognition object
print('Setting up search...')
objectSearch = detect.EigenSearch()
objectSearch.trees = args.trees
objectSearch.checks = args.checks
objectSearch.target = objectFeat

print('Setting up recognition...')
rec = detect.Recognition()
rec.multiInstance = args.multi_instance
rec.refinementIterations = args.iterations_icp
rec.sources = objectCloud
rec.sourceNames = dataset.objectLabels
rec.sourceFeatures = objectFeat
rec.inlierThreshold = 5 * resolution
rec.verbose = True
if args.use_ransac:
    rec.detector = detect.Recognition.DETECTOR.RANSAC
    rec.ransacIterations = 10000
    rec.inlierFraction = 0
else:
    rec.detector = detect.Recognition.DETECTOR.VOTING
    rec.translationBandwidth = args.translation_bandwidth * resolution
    rec.kdeThreshold = 0

# Loop over scenes
totalPositives = 0
totalRetrieved = 0
totalInliers = 0
cnt = 0 # Relative scene count
for i in range(args.scene_offset, dataset.size, args.scene_nth):
    scene = dataset.at(i)
    sceneMesh = scene.scene

    print('Processing scene {}/{} ({})...'.format(i+1, dataset.size, scene.label))

    startTimeScene = timeit.default_timer()
    print('Preprocessing scene...')
    sceneSurf = filter.preprocess(mesh=sceneMesh,
                                  resolution=resolution,
                                  far=args.far,
                                  normalRadius=args.radius_normal * resolution)

    sceneCloud = filter.downsample(cloud=sceneSurf, resolution=args.resolution_feature * resolution)

    print('Computing {} scene features...'.format(sceneCloud.size))
    sceneFeat = feature.computeFeature(name=args.feature,
                                       cloud=sceneCloud,
                                       surface=sceneSurf,
                                       radius=frad)

    if 0 < args.pca < 1:
        print('\tCompressing using PCA...')
        sceneFeat = pcaProject(sceneFeat, pca)

    numQuery = sceneFeat.shape[1]
    numTarget = objectFeat.shape[1]
    objectFeatTranspose = numpy.transpose(objectFeat)
    sceneFeatTranspose = numpy.transpose(sceneFeat)

    print('Preprocessing and feature computation time: {:.3f} seconds'.format(timeit.default_timer() - startTimeScene))

    predictions = []
    if len(args.net_classifier): # Using classifier net
        startTime = timeit.default_timer()
        root, ext = os.path.splitext(args.net_classifier)
        numQuery = sceneFeat.shape[1]
        if ext.lower() == '.h5':
            print('Using TF classifier for {} scene features...'.format(numQuery))
            if args.use_gpu:
                print('\tUsing GPU')

            netClassifier = load(args.net_classifier, args.use_gpu)
            predictions = netClassifier.predict(sceneFeatTranspose, batch_size=numQuery)
        elif ext.lower() == '.pkl':
            print('Using sklearn classifier for {} scene features...'.format(numQuery))
            clf = joblib.load(args.net_classifier)
            scaler = joblib.load(root + '-scaler.pkl')
            predictions = clf.predict_proba(scaler.transform(sceneFeatTranspose))
        else:
            raise 'Unknown extension for file: {}'.format(args.net_classifier)
        assert predictions.shape[0] == sceneFeat.shape[1]
        # assert predictions.shape[1] == len(dataset.objectLabels)
        predictions = predictions[:,labelMapInv]
        threshold = args.prediction_threshold
        
        if args.use_soft_predictions:
            softLabel = args.soft_label if len(args.soft_label) else dataset.objectLabels[0]
            softLabelIndex = labelsOrdered.index(softLabel)
            print('USING SOFT PREDICTIONS OF {} (index {})'.format(softLabel, softLabelIndex))
            classifications = numpy.full((predictions.shape[0]), softLabelIndex, dtype=int)
            scores = predictions[:, softLabelIndex]
        else:
            print('USING HARD CLASSIFICATIONS')
            classifications = numpy.argmax(predictions, axis=1)
            scores = numpy.max(predictions, axis=1)

        cnt = 0
        for row in range(classifications.shape[0]):
            if scores[row] < threshold:
                classifications[row] = -1
                cnt += 1
        print('\t\tRemoved {} points with too low prediction score'.format(cnt))

        print('\tClassification time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

        print('Using the labels to constrain the matching...')
        objectFeatSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        objectFeatSeperatedIdx = [numpy.empty([0], dtype=int) for o in objectCloud]
        offset = 0
        for j in range(len(objectCloud)):
            objectFeatSeperated[j] = objectFeat[:,offset:offset+objectCloud[j].size]
            objectFeatSeperatedIdx[j] = numpy.arange(offset, offset+objectCloud[j].size)
            offset += objectCloud[j].size

        sceneCloudArray = sceneCloud.array()
        sceneCloudSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        sceneFeatSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        sceneFeatSeperatedIdx = [numpy.empty([0], dtype=int) for o in objectCloud]

        sceneRange = numpy.arange(len(classifications), dtype=int)
        for idx in range(len(dataset.objectLabels)):
            idxMask = classifications == idx
            sceneCloudSeperated[idx] = sceneCloudArray[:,idxMask]
            sceneFeatSeperated[idx] = sceneFeat[:,idxMask]
            sceneFeatSeperatedIdx[idx] = sceneRange[idxMask]

    startTime = timeit.default_timer()

    if len(predictions): # Using classifer+k-NN
        featureCorrSeperated = []
        for j in range(len(sceneFeatSeperated)):
            print('\tMatching {} scene points classified as object {}...'.format(sceneFeatSeperated[j].shape[1], dataset.objectLabels[j]))
            featureCorrSeperated.append(objectSearch.knn(sceneFeatSeperated[j], args.knn))

        featureCorr = []
        for j in range(len(featureCorrSeperated)):
            for k in range(len(featureCorrSeperated[j])):
                query = sceneFeatSeperatedIdx[j][k]
                for l in range(len(featureCorrSeperated[j][k].match)):
                    match = objectFeatSeperatedIdx[j][featureCorrSeperated[j][k].match[l]]
                    distance = featureCorrSeperated[j][k].distance[l]
                    featureCorr.append(core.Correspondence(query, match, distance))
    else:
        print('Using baseline k-NN matcher for {} --> {} {} features...'.format(sceneFeat.shape[1], objectFeat.shape[1], args.feature))
        featureCorr = objectSearch.knn(sceneFeat, args.knn)

    print('Total k-NN matching time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

    print('Performing pose estimation using {} correspondences...'.format(len(featureCorr)))
    rec.target = sceneCloud
    rec.targetFeatures = sceneFeat
    rec.featureCorrespondences = featureCorr
    startTimeEst = timeit.default_timer()
    detections = rec.recognize()
    print('\tPose estimation time: {:.3f} seconds'.format(timeit.default_timer() - startTimeEst))
    totalTimeScene = timeit.default_timer() - startTimeScene
    print('TOTAL PROCESSING TIME FOR SCENE: {:.3f} seconds'.format(totalTimeScene))
    if len(detections):
        print 'Got the following detections:'
        poseFiles = []
        scoreFiles = []
        for d in detections:
            print d
            if args.save_poses:
                poseFiles.append(args.output_dir + '/' + d.label + '-' + scene.label + '.txt')
                scoreFiles.append(args.output_dir + '/' + d.label + '-' + scene.label + '-scores.txt')

        if not os.path.isdir(args.output_dir) and (args.save_poses or args.save_scores or args.save_sixdb_output):
            os.makedirs(args.output_dir)
        if args.save_poses: print 'Saving poses...'
        if args.save_scores: print 'Saving scores...'
        if args.save_sixdb_output:
            assert args.multi_instance == 1
            assert len(dataset.objectLabels) == 1
            sixdbFile = args.output_dir + '/' + scene.label + '_' + dataset.objectLabels[0][4:] + '.yml'
            print('Saving sixdb formatted output file {}...'.format(sixdbFile))

        for j in range(len(detections)):
            if args.save_poses:
                with open(poseFiles[j], 'a') as f:
                    numpy.savetxt(f, detections[j].pose.array(), '%.6f')

            if args.save_scores:
                with open(scoreFiles[j], 'a') as f:
                    f.write('{:.6f}\n'.format(detections[j].kde))

            if args.save_sixdb_output:
                T = detections[j].pose.array()
                Tscore = detections[j].inlierfrac if args.use_ransac else detections[j].kde
                yamlData = {'run_time': totalTimeScene,
                            'ests':
                                [{'score': Tscore,
                                 'R': [float(T[0,0]), float(T[0,1]), float(T[0,2]), float(T[1,0]), float(T[1,1]), float(T[1,2]), float(T[2,0]), float(T[2,1]), float(T[2,2])],
                                 't': [float(T[0,3]), float(T[1,3]), float(T[2,3])]}]}
                stream = file(sixdbFile, 'w')
                yaml.dump(yamlData, stream)

        if args.visualize:
            visu.showDetections(query=objects,
                                target=sceneMesh,
                                detections=detections,
                                title='Detections in scene {}'.format(scene.label))
    else:
        print('No objects found!')
    cnt += 1
