#!/usr/bin/env python

import argparse, numpy, os, re, sys, timeit
import matplotlib.cm
from sklearn.externals import joblib

sys.path.append('../../covis/build/lib')

from covis import *

sys.path.append('keras-code')
from LoadKerasModelAndPredictEssentials import load

# Setup program options
po = argparse.ArgumentParser()

po.add_argument("scene", type=str, help="scene file")
po.add_argument("classifier", type=str, help="filename for classifer network")
po.add_argument("--resolution-surface", '-r', default=0, type=float,
                help="downsample point clouds to this resolution (<= 0 for the average object resolution)")
po.add_argument("--far", default=0, type=float, help="do not consider target points beyond this depth (0 for disabled)")

po.add_argument("--resolution-feature", default=5, type=float, help="resolution of features in mr")
po.add_argument("--feature", '-f', default="ppfhistfull", type=str,
                help="name the feature to compute - possible names are: ecsad,fpfh,ndhist,ppfhist,ppfhistfull,si,usc")
po.add_argument("--radius-feature", default=40, type=float,
                help="feature estimation radius as a multiplum of the resolution")
po.add_argument("--radius-normal", default=5, type=float, help="normal estimation radius as a multiplum of the resolution")
po.add_argument("--labels", default="", type=str, help="specify labels produced by the classifier - if unspecified, dummy labels are used")
po.add_argument("--net-batch-size", default=65536, type=int,
                help="specify batch size to use during network prediction")
po.add_argument("--use-gpu", '-u', action='store_true', help="set to true to use GPU for the matcher network")

po.add_argument("--use-soft-predictions", '-s', action='store_true', help="set to true to use soft predictions (see also --soft-label)")
po.add_argument("--soft-label", default='', type=str, help="specify label for doing soft predictions, leave empty to use first label")
po.add_argument("--prediction-threshold", '-t', default=0.5, type=float, help="specify lower prediction threshold in range [0,1]")
po.add_argument("--cmap", '-c', default="viridis", type=str, help="specify colormap (see matplotlib.org/users/colormaps.html)")

args = po.parse_args()

print('Loading scene from {}...'.format(args.scene))
sceneMesh = util.load(args.scene)

sceneSurf = filter.preprocess(mesh=sceneMesh,
                              resolution=args.resolution_surface,
                              far=args.far,
                              normalRadius=args.radius_normal * args.resolution_surface)

sceneCloud = filter.downsample(cloud=sceneSurf, resolution=args.resolution_feature * args.resolution_surface)

print('Computing {} scene features...'.format(sceneCloud.size))
sceneFeat = feature.computeFeature(name=args.feature,
                                   cloud=sceneCloud,
                                   surface=sceneSurf,
                                   radius=args.radius_feature * args.resolution_surface)

numQuery = sceneFeat.shape[1]
sceneFeatTranspose = numpy.transpose(sceneFeat)

startTime = timeit.default_timer()
root,ext = os.path.splitext(args.classifier)
if ext.lower() == '.h5':
    print('Using TF classifier for {} scene features...'.format(numQuery))
    if args.use_gpu:
        print('\tUsing GPU')
    netClassifier = load(args.classifier, args.use_gpu)
    predictions = netClassifier.predict(sceneFeatTranspose, batch_size=numQuery)
elif ext.lower() == '.pkl':
    print('Using sklearn classifier for {} scene features...'.format(numQuery))
    clf = joblib.load(args.classifier)
    scaler = joblib.load(root + '-scaler.pkl')
    predictions = clf.predict_proba(scaler.transform(sceneFeatTranspose))
else:
    raise 'Unknown extension for file: {}'.format(args.classifier)

assert predictions.shape[0] == numQuery
if len(args.labels):
    labels = re.split(' |,|;|\t|\n', args.labels)
else:
    print('Using dummy labels...')
    labels = ['Object {}'.format(i) for i in range(predictions.shape[1])]
assert len(labels) == predictions.shape[1]
if args.use_soft_predictions:
    softLabel = args.soft_label if len(args.soft_label) else labels[0]
    softLabelIndex = labels.index(softLabel)
    print('USING SOFT PREDICTIONS OF {} (index {})'.format(softLabel, softLabelIndex))
    classifications = numpy.full((predictions.shape[0], 1), softLabelIndex, dtype=int)
    scores = predictions[:,softLabelIndex]
else:
    print('USING HARD CLASSIFICATIONS')
    classifications = numpy.argmax(predictions, axis=1)
    scores = numpy.max(predictions, axis=1)
print('\tClassification time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

print('Visualizing classifications...')
if args.prediction_threshold > 0:
    print('\tUsing a lower prediction threshold of {}!'.format(args.prediction_threshold))
visu3d = visu.Visu3D()

sceneCloudArray = sceneCloud.array()
cmap = matplotlib.cm.get_cmap(args.cmap)
counts = numpy.zeros(len(labels)+1, numpy.float) if args.prediction_threshold > 0 else numpy.zeros(len(labels), numpy.float)
for i in range(numQuery):

    # ci = cmap(predictions[i,0])
    # sceneCloudArray[7, i] = ci[0]
    # sceneCloudArray[8, i] = ci[1]
    # sceneCloudArray[9, i] = ci[2]

    if scores[i] >= args.prediction_threshold:
        counts[classifications[i]] += 1
        ci = cmap(float(classifications[i]) / float(len(labels)-1))
        sceneCloudArray[7, i] = ci[0]
        sceneCloudArray[8, i] = ci[1]
        sceneCloudArray[9, i] = ci[2]
    else:
        counts[-1] += 1
        sceneCloudArray[7:10, i] = 1

visu3d.addPointCloud(core.PointCloud(sceneCloudArray))
if args.prediction_threshold > 0:
    visu3d.addText('(Not classified): {:.2f} %'.format(100 * counts[-1] / sceneCloudArray.shape[1]),
                   255, 255, 255)
for i in range(len(labels)):
    ci = cmap(float(i) / float(len(labels)-1))
    visu3d.addText(labels[i] + ': {:.2f} %'.format(100 * counts[i] / sceneCloudArray.shape[1]),
                   int(ci[0] * 255), int(ci[1] * 255), int(ci[2] * 255))
visu3d.show()
