Matcher learning
================

Activate your TF virtualenv (adjust paths if necessary):
```sh
source ~/tensorflow/bin/activate
```
Here's an example for running the baseline k-NN matcher UWA dataset:
```sh
python feature_matching_nnet.py ~/workspace/datasets/uwa/ -o models -s scenes -p GroundTruth\_3Dscenes --object-ext=.ply --scene-ext=.ply --pose-ext=.xf --object-regex=chef -r 1 
```

Here's an example for using a classifier net in front of the NN search (note the label ordering needs to be specified to fit the indices produced by the net with the order in which the objects were loaded):
```sh
python feature_matching_nnet.py ~/workspace/datasets/uwa/ -o models -s scenes -p GroundTruth\_3Dscenes --object-ext=.ply --scene-ext=.ply --pose-ext=.xf -r 1 --net-classifier keras-models/model_templates0.2BalancedTest_no_decay_lr-0.001_val-0.1_net-1024-128-16-5_minibatch-8192_1000e.h5 --order-labels=chef,chicken,parasaurolophus,rhino,T-rex
```

and with a matcher net:
```sh
python feature_matching_nnet.py ~/workspace/datasets/uwa/ -o models -s scenes -p GroundTruth\_3Dscenes --object-ext=.ply --scene-ext=.ply --pose-ext=.xf --object-regex=chef -r 1 --net-matcher keras-models/model_all_nr-nir-pir_decay-diff_lr-0.001_val-0.1_net-196-128-2_minibatch-8192_1000e_weights-improvement-424-0.92.h5
```

Finally, putting it all together, we can use both nets (point-wise classification to constrain the search and matching net to remove mismatches):
```sh
python feature_matching_nnet.py ~/workspace/datasets/uwa/ -o models -s scenes -p GroundTruth\_3Dscenes --object-ext=.ply --scene-ext=.ply --pose-ext=.xf -r 1 --net-classifier keras-models/model_templates0.2BalancedTest_no_decay_lr-0.001_val-0.1_net-1024-128-16-5_minibatch-8192_1000e.h5 --order-labels=chef,chicken,parasaurolophus,rhino,T-rex --net-matcher keras-models/model_all_nr-nir-pir_decay-diff_lr-0.001_val-0.1_net-196-128-2_minibatch-8192_1000e_weights-improvement-424-0.92.h5
```

Testing the regression net is for now done as follows:
```sh
python regression_test.py ~/workspace/datasets/uwa/ -o models -s scenes -p GroundTruth\_3Dscenes --object-ext=.ply --scene-ext=.ply --pose-ext=.xf -r 1 --object-regex=chef
```


Overall stats
=============
All tests are performed on all 50 UWA scenes. In the first test below, each scene is matched to all 5 objects. In the
remaining tests, only the Chef was used.

Comparing the *Classifier* results with *Baseline* implicitly tells us how well the classifier network performs, since a
perfect classifier should lead to equal or better performance than *Baseline*. The inlier threshold is **5 mm**.


| Method                          | Recall                | Precision             |
|:--------------------------------|:---------------------:|:---------------------:|
| Baseline (k-NN)                 | 19508/51231 (38.08 %) | 19508/59030 (33.05 %) |
| Classifier  + k-NN              | 19531/51231 (38.12 %) | 19531/59030 (33.09 %) |
| Classifier + matcher, joint net | 16067/51231 (31.36 %) | 16067/59030 (27.22 %) |


The 5 mm inlier threshold used above seems pessimistic, especially for the regressor net, which cannot be expected to
produce such accurate positions. Therefore, we give the same results as above, but now with an inlier tolerance of
**10 mm**:

| Method                              | Recall                | Precision             |
|:------------------------------------|:---------------------:|:---------------------:|
| Baseline (k-NN)                     | 9525/19462 (48.94 %)  |  9525/59030 (16.14 %) |
| Matcher, joint net                  | 11533/19462 (59.26 %) | 11533/59030 (19.54 %) |
| Matcher, Chef net                   | 11986/19462 (61.59 %) | 11986/59030 (20.30 %) |
| Classifier + k-NN                   | 8470/19462 (43.52 %)  | 8470/16163 (52.40 %)  |
| Classifier + matcher, Chef net      | 6904/19462 (35.47 %)  | 6904/16163 (42.71 %)  |
| Classifier + matcher, joint net     | 6835/19462 (35.12 %)  | 6835/16163 (42.29 %)  |
| Regressor                           | 5267/19462 (27.06 %)  |  5267/59030 (8.92 %)  |
