#!/usr/bin/env python

import argparse, timeit, numpy, re

import sys
sys.path.append('../../covis/build/lib')

from covis import *

sys.path.append('keras-code')
from LoadKerasModelAndPredictEssentials import load

# Setup program options
po = argparse.ArgumentParser()

po.add_argument("root", type=str, help="root path of your dataset")
po.add_argument("--object-dir", '-o', default="objects", type=str, help="subdirectory for the object models")
po.add_argument("--scene-dir", '-s', default="scenes", type=str, help="subdirectory for the scene models")
po.add_argument("--pose-dir", '-p', default="ground_truth", type=str, help="subdirectory for the ground truth pose models")
po.add_argument("--object-ext", default=".pcd", type=str, help="object file extension")
po.add_argument("--scene-ext", default=".pcd", type=str, help="scene file extension")
po.add_argument("--pose-ext", default=".txt", type=str, help="pose file extension")
po.add_argument("--pose-sep", default="-", type=str, help="pose file separator")
po.add_argument("--object-regex", default="", type=str, help="set this option to use a regular expression search when collecting object files")
po.add_argument("--scene-regex", default="", type=str, help="set this option to use a regular expression search when collecting scene files")

po.add_argument("--query-scale", default=1, type=float, help="sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates")
po.add_argument("--resolution-surface", '-r', default=0, type=float, help="downsample point clouds to this resolution (<= 0 for the average object resolution)")

po.add_argument("--resolution-feature", default=10, type=float, help="resolution of features in mr")
po.add_argument("--feature", '-f', default="ppfhistfull", type=str, help="name the feature to compute - possible names are: ecsad,fpfh,ndhist,ppfhist,ppfhistfull,si,usc")
po.add_argument("--radius-feature", default=40, type=float, help="feature estimation radius as a multiplum of the resolution")

po.add_argument("-knn", '-k', default=1, type=int, help="specifiy number of neighbors to search for during matching")
po.add_argument("--inlier-threshold", default=0, type=float, help="specify inlier threshold (set to <= 0 to use feature resolution)")

po.add_argument("--net-classifier", default="", type=str, help="specify filename for point classifier network - this net can be used both in combination with baseline k-NN search and network-based matching")
po.add_argument("--order-labels", default="", type=str, help="specify order of object labels (file name stems) produced by the classifier, e.g. T-rex,chef,chicken,parasaurolophus,rhino")
po.add_argument("--net-matcher", default="", type=str, help="specify filename for matcher network, set to empty to use baseline k-NN search")
po.add_argument("--net-batch-size", default=65536, type=int, help="specify batch size to use during matching network prediction")
po.add_argument("--use-gpu", '-u', action='store_true', help="set to true to use GPU for the matcher network")

args = po.parse_args()

print('Loading dataset from root {}...'.format(args.root))
dataset = util.loadDataset(args.root, args.object_dir, args.scene_dir, args.pose_dir, args.object_ext, args.scene_ext, args.pose_ext, args.pose_sep, args.object_regex, args.scene_regex)
print('\tGot {} model(s), {} scenes and {} poses'.format(len(dataset.objectLabels), len(dataset.sceneLabels), len(dataset.poseLabels)))
objects = dataset.objects

if len(args.net_classifier):
    labelsOrdered = re.split(' |,|;|\t|\n', args.order_labels)
    # Mapping from net index --> loaded index
    labelMap = []
    for j in range(len(labelsOrdered)):
        labelMap.append(dataset.objectLabels.index(labelsOrdered[j]))
    # Mapping from loaded index --> net index
    labelMapInv = [None] * len(labelMap)
    for j in range(len(labelMap)):
        labelMapInv[labelMap[j]] = j
    print('Interpreting network output classifications as follows:')
    for j in range(len(dataset.objectLabels)):
        print('\tIndex {}: {}'.format(j, dataset.objectLabels[labelMap[j]]))

# Surface scaling
queryScale = args.query_scale
resolution = args.resolution_surface
resolutionInput = resolution > 0
if not resolutionInput:
    resolution = 0
diag = 0
for i in range(len(objects)):
    if not resolutionInput:
        resolution += detect.computeResolution(objects[i]) * queryScale
    diag += detect.computeDiagonal(objects[i]) * queryScale
if not resolutionInput:
    resolution /= len(objects)
diag /= len(objects)

objectSurf = []
objectCloud = []
sys.stdout.write('Preprocessing '),sys.stdout.flush()
for i in range(len(objects)):
    sys.stdout.write(dataset.objectLabels[i] + ' '),sys.stdout.flush()
    objectSurf.append(filter.preprocess(mesh=objects[i],
                                       scale=queryScale,
                                       resolution=resolution,
                                       normalRadius=5 * resolution,
                                       orientNormals=True))

    # Generate feature points
    objectCloud.append(filter.downsample(cloud=objectSurf[i], resolution=args.resolution_feature * resolution))
sys.stdout.write('\n'),sys.stdout.flush()

# Compute features
print('Computing {} object features with a radius of {:.3f}...'.format(sum([c.width * c.height for c in objectCloud]), args.radius_feature * resolution))
objectFeat = feature.computeFeature(name=args.feature,
                                    clouds=objectCloud,
                                    surfaces=objectSurf,
                                    radius=args.radius_feature * resolution)

# Loop over scenes
totalPositives = 0
totalRetrieved = 0
totalInliers = 0
for i in range(dataset.size):
    scene = dataset.at(i)
    sceneMesh = scene.scene

    print('Processing scene {}/{} ({})...'.format(i+1, dataset.size, scene.label))
    if scene.empty:
        print('\tScene empty - skipping...')
        continue

    print('Preprocessing scene...')
    sceneSurf = filter.preprocess(mesh=sceneMesh,
                                  resolution=resolution,
                                  normalRadius=5 * resolution)

    sceneCloud = filter.downsample(cloud=sceneSurf, resolution=args.resolution_feature * resolution)

    print('Computing {} scene features...'.format(sceneCloud.size))
    sceneFeat = feature.computeFeature(name=args.feature,
                                       cloud=sceneCloud,
                                       surface=sceneSurf,
                                       radius=args.radius_feature * resolution)

    numQuery = sceneFeat.shape[1]
    numTarget = objectFeat.shape[1]
    objectFeatTranspose = numpy.transpose(objectFeat)
    sceneFeatTranspose = numpy.transpose(sceneFeat)

    predictions = []
    if len(args.net_classifier): # Using classifier net
        numQuery = sceneFeat.shape[1]
        print('Using network classifier for {} scene features...'.format(numQuery))
        if args.use_gpu:
            print('\tUsing GPU')

        netClassifier = load(args.net_classifier, args.use_gpu)
        startTime = timeit.default_timer()
        predictions = netClassifier.predict(sceneFeatTranspose, batch_size=numQuery)
        assert predictions.shape[0] == sceneFeat.shape[1]
        assert predictions.shape[1] == len(dataset.objectLabels)
        predictions = predictions[:,labelMapInv]
        classifications = numpy.argmax(predictions, axis=1)
        print('\tClassification time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

    if len(args.net_classifier):
        print('Point-wise classifications available, using the labels to constrain the matching...')

        objectFeatSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        objectFeatSeperatedIdx = [numpy.empty([0], dtype=int) for o in objectCloud]
        offset = 0
        for j in range(len(objectCloud)):
            objectFeatSeperated[j] = objectFeat[:,offset:offset+objectCloud[j].size]
            objectFeatSeperatedIdx[j] = numpy.arange(offset, offset+objectCloud[j].size)
            offset += objectCloud[j].size

        sceneCloudArray = sceneCloud.array()
        sceneCloudSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        sceneFeatSeperated = [numpy.empty([0], dtype=float) for o in objectCloud]
        sceneFeatSeperatedIdx = [numpy.empty([0], dtype=int) for o in objectCloud]

        for j in range(len(classifications)):
            idx = classifications[j]

            col = sceneCloudArray[:,j]
            col.shape = (sceneCloudArray.shape[0],1)
            sceneCloudSeperated[idx] = numpy.hstack([sceneCloudSeperated[idx], col]) if sceneCloudSeperated[idx].size else col

            col = sceneFeat[:, j]
            col.shape = (sceneFeat.shape[0], 1)
            sceneFeatSeperated[idx] = numpy.hstack([sceneFeatSeperated[idx], col]) if sceneFeatSeperated[idx].size else col
            sceneFeatSeperatedIdx[idx] = numpy.append(sceneFeatSeperatedIdx[idx], [j])

    if len(args.net_matcher): # Using matcher
        print('Using network matcher for {} {} feature pairs...'.format(sceneFeat.shape[1] * objectFeat.shape[1], args.feature))
        if args.use_gpu:
            print('\tUsing GPU')
        netMatcher = load(args.net_matcher, args.use_gpu)
        startTime = timeit.default_timer()

        if len(args.net_classifier): # Using classifier+matcher
            featureCorr = []
            for j in range(len(objectFeatSeperated)):
                numQueryJ = sceneFeatSeperated[j].shape[1]
                numTargetJ = objectFeatSeperated[j].shape[1]
                print('\tMatching {} scene points classified as object {}...'.format(numQueryJ, dataset.objectLabels[j]))
                Ntotal = numQueryJ * numTargetJ
                pairs = numpy.empty([Ntotal, 2], dtype=int)
                idxTarget = numpy.arange(0, numTargetJ).reshape((numTargetJ,1))
                for k in range(numQueryJ):
                    row = k * numTargetJ
                    idxJ = numpy.repeat([k],numTargetJ).reshape((numTargetJ,1))
                    pairs[row:row+numTargetJ,:] = numpy.hstack([idxJ, idxTarget])
    
                bsize = min(Ntotal, args.net_batch_size) if args.net_batch_size >= 1 else Ntotal
                featureCorrSeperated = []
                objectFeatTranspose = numpy.transpose(objectFeatSeperated[j])
                sceneFeatTranspose = numpy.transpose(sceneFeatSeperated[j])
                for k in range(0, Ntotal, bsize):
                    startTimeJ = timeit.default_timer()
                    Nbatch = bsize if k + bsize <= Ntotal else Ntotal - k
                    outputs = netMatcher.predict(numpy.hstack([objectFeatTranspose[pairs[k:k + Nbatch, 1], :],
                                                               sceneFeatTranspose[pairs[k:k+Nbatch,0],:]]),
                                                 batch_size=Nbatch)
        
                    for row in range(Nbatch):
                        if outputs[row,0] > outputs[row,1]:
                            featureCorrSeperated.append(core.Correspondence(pairs[k+row,0], pairs[k+row,1], 1 - outputs[row,0]))

                    sys.stdout.write('\r' + ' ' * 5)
                    sys.stdout.write('\r\t{} %'.format(100 * (k+Nbatch) / Ntotal))
                    sys.stdout.flush()

                for k in range(len(featureCorrSeperated)):
                    query = sceneFeatSeperatedIdx[j][ featureCorrSeperated[k].query ]
                    match = objectFeatSeperatedIdx[j][ featureCorrSeperated[k].match[0] ]
                    distance = featureCorrSeperated[k].distance[0]
                    featureCorr.append(core.Correspondence(query, match, distance))
                print('')
        else: # Using matcher
            Ntotal = numQuery * numTarget
            pairs = numpy.empty([Ntotal, 2], dtype=int)
            idxTarget = numpy.arange(0, numTarget).reshape((numTarget,1))
            for j in range(numQuery):
                row = j * numTarget
                idxJ = numpy.repeat([j],numTarget).reshape((numTarget,1))
                pairs[row:row+numTarget,:] = numpy.hstack([idxJ, idxTarget])
    
            bsize = min(Ntotal, args.net_batch_size) if args.net_batch_size >= 1 else Ntotal
            featureCorr = []
            for j in range(0, Ntotal, bsize):
                startTimeJ = timeit.default_timer()
                Nbatch = bsize if j + bsize <= Ntotal else Ntotal - j
                outputs = netMatcher.predict(numpy.hstack([objectFeatTranspose[pairs[j:j + Nbatch, 1], :],
                                                           sceneFeatTranspose[pairs[j:j+Nbatch,0],:]]),
                                             batch_size=Nbatch)
    
                for row in range(Nbatch):
                    if outputs[row,0] > outputs[row,1]:
                        featureCorr.append(core.Correspondence(pairs[j+row,0], pairs[j+row,1], 1 - outputs[row,0]))
    
                sys.stdout.write('\r\t{} %'.format(100 * (j+Nbatch) / Ntotal))
                sys.stdout.flush()
            sys.stdout.write('\n')
            
        print('\tPrediction time: {:.3f} seconds'.format(timeit.default_timer() - startTime))
    else: # No matcher net
        startTime = timeit.default_timer()

        if len(predictions): # Using classifer+k-NN
            featureCorrSeperated = []
            for j in range(len(sceneFeatSeperated)):
                print('\tMatching {} scene points classified as object {}...'.format(sceneFeatSeperated[j].shape[1], dataset.objectLabels[j]))
                featureCorrSeperated.append(detect.computeKnnMatchesExact(sceneFeatSeperated[j], objectFeatSeperated[j], args.knn))

            featureCorr = []
            for j in range(len(featureCorrSeperated)):
                for k in range(len(featureCorrSeperated[j])):
                    query = sceneFeatSeperatedIdx[j][k]
                    match = objectFeatSeperatedIdx[j][featureCorrSeperated[j][k].match[0]]
                    distance = featureCorrSeperated[j][k].distance[0]
                    featureCorr.append(core.Correspondence(query, match, distance))
        else:
            print('Using baseline k-NN matcher for {} --> {} {} features...'.format(sceneFeat.shape[1], objectFeat.shape[1], args.feature))
            featureCorr = detect.computeKnnMatchesExact(sceneFeat, objectFeat, args.knn)


        print('\tBaseline k-NN matching time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

    featureCorr = core.sort(featureCorr)
    featureCorr = featureCorr[0:numQuery*args.knn]

    inlierThreshold = args.resolution_feature if args.inlier_threshold <= 0 else args.inlier_threshold
    print('Verifying matches with an inlier threshold of {}...'.format(inlierThreshold))

    objectMask = scene.objectMask
    objectCloudAll = core.PointCloud()
    idxVisible = 0
    for j in range(len(objectCloud)):
        if objectMask[j]:
            tmp = core.transform(objectCloud[j], scene.poses[idxVisible])
            idxVisible += 1
        else:
            tmp = core.PointCloud(size=objectCloud[j].size)
        objectCloudAll += tmp

    corrTotal = core.computeAllCorrespondences(sceneCloud.size, objectCloudAll.size, distance=-1)
    scenePositives = detect.filterCorrespondencesTransform(corrTotal,
                                                           sceneCloud,
                                                           objectCloudAll,
                                                           core.Identity(),
                                                           inlierThreshold)

    scenePositives = len(core.unique(scenePositives, args.knn))
    totalPositives += scenePositives

    sceneRetrieved = len(featureCorr)
    totalRetrieved += sceneRetrieved

    inliers = detect.filterCorrespondencesTransform(featureCorr,
                                                    sceneCloud,
                                                    objectCloudAll,
                                                    core.Identity(),
                                                    inlierThreshold)
    sceneInliers = len(inliers)
    totalInliers += sceneInliers

    print('Stats for scene {}'.format(scene.label))
    print('\t{} feature matches retrieved (searching for {}-NNs)'.format(len(featureCorr), args.knn))
    print('\t{} possible {}-NN matches'.format(scenePositives, args.knn))
    print('\tRecall for scene:    {}/{} ({:.2f} %)'.format(sceneInliers, scenePositives, 100 * sceneInliers / float(scenePositives)))
    print('\tPrecision for scene: {}/{} ({:.2f} %)'.format(sceneInliers, sceneRetrieved, 100 * sceneInliers / float(sceneRetrieved)))

    print('Overall stats')
    print('\tRecall:      {}/{} ({:.2f} %)'.format(totalInliers, totalPositives, 100 * totalInliers / float(totalPositives)))
    print('\tPrecision:   {}/{} ({:.2f} %)'.format(totalInliers, totalRetrieved, 100 * totalInliers / float(totalRetrieved)))

