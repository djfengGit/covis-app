// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>

using namespace covis;

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/ppf_registration.h>

// TF
#include "tensorflow/cc/ops/const_op.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/stringpiece.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/env.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"

// Load a TF graph (definition at the bottom)
tensorflow::Status LoadGraph(const std::string& graph_file_name, std::unique_ptr<tensorflow::Session>* session);

// Point and feature types
typedef pcl::PointNormal PointT;
typedef pcl::PointCloud<PointT> CloudT;

// Loaded point clouds
std::vector<CloudT::Ptr> objectSurf, objectCloud;

/*
 * Main entry point
 */
int main(int argc, const char **argv) {
    // Setup program options
    core::ProgramOptions po;

    po.addPositional("root-path", "root path of your dataset");
    po.addOption("object-dir", 'o', "objects", "subdirectory for the object models");
    po.addOption("scene-dir", 's', "scenes", "subdirectory for the scene models");
    po.addOption("pose-dir", 'p', "ground_truth", "subdirectory for the ground truth pose models");
    po.addOption("object-ext", ".pcd", "object file extension");
    po.addOption("scene-ext", ".pcd", "scene file extension");
    po.addOption("pose-ext", ".txt", "pose file extension");
    po.addOption("pose-sep", "-", "pose file separator");
    po.addOption("object-regex", "", "set this option to use a regular expression search when collecting object files");
    po.addOption("scene-regex", "", "set this option to use a regular expression search when collecting scene files");

    // Surfaces and normals
    po.addOption("object-scale", 1,
                 "sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates");
    po.addOption("resolution-surface", 'r', 0,
                 "downsample point clouds to this resolution (<= 0 for the average object resolution)");
    po.addOption("far", 0, "do not consider scene points beyond this depth (0 for disabled)");
    po.addOption("radius-normal", 'n', 5, "normal estimation radius in mr (<= 0 means 5 resolution units)");
    po.addFlag("orient-object-normals", "ensure consistent normal orientation for the object model");
    po.addFlag("concave", "set this flag to assume non-convex object models during normal correction");

    // Features and matching
    po.addOption("resolution-feature", 5, "resolution of features in mr (<= 0 for five resolution units)");
    po.addOption("feature", feature::FeatureNames + ",ppf",
                 "name the features to benchmark - possible names are: " + feature::FeatureNames + ",ppf");
    po.addOption("radius-feature", 'f', "40",
                 "feature estimation radius as a multiple of the resolution");
    po.addOption("knn-ppf-voting", 50, "number of nearest features to use during PPF voting (0 for automatic)");
    po.addFlag("match-full-scene",
               "by default, only object points in the scenes are used - set this flag to include background information during matching");
    po.addOption("knn", 'k', 0, "number of nearest features to search for (0 for automatic, 2 for ratio scoring)");
    po.addOption("inlier-threshold", 5, "specify inlier threshold as a multiple of the resolution");

    po.addOption("net", "", "specify filename for matcher network, set to empty to use baseline k-NN search");
    po.addOption("net-input", "main-input_input", "specify ID for the net input layer");
    po.addOption("net-output", "main-output_output", "specify ID for the net output layer");
    po.addOption("net-batch-size", 64, "specify batch size to use during prediction");

    // Outputs
    po.addFlag("save-outputs",
               "generate binary result files containing two columns with NN distance and a binary value indicating inlier/outlier");

    // Misc.
    po.addFlag('v', "verbose", "show additional information");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    // Misc.
    const bool verbose = po.getFlag("verbose");

    if(verbose)
        po.print();

    // Load dataset
    util::DatasetLoader dataset(
            po.getValue("root-path"),
            po.getValue("object-dir"),
            po.getValue("scene-dir"),
            po.getValue("pose-dir"),
            po.getValue("object-ext"),
            po.getValue("scene-ext"),
            po.getValue("pose-ext"),
            po.getValue("pose-sep")
    );
    dataset.setRegexObject(po.getValue("object-regex"));
    dataset.setRegexScene(po.getValue("scene-regex"));
    dataset.parse();
    if(verbose)
        COVIS_MSG(dataset);

    // Load object point clouds
    std::vector<util::DatasetLoader::ModelPtr> objectMesh = dataset.getObjects();
    COVIS_ASSERT(!objectMesh.empty());

    // Surface scaling
    const float objectScale = po.getValue<float>("object-scale");
    float resolution = po.getValue<float>("resolution-surface");
    const bool resolutionInput = (resolution > 0);
    if(!resolutionInput)
        resolution = 0;
    float diag = 0;
    for(size_t i = 0; i < objectMesh.size(); ++i) {
        if(!resolutionInput)
            resolution += detect::computeResolution(objectMesh[i]) * objectScale;
        diag += detect::computeDiagonal(objectMesh[i]) * objectScale;
        if(objectScale != 1)
            objectMesh[i] = filter::scale(objectMesh[i], objectScale);
    }
    if(!resolutionInput)
        resolution /= float(objectMesh.size());
    diag /= float(objectMesh.size());

    const float far = po.getValue<float>("far");
    const float nrad =
            po.getValue<float>("radius-normal") > 0.0 ?
            po.getValue<float>("radius-normal") * resolution :
            5 * resolution;
    const bool orientObjectNormals = po.getFlag("orient-object-normals");
    const bool concave = po.getFlag("concave");

    // Features and matching
    const float resFeat =
            po.getValue<float>("resolution-feature") > 0.0 ?
            po.getValue<float>("resolution-feature") * resolution :
            5 * resolution;
    std::vector<std::string> features = po.getVector("feature");
    COVIS_ASSERT(!features.empty());
    std::vector<float> frad = po.getVector<float>("radius-feature");
    COVIS_ASSERT_MSG(!frad.empty(), "No feature radius specified!");
    if(frad.size() == 1 && features.size() > 1) {
        COVIS_MSG_INFO("Using the same radius (" << frad[0] * resolution << ") for all features...");
        frad.assign(features.size(), frad[0] * resolution);
    } else if(frad.size() > 1 && features.size() == 1) {
        COVIS_MSG_INFO(
                "Using multi-scale fusion (" << frad.size() << " scales) for the same feature (" << features[0]
                                             << ")...");
        features.assign(frad.size(), features[0]);
    }
    for(size_t i = 0; i < frad.size(); ++i)
        frad[i] = (frad[i] > 0 ? frad[i] * resolution : 0);
    COVIS_ASSERT_MSG(frad.size() == features.size(), "You must specify as many feature radii as features!");
    size_t knnPpfVoting = po.getValue<size_t>("knn-ppf-voting");
    if(knnPpfVoting == 0)
        knnPpfVoting = 50;

    const bool matchFullScene = po.getFlag("match-full-scene");
    std::vector<size_t> knn = po.getVector<size_t>("knn");
    if(knn.size() == 1)
        knn.assign(features.size(), knn[0]);
    if(knn.empty())
        knn.assign(features.size(), 0);
    COVIS_ASSERT_MSG(knn.size() == features.size(), "You must specify either one or as many knn values as features!");
    const float inlierThreshold = po.getValue<float>("inlier-threshold") * resolution;
    COVIS_ASSERT(inlierThreshold > 0);
    const bool saveOutputs = po.getFlag("save-outputs");

    /*
     * Preprocess objects
     */
    {
        core::ScopedTimer::Ptr t;
        if(verbose)
            t.reset(new core::ScopedTimer("Preprocessing"));

        objectSurf.resize(objectMesh.size());
        for(size_t i = 0; i < objectMesh.size(); ++i) {
            objectSurf[i] = filter::preprocess<PointT>(objectMesh[i], 1, true, 0, resolution, nrad, orientObjectNormals,
                                                      concave, verbose);
            COVIS_ASSERT(!objectSurf[i]->empty());
        }

        // Generate feature points
        objectCloud.resize(objectSurf.size());
        for(size_t i = 0; i < objectSurf.size(); ++i) {
            objectCloud[i] = filter::downsample<PointT>(objectSurf[i], resFeat);

            COVIS_ASSERT(!objectCloud[i]->empty());
        }
    }

    /*
     * Compute object features and index
     */
    feature::MatrixVecT objectFeat(features.size());
    size_t ftotal = 0;
    std::vector<std::pair<int, int> > ppfIndexObject[features.size()];
    typedef flann::Index<flann::L2_Simple<float> > IndexT;
    typedef boost::shared_ptr <IndexT> IndexPtrT;
    std::vector<IndexPtrT> index(features.size());

    {
        core::ScopedTimer::Ptr t;
        if(verbose) {
            t.reset(new core::ScopedTimer("Object features"));
            COVIS_MSG_INFO("Feature names:");
            core::print(features);
            COVIS_MSG_INFO("Feature radii:");
            core::print(frad);
            COVIS_MSG_INFO("Total number of object features:");
            for(size_t i = 0; i < objectCloud.size(); ++i)
                ftotal += objectCloud[i]->size();
            COVIS_MSG(ftotal);
        }

#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(size_t i = 0; i < features.size(); ++i) {
            if(boost::iequals(features[i], "ppf")) {
                objectFeat[i] = feature::computePPFEigen<PointT>(objectCloud, frad[i], ppfIndexObject[i]);
                index[i].reset(new IndexT(flann::KDTreeSingleIndexParams()));
            } else {
                objectFeat[i] = feature::computeFeature<PointT>(features[i], objectCloud, objectSurf, frad[i]);
                index[i].reset(new IndexT(flann::LinearIndexParams()));
            }

            index[i]->buildIndex(flann::Matrix<float>(objectFeat[i].data(), objectFeat[i].cols(), objectFeat[i].rows()));
        }
    }

    /*
     * Run
     */
    // Total number of possible inliers (up to inlier threshold) in the scenes
    size_t totalPositives = 0;
    Eigen::MatrixXf matchingOutput[features.size()];
    for(size_t i = 0; i < dataset.size(); ++i) {
        // Load
        util::DatasetLoader::SceneEntry scene = dataset.at(i);
        util::DatasetLoader::ModelPtr sceneMesh = scene.scene;

        if(verbose)
            COVIS_MSG(scene);

        if(scene.empty()) {
            if(verbose)
                COVIS_MSG("Skipping empty scene " << scene.label << "...");
            continue;
        }

        // Start timer
        core::ScopedTimer::Ptr t;
        if(verbose)
            t.reset(new core::ScopedTimer("Matching, scene " +
                                          core::stringify(i + 1) + "/" + core::stringify(dataset.size())));

        CloudT::Ptr sceneSurf = filter::preprocess<PointT>(sceneMesh, 1, true, far, resolution, nrad, false, false,
                                                            verbose);

        if(sceneSurf->empty()) {
            COVIS_MSG_WARN("No scene points left after preprocessing! Skipping scene...");
            continue;
        }

        // Generate feature points in the scene
        CloudT::Ptr sceneCloud = filter::downsample<PointT>(sceneSurf, resFeat);
        if(!matchFullScene) {
            std::vector<bool> sceneMask(sceneCloud->size(), false);

            std::vector<CloudT::Ptr> objectCloudVisible = core::mask(objectCloud, scene.objectMask);
            for(size_t j = 0; j < scene.poses.size(); ++j) {
                // Place the present object in the scene using GT pose
                CloudT::Ptr tmp(new CloudT);
                core::transform<PointT>(*objectCloudVisible[j], *tmp, scene.poses[j]);
                // Remove occluded points
                core::Correspondence::VecPtr corr = detect::knnSearch<PointT>(tmp, sceneCloud, 1);
                const float thres = resFeat * resFeat; /// TODO: What threshold here?
                for(size_t k = 0; k < corr->size(); ++k)
                    if((*corr)[k].distance[0] <= thres)
                        sceneMask[ (*corr)[k].match[0] ] = true;
            }

            *sceneCloud = core::mask(*sceneCloud, sceneMask);
        }
        COVIS_ASSERT(!sceneCloud->empty());

        if(verbose) {
            t->intermediate("preprocessing");
            COVIS_MSG_INFO("Feature names:");
            core::print(features);
            COVIS_MSG_INFO("Feature radii:");
            core::print(frad);
        }

        feature::MatrixVecT sceneFeat(features.size());
        std::vector<std::pair<int, int> > ppfIndexScene[features.size()];
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(size_t j = 0; j < features.size(); ++j) {
            if(boost::iequals(features[j], "ppf"))
                sceneFeat[j] = feature::computePPFEigen<PointT>(sceneCloud, frad[j], ppfIndexScene[j]);
            else
                sceneFeat[j] = feature::computeFeature<PointT>(features[j], sceneCloud, sceneSurf, frad[j]);
        }

        if(verbose)
            t->intermediate(core::stringify(sceneCloud->size()) + " features");

        // Perform feature matching
        std::vector<core::Correspondence::VecPtr> featureCorr(features.size());
        const std::string graph = po.getValue("net");
        if(graph.empty()) {
            for(size_t j = 0; j < features.size(); ++j) {
                // Do search
                size_t knnj = (knn[j] == 0 ? 2 : knn[j]);

                if(verbose) {
                    COVIS_MSG_INFO("Computing " << knnj << " nearest neighbors for " << features[j] << "...");
                    if(knnj == 2)
                        COVIS_MSG("\tUsing ratio scoring");
                    if(boost::iequals(features[j], "ppf"))
                        COVIS_MSG("\tUsing " << knnPpfVoting << " neighbors to perform PPF voting...");
                }

                std::vector<std::vector<size_t> > indices;
                std::vector<std::vector<float> > dists;
                flann::Matrix<float> mscene(sceneFeat[j].data(), sceneFeat[j].cols(), sceneFeat[j].rows());
                flann::SearchParams param;
                param.cores = 0;
                if(boost::iequals(features[j], "ppf"))
                    index[j]->knnSearch(mscene, indices, dists, knnPpfVoting, param);
                else
                    index[j]->knnSearch(mscene, indices, dists, knnj, param);

                // Convert FLANN outputs to correspondence vector
                featureCorr[j].reset(new core::Correspondence::Vec(indices.size()));
                for(size_t k = 0; k < indices.size(); ++k)
                    (*featureCorr[j])[k] = core::Correspondence(k, indices[k], dists[k]);

                // Special case for PPF voting
                if(boost::iequals(features[j], "ppf")) {
                    featureCorr[j] = detect::computeKnnMatchesPPF(*featureCorr[j],
                                                                  sceneCloud->size(),
                                                                  ftotal,
                                                                  ppfIndexScene[j],
                                                                  ppfIndexObject[j],
                                                                  knnj);
                }

                // Special case for ratio scoring
                if(knnj == 2) {
                    for(size_t k = 0; k < featureCorr[j]->size(); ++k) {
                        (*featureCorr[j])[k].match.resize(1);
                        (*featureCorr[j])[k].distance.assign(1, (*featureCorr[j])[k].distance[0] / (*featureCorr[j])[k].distance[1]);
                    }
                }

                featureCorr[j] = core::flatten(*featureCorr[j]);
            }

            if(verbose)
                t->intermediate("nearest neighbor search");
        } else {
            /*
             * BEGIN NN SECTION
             */
            if(verbose)
                COVIS_MSG_INFO("Using NN matching!");

            const std::string inputLayer = po.getValue("net-input");
            const std::string outputLayer = po.getValue("net-output");

            std::unique_ptr<tensorflow::Session> session;
            tensorflow::Status load_graph_status = LoadGraph(graph, &session);
            COVIS_ASSERT_MSG(load_graph_status.ok(), "Loading TF graph failed: " << load_graph_status);

            for(size_t j = 0; j < features.size(); ++j) {
                core::ScopedTimer::Ptr t;
                if(verbose)
                    t.reset(new core::ScopedTimer("TF prediction"));
                size_t knnj = (knn[j] == 0 ? 1 : knn[j]);
                const int numQuery = sceneFeat[j].cols();
                const int numTarget = objectFeat[j].cols();
                const int dim = sceneFeat[j].rows();
                const int stride = 2 * dim;

                /*
                 * Brute-force matching
                 * Mini-batch processing
                 */
                featureCorr[j].reset(new core::Correspondence::Vec);
                core::ProgressDisplay::Ptr pd;
                if(verbose)
                    pd.reset(new core::ProgressDisplay(numQuery));
                const int bsize = MAX(1, po.getValue<int>("net-batch-size"));
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(int k = 0; k < numQuery; k += bsize) {
                    const int bsizek = (k + bsize <= numQuery ? bsize : numQuery - k);
                    tensorflow::Tensor input(tensorflow::DT_FLOAT, {bsizek * numTarget, stride});

                    auto minput = input.matrix<float>();
                    Eigen::MatrixXi index(bsizek * numTarget, 2);
                    int row = 0;
                    for(int kk = k; kk < k + bsizek ; ++kk) {
                        for(int l = 0; l < numTarget; ++l) {
                            std::copy(objectFeat[j].col(l).data(),
                                      objectFeat[j].col(l).data() + dim,
                                      minput.data() + row * stride);
                            std::copy(sceneFeat[j].col(kk).data(),
                                      sceneFeat[j].col(kk).data() + dim,
                                      minput.data() + row * stride + dim);
                            index(row, 0) = kk;
                            index(row, 1) = l;
                            ++row;
                        }
                    }

                    std::vector<tensorflow::Tensor> outputs;
                    tensorflow::Status run_status = session->Run({{inputLayer, input}}, {outputLayer}, {}, &outputs);
                    if (!run_status.ok())
                        COVIS_THROW("Running model failed: " << run_status);

                    auto moutput = outputs[0].matrix<float>();
                    for(int row = 0; row < bsizek * numTarget; ++row)
                        if(moutput(row, 0) > moutput(row, 1)) {
#pragma omp critical
                            featureCorr[j]->push_back(core::Correspondence(index(row, 0), index(row, 1), 1 - moutput(row, 0)));
                        }

#pragma omp critical
                    if(verbose)
                        *pd += bsizek;
                }

                // TODO: Take 1-NNs
                core::sort(*featureCorr[j]);
                featureCorr[j]->resize(knnj * numQuery);
            } // End loop over features
        }

        // Place all models in the scene (using bogus zero-transform for non-present objects)
        CloudT::Ptr objectCloudAll(new CloudT);
        size_t idxVisible = 0;
        for(size_t j = 0; j < objectCloud.size(); ++j) {
            CloudT tmp;
            if(scene.objectMask[j])
                core::transform<PointT>(*objectCloud[j], tmp, scene.poses[idxVisible++]);
            else
                tmp = CloudT(objectCloud[j]->size(), 1, PointT());

            *objectCloudAll += tmp;
        }

        // Find the total number of positives
        size_t scenePositives;
        if(matchFullScene) {
            core::Correspondence::Vec corrTotal;
            for(size_t j = 0; j < sceneCloud->size(); ++j)
                for(size_t k = 0; k < objectCloudAll->size(); ++k)
                    corrTotal.push_back(core::Correspondence(j, k, -1));

            scenePositives = detect::filterCorrespondencesTransform<PointT>(corrTotal,
                                                                          sceneCloud,
                                                                          objectCloudAll,
                                                                          core::Detection::MatrixT::Identity(),
                                                                          inlierThreshold)->size();
        } else {
            scenePositives = sceneCloud->size();
        }

        totalPositives += scenePositives;

        // Validate matches
        if(verbose)
            COVIS_MSG_INFO("Validating matches with an inlier treshold of " << inlierThreshold << "...");
        for(size_t j = 0; j < features.size(); ++j) {
            detect::CorrespondenceFilterTransform <PointT> filter;
            filter.setQuery(sceneCloud);
            filter.setTarget(objectCloudAll);
            filter.setTransformation(core::Detection::MatrixT::Identity());
            filter.setThreshold(inlierThreshold);
            filter.filter(*featureCorr[j]);

            const std::vector<bool> &cmask = filter.getMask();

            Eigen::MatrixXf matchingOutputCurrent(cmask.size(), 2);
            for(size_t k = 0; k < cmask.size(); ++k) {
                matchingOutputCurrent(k, 0) = (*featureCorr[j])[k].distance[0];
                matchingOutputCurrent(k, 1) = float(cmask[k]);
            }

            if(saveOutputs) {
                const std::string outname = "matching_output_" + features[j] + ".txt";
                const bool append = (matchingOutput[j].size() > 0);
                if(verbose)
                    COVIS_MSG_INFO(
                            (append ? "Appending" : "Saving") << " matching output to file " << outname << "...");
                util::saveEigen(outname, matchingOutputCurrent, true, true, append);
            }

            Eigen::MatrixXf tmp(matchingOutput[j].rows() + matchingOutputCurrent.rows(), 2);
            tmp << matchingOutput[j], matchingOutputCurrent;
            matchingOutput[j] = tmp;
            if(verbose) {
                COVIS_MSG("Stats for " << features[j] << " in scene \"" << scene.label << "\"");
                COVIS_MSG("\t" << featureCorr[j]->size() << " feature matches");
                COVIS_MSG("\tInliers in scene: " <<
                          matchingOutputCurrent.col(1).sum() << "/" << scenePositives << " (" <<
                          std::fixed << std::setprecision(2) <<
                          100 * matchingOutputCurrent.col(1).sum() / scenePositives << " %)");
                const float precision = matchingOutput[j].col(1).sum() / matchingOutput[j].rows();
                const float recall = matchingOutput[j].col(1).sum() / totalPositives;
                COVIS_MSG("\tOverall recall: " << std::fixed << std::setprecision(2) <<
                          matchingOutput[j].col(1).sum() << "/" << totalPositives << " (" << 100 * recall << " %)");
                COVIS_MSG("\tOverall precision: " << std::fixed << std::setprecision(2) <<
                          100 * precision << " %");
                COVIS_MSG("\tOverall F1 score: " << std::fixed << std::setprecision(2) <<
                          2 * precision * recall / (precision + recall));
            }
        }
    }

    return 0;
}

tensorflow::Status LoadGraph(const std::string& graph_file_name, std::unique_ptr<tensorflow::Session>* session) {
    tensorflow::GraphDef graph_def;
    tensorflow::Status load_graph_status = tensorflow::ReadBinaryProto(tensorflow::Env::Default(),
                                                                       graph_file_name,
                                                                       &graph_def);
    if (!load_graph_status.ok())
        return tensorflow::errors::NotFound("Failed to load compute graph at '", graph_file_name, "'");

    session->reset(tensorflow::NewSession(tensorflow::SessionOptions()));

    return (*session)->Create(graph_def);
}
