import itertools
import matplotlib.pyplot as plt
import numpy as np

def plotconfusion(cnf, title='Confusion matrix', block=True):
    # Sanity checks
    assert cnf.ndim >= 2
    assert cnf.shape[0] == cnf.shape[1]
    Nclass = cnf.shape[0]

    # Create key stats
    cnf = cnf.astype('float')
    cnfsum = np.sum(cnf)
    cnfnorm = cnf / cnfsum
    cnf = np.vstack([np.hstack([cnf, np.zeros((Nclass, 1))]), np.zeros((1, Nclass + 1))])
    cnfnorm = np.vstack([np.hstack([cnfnorm, np.zeros((Nclass, 1))]), np.zeros((1, Nclass + 1))])
    for i in range(Nclass):
        rowsum = np.sum(cnf[i, :])
        cnf[i, Nclass] = rowsum
        cnfnorm[i, Nclass] = cnf[i, i] / rowsum if rowsum > 0 else np.nan

        colsum = np.sum(cnf[:, i])
        cnf[Nclass, i] = colsum
        cnfnorm[Nclass, i] = cnf[i, i] / colsum if colsum > 0 else np.nan
    cnf[Nclass, Nclass] = cnfsum
    cnfnorm[Nclass, Nclass] = np.trace(cnf[0:Nclass,0:Nclass]) / cnfsum

    # Create background colors as a 3rd dimension
    cnf3d = np.zeros([cnf.shape[0], cnf.shape[1], 3])
    for i in range(Nclass):
        cnf3d[i, i, 0:3] = [120. / 255., 230. / 255., 180. / 255.]
    for i, j in itertools.product(range(Nclass), range(Nclass)):
        cnf3d[i, j, 0:3] = [230. / 255, 140. / 255, 140. / 255.] if i != j else cnf3d[i, j,0:3]
    cnf3d[Nclass, 0:Nclass, 0:3] = [0.5, 0.5, 0.5]
    cnf3d[0:Nclass, Nclass, 0:3] = [0.5, 0.5, 0.5]
    cnf3d[Nclass, Nclass, 0:3] = [120. / 255., 150. / 255., 230. / 255.]

    # Startup a plot with specified window title
    plt.figure()
    plt.imshow(cnf3d, interpolation='nearest')
    plt.gcf().canvas.set_window_title(title)

    # Setup labels and ticks
    plt.xlabel('Predicted label', weight='bold')
    plt.ylabel('True label', weight='bold')
    # plt.gca().xaxis.set_label_position('top')
    # plt.gca().yaxis.set_label_position('right')
    tick_marks = np.arange(Nclass)
    plt.xticks(tick_marks, rotation=45)
    plt.yticks(tick_marks)

    # TODO: Create grid lines between cells
    # plt.grid(which='both', color='k')

    # Insert text
    # TODO: Show FPs/FNs with the color [100./255., 0, 0] (http://se.mathworks.com/help/nnet/ref/plotconfusion.html)
    for i, j in itertools.product(range(Nclass + 1), range(Nclass + 1)):
        if i == Nclass and j < Nclass:
            plt.text(j, i,
                     'N/A' if np.isnan(cnfnorm[i,j]) else '{:.0f} %'.format(100 * cnfnorm[i,j]),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color=[0, 100. / 255., 0])
        elif j == Nclass and i < Nclass:
            plt.text(j, i,
                     'N/A' if np.isnan(cnfnorm[i,j]) else '{:.0f} %'.format(100 * cnfnorm[i,j]),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color=[0, 100. / 255., 0])
        elif i == Nclass and j == Nclass:
            plt.text(j, i,
                     '{:.0f}%'.format(100 * cnfnorm[i, j]),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='black',
                     weight='bold')
        else:
            plt.text(j, i,
                     '{:.0f}\n{:.0f} %'.format(cnf[i, j], 100 * cnfnorm[i, j]),
                     horizontalalignment='center',
                     verticalalignment='center',
                     color='black')

    plt.tight_layout()
    plt.show(block=block)