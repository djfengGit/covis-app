%% Init
clear;clc;close all;

% Specify histogram dimensions
num_planes = 3;
plane_dims = [32,16];
plane_size = prod(plane_dims);

%% Load
filename = 'weights.h5';
weights = double(h5read(filename, '/dense_1/dense_1/kernel:0'));

%% Show full weight matrix
figure('Name', 'Full weight matrix')
imagesc(abs(weights))
title('Full weight matrix')
colormap gray
colorbar

%% Show the weighting of the input neurons
% First compute column norms of weight matrix
[Nneuron, Nfeat] = size(weights);
norms = zeros(1, Nfeat);
for i=1:Nfeat
    norms(i) = norm(weights(:,i));
end

color_range = [0, max(norms)];

figure('Name', 'Input layer weighting')
for i = 1:2 * num_planes
    subplot(2,num_planes,i)
    offset = (i - 1) * plane_size + 1;
    imagesc(reshape(norms(offset:i*plane_size), plane_dims), color_range)
    title(sprintf('Plane %i weighting', i))
    colormap gray
    colorbar
    axis off
end

%% Show some first-layer neurons
disp 'Showing first-layer neurons - press any key to go to next plot'
figure('Name', 'First-layer neuron')
hax = gca;
for i = 1:size(weights,1)
    fprintf('Showing neuron %d/%d...\n', i, size(weights,1))
    neuron = weights(i,:);
    for j = 1:2 * num_planes
        subplot(2, num_planes, j)
        offset = (j - 1) * plane_size + 1;
        imagesc(reshape(neuron(offset:j*plane_size), plane_dims))
        title(sprintf('Neuron %d, plane %d', i, j))
        colormap gray
        colorbar
        axis off
    end
    pause
end