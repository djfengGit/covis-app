// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// STL
#include <random>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/geometry.h>

#include <H5Cpp.h>
#include <eigen3-hdf5/eigen3-hdf5.hpp>

// Point and feature types
typedef pcl::PointXYZRGBNormal PointT;

// Loaded point clouds and computed histogram features
pcl::PointCloud<PointT>::Ptr objectSurf, sceneSurf;
pcl::PointCloud<PointT>::Ptr objectCloud, sceneCloud;
feature::MatrixT objectFeat, sceneFeat;

void transformPolygonMesh(pcl::PolygonMesh::Ptr &inMesh, const Eigen::Matrix4f &transform) {
    //Important part starts here
    pcl::PointCloud<pcl::PointXYZ> cloud, cloud_out;
    pcl::fromPCLPointCloud2(inMesh->cloud, cloud);
    pcl::transformPointCloud(cloud, cloud_out, transform);
    pcl::toPCLPointCloud2(cloud_out, inMesh->cloud);
}

void saveVec( const std::vector<feature::MatrixT>& featurePairs, const std::string & filename) {
    std::cout << "Saving " << featurePairs.size() << " features to " << filename << std::endl;
    if (featurePairs.size()) {
        feature::MatrixT conc(featurePairs.size(),featurePairs[0].cols());
        for (unsigned int i=0; i<featurePairs.size(); i++) {
            conc.row(i) = featurePairs[i];
        }
        //util::saveEigen( filename, conc);
        H5::H5File file(filename, H5F_ACC_TRUNC);
        EigenHDF5::save(file, "dataset_1", conc);
    } else {
        std::ofstream newFile(filename);
        newFile.open(filename);
        newFile.close();
    }
}

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("object", "mesh or point cloud file for object model");
    po.addPositional("scene", "mesh or point cloud file for scene model");
    po.addPositional("pose", "transformation that aligns object and scene");

    // Surfaces and normals
    po.addOption("resolution", 'r', 1, "downsample point clouds to this resolution (<= 0 for disabled)");
    po.addOption("far", -1, "do not consider scene points beyond this depth (<= 0 for disabled)");
    po.addOption("radius-normal", 'n', 5, "normal estimation radius in mr (<= 0 means two resolution units)");
    po.addFlag('o', "orient-object-normals", "ensure consistent normal orientation for the object model");

    // Features and matching
    po.addOption("feature", "si", "choose which feature to use from this list: " + feature::FeatureNames);
    po.addOption("resolution-object", 5, "resolution of object features in mr (<= 0 for five resolution units)");
    po.addOption("resolution-scene", 5, "resolution of scene features in mr (<= 0 for five resolution units)");
    po.addOption("radius-feature", 'f', 25, "feature estimation radius (<= 0 means 25 resolution units)");

    // Patch extraction specific
    po.addOption("positive-radius", 3, "radius around a transformed feature location that is considered the area of positive features");
    po.addOption("negative-radius", 6, "radius around a transformed feature location that is considered the area of dense negative features");

    po.addOption("output-filename-base", "out-base", "Base name of the to be created output files");

    po.addOption("max-pos-count-radius", "-1", "Maximum number of positive scene surface neighbours sampled per object cloud feature");
    po.addOption("max-neg-count-radius", "-1", "Maximum number of negative scene surface neighbours sampled per object cloud feature");
    po.addFlag("balance-pos-neg-radius", "Balance the positive and negative samples coming from the radius search");
//    po.addFlag("balance-pos-neg-overall", "Balance the positive and negative samples overall");

    po.addOption("max-neg-count-cloud", "-1", "Maximum number of negative scene cloud features sampled per object cloud feature");


    // Parse
    if(!po.parse(argc, argv))
        return 1;
    po.print();

    // Load models
    pcl::PolygonMesh::Ptr mobject(new pcl::PolygonMesh);
    pcl::PolygonMesh::Ptr mscene(new pcl::PolygonMesh);
    util::load(po.getValue("object"), *mobject);
    util::load(po.getValue("scene"), *mscene);

    // Load pose
    std::string poseFile = po.getValue("pose");
    Eigen::Matrix4f pose;
    try {
        util::loadEigen(poseFile, pose);
    } catch(const std::exception &e) {
        COVIS_MSG_ERROR("Could not load pose file " << poseFile << "!");
    }

    transformPolygonMesh(mobject, pose);


//    pcl::PLYWriter pwrite;
//    pwrite.writeASCII("o.ply", mobject->cloud);
//    pwrite.writeASCII("s.ply", mscene->cloud);


    // Surfaces and normals
    float res = po.getValue<float>("resolution");
    const bool resolutionInput = (res > 0.0f);
    if(!resolutionInput)
        res = detect::computeResolution(mscene);
    const float far = po.getValue<float>("far");
    const float nrad =
            po.getValue<float>("radius-normal") > 0.0 ?
                    po.getValue<float>("radius-normal") * res:
                    2 * res;

    // Features and matching
    const float resObject =
            po.getValue<float>("resolution-object") > 0.0 ?
                    po.getValue<float>("resolution-object") * res :
                    5 * res;
    const float resScene =
            po.getValue<float>("resolution-scene") > 0.0 ?
                    po.getValue<float>("resolution-scene") * res :
                   5 * res;
    const float frad =
            po.getValue<float>("radius-feature") > 0.0 ?
                    po.getValue<float>("radius-feature") * res :
                    25 * res;


    // Preprocess
    objectSurf = filter::preprocess<PointT>(mobject, 1, true, far, res, nrad, po.getFlag("orient-object-normals"), false, true);
    sceneSurf = filter::preprocess<PointT>(mscene, 1, true, far, res, nrad, false, false, true);
    COVIS_ASSERT(!objectSurf->empty() && !sceneSurf->empty());

    // Generate feature points
    objectCloud = filter::downsample<PointT>(objectSurf, resObject);
    sceneCloud = filter::downsample<PointT>(sceneSurf, resScene);
    COVIS_ASSERT(!objectCloud->empty() && !sceneCloud->empty());

    /*
     * Compute features
     */
    {
        const std::string feat = po.getValue("feature");
        core::ScopedTimer t("Features (" + feat + ")");
        objectFeat = feature::computeFeature<PointT>(feat, objectCloud, objectSurf, frad);
        sceneFeat = feature::computeFeature<PointT>(feat, sceneCloud, sceneSurf, frad);
    }


    // Structures used for both processes (features in radius and coarse features)
    pcl::PointCloud<pcl::PointXYZ>::Ptr objectPosCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud( *objectCloud, *objectPosCloud);

    const float positiveRadius = po.getValue<float>("positive-radius");
    const float positiveRadiusSquared = positiveRadius * positiveRadius;

    const float negativeRadius = po.getValue<float>("negative-radius");
    //const float negativeRadiusSquared = negativeRadius * negativeRadius;

    const int maxPosCountRadius = po.getValue<int>("max-pos-count-radius");
    const int maxNegCountRadius = po.getValue<int>("max-neg-count-radius");
    const bool balancePosNegRadius = po.getFlag("balance-pos-neg-radius");

    const int maxNegCountCloud = po.getValue<int>("max-neg-count-cloud");


    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    {
        cout << "Starting features in radius" << endl;

        pcl::PointCloud<pcl::PointXYZ>::Ptr scenePosSurf(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud( *sceneSurf, *scenePosSurf);

        pcl::search::KdTree<pcl::PointXYZ> search(true);
        search.setInputCloud(scenePosSurf);

        std::vector<int> knn(scenePosSurf->size());
        std::vector<float> distsq(scenePosSurf->size());

        std::vector<feature::MatrixT> positivesInRadius;
        std::vector<feature::MatrixT> negativesInRadius;

        for (unsigned int i = 0; i < objectPosCloud->size(); i++) {
            std::vector<feature::MatrixT> localPositivesInRadius;
            std::vector<feature::MatrixT> localNegativesInRadius;

            // Find closest points in sceneSurf
            cout << '\r' << i << "/" << objectPosCloud->size() << " - apir: " << positivesInRadius.size() << " - anir: "
                 << negativesInRadius.size() << " - radius Search     " << std::flush;
            int neighbours = search.radiusSearch((*objectPosCloud)[i], negativeRadius, knn, distsq, scenePosSurf->size());

            if (neighbours == 0) {
                continue;
            }

            cout << '\r' << i << "/" << objectPosCloud->size() << " - apir: " << positivesInRadius.size() << " - anir: "
                 << negativesInRadius.size() << " - collecting " << neighbours << " neighbours     " << std::flush;


            pcl::PointCloud<PointT>::Ptr opposingPoints(new pcl::PointCloud<PointT>);

            for (int j = 0; j < neighbours; j++) {
                opposingPoints->push_back((*sceneSurf)[knn[j]]);
            }

            cout << '\r' << i << "/" << objectPosCloud->size() << " - apir: " << positivesInRadius.size() << " - anir: "
                 << negativesInRadius.size() << " - computing neighbour features for " << neighbours << " neighbours     "
                 << std::flush;

            const std::string feat = po.getValue("feature");
            feature::MatrixT opposingFeat;
            opposingFeat = feature::computeFeature<PointT>(feat, opposingPoints, sceneSurf, frad);
            assert(objectFeat.rows() == opposingFeat.rows());


            cout << '\r' << i << "/" << objectPosCloud->size() << " - apir: " << positivesInRadius.size() << " - anir: "
                 << negativesInRadius.size() << " - packing up " << neighbours << " neighbours     " << std::flush;
            for (int j = 0; j < neighbours; j++) {

                feature::MatrixT conc(objectFeat.rows() * 2, 1);
                conc << objectFeat.col(i), opposingFeat.col(j);

                if (distsq[j] <= positiveRadiusSquared) {
                    localPositivesInRadius.push_back(conc.transpose());
                } else {
                    localNegativesInRadius.push_back(conc.transpose());
                }
            }

            int localMaxPos;
            int localMaxNeg;

            if (balancePosNegRadius) {
                int max = std::max(maxPosCountRadius, maxNegCountRadius);
                if ((max < 0) || (localPositivesInRadius.size() < size_t(max))) {
                    max = localPositivesInRadius.size();
                }
                if (localNegativesInRadius.size() < size_t(max)) {
                    max = localNegativesInRadius.size();
                }
                localMaxPos = max;
                localMaxNeg = max;
            } else {
                if (size_t(maxPosCountRadius) > localPositivesInRadius.size()) {
                    localMaxPos = -1;
                } else {
                    localMaxPos = maxPosCountRadius;
                }
                if (size_t(maxNegCountRadius) > localNegativesInRadius.size()) {
                    localMaxNeg = -1;
                } else {
                    localMaxNeg = maxNegCountRadius;
                }
            }

            for(unsigned int j = 0; j < localPositivesInRadius.size(); j++){
                bool add = false;
                if (localMaxPos == -1) {
                    add = true;
                } else {
                    if ((((double)localMaxPos)/localPositivesInRadius.size()) > dis(gen)) {
                        add = true;
                    }

                }
                if (add) {
                    positivesInRadius.push_back(localPositivesInRadius[j]);
                }
            }

            for(unsigned int j = 0; j < localNegativesInRadius.size(); j++){
                bool add = false;
                if (localMaxNeg == -1) {
                    add = true;
                } else {
                    if ((((double)localMaxNeg)/localNegativesInRadius.size()) > dis(gen)) {
                        add = true;
                    }

                }
                if (add) {
                    negativesInRadius.push_back(localNegativesInRadius[j]);
                }
            }

            cout << '\r' << i << "/" << objectPosCloud->size() << " - apir: " << positivesInRadius.size() << " - anir: "
                 << negativesInRadius.size() << " - end      " << std::flush;
        }

        cout << endl;
        cout << "Writing radius based results out!" << endl;

        saveVec(positivesInRadius, po.getValue("output-filename-base") + "-pir.h5");
        saveVec(negativesInRadius, po.getValue("output-filename-base") + "-nir.h5");

    }

    {

        cout << "Starting features at coarse level" << endl;

        std::vector<feature::MatrixT> negativesOutOfRadius;

        pcl::PointCloud<pcl::PointXYZ>::Ptr scenePosCloud(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud(*sceneCloud, *scenePosCloud);

        for (unsigned int i = 0; i < objectPosCloud->size(); i++) {
            for (unsigned int j = 0; j < scenePosCloud->size(); j++) {
                if (pcl::geometry::distance<pcl::PointXYZ>((*objectPosCloud)[i], (*scenePosCloud)[j]) > negativeRadius) {

                    feature::MatrixT conc(objectFeat.rows() * 2, 1);
                    conc << objectFeat.col(i), sceneFeat.col(j);

                    if ((maxNegCountCloud > 0) && ((((double)maxNegCountCloud)/scenePosCloud->size()) > dis(gen))) {
                        negativesOutOfRadius.push_back(conc.transpose());
                    }

                }
            }

            cout << '\r' << i << "/" << objectPosCloud->size() << "     " << std::flush;

        }

        cout << endl;

        saveVec(negativesOutOfRadius, po.getValue("output-filename-base") + "-nr.h5");

    }

    return 0;
}
