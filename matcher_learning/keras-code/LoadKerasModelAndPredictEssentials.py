import sys, getopt, os, multiprocessing
import tensorflow as tf
from keras import backend as K
from keras.models import load_model

def rmse(y_pred, y_true):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

def load(model_filename, use_gpu=False, num_cores=0, use_custom_rmse=False):
    os.environ['TF_CPP_MIN_LOG_LEVEL']='2' # Default is probably '0'
    num_cpu = 1
    num_gpu = 1 if use_gpu else 0
    threads = multiprocessing.cpu_count() if num_cores <= 0 else num_cores

    config = tf.ConfigProto(intra_op_parallelism_threads=threads,
                            inter_op_parallelism_threads=threads,
                            allow_soft_placement=True,
                            device_count={'CPU': num_cpu, 'GPU': num_gpu})
    session = tf.Session(config=config)
    K.set_session(session)

    if use_custom_rmse:
        return load_model(model_filename, custom_objects={'rmse': rmse})
    else:
        return load_model(model_filename)

def main(argv):
    modelfile = ""
    error_string = 'test.py -m <modelfile>'
    try:
        opts, args = getopt.getopt(argv, "hm:", ["mfile"])
    except getopt.GetoptError:
        print (error_string)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print (error_string)
            sys.exit()
        elif opt in ("-m", "--mfile"):
            modelfile = arg

    if modelfile == "":
        print (error_string)
        sys.exit(2)

    print ('Model file is', modelfile)

    predict(modelfile)


if __name__ == "__main__":
    main(sys.argv[1:])
