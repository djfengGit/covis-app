#!/usr/bin/env python

import argparse, timeit, numpy
import matplotlib.pyplot as plt

import sys
sys.path.append('../../covis/build/lib')

import covis
from covis import *

sys.path.append('keras-code')
from LoadKerasModelAndPredictEssentials import load

# Setup program options
po = argparse.ArgumentParser()

po.add_argument("--scene", '-s', default="", type=str, help="scene model")
po.add_argument("--query-scale", default=1, type=float, help="sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates")
po.add_argument("--resolution-surface", '-r', default=0, type=float, help="downsample point clouds to this resolution (<= 0 for the average object resolution)")

po.add_argument("--resolution-feature", default=10, type=float, help="resolution of features in mr")
po.add_argument("--feature", '-f', default="ppfhistfull", type=str, help="name the feature to compute - possible names are: ecsad,fpfh,ndhist,ppfhist,ppfhistfull,si,usc")
po.add_argument("--radius-feature", default=40, type=float, help="feature estimation radius as a multiplum of the resolution")

po.add_argument("--net", default="", type=str, help="specify filename for matcher network, set to empty to use baseline k-NN search")
po.add_argument("--net-batch-size", default=64, type=int, help="specify batch size to use during prediction")
po.add_argument("--use-gpu", '-u', action='store_false', help="set to true to use GPU for the matcher network")

po.add_argument("--out_name", '-o', default="", type=str, help="output pcd file name")

args = po.parse_args()


# Surface scaling
queryScale = args.query_scale
resolution = args.resolution_surface
resolutionInput = resolution > 0
if not resolutionInput:
    resolution = 0

sceneFile = args.scene
if sceneFile == "":
    print "We need a scene file"
    sys.exit(-1)

sceneTmp = covis.util.load(sceneFile)

sceneSurf = filter.preprocess(mesh=sceneTmp,
                              resolution=resolution,
                              normalRadius=5 * resolution)

sceneFeat = feature.computeFeature(name=args.feature,
                                   cloud=sceneSurf,
                                   surface=sceneSurf,
                                   radius=args.radius_feature * resolution)


net = load(args.net, args.use_gpu)

sceneFeat = sceneFeat.transpose()
# sceneFeat = sceneFeat[0:1000,:]
print numpy.shape(sceneFeat)

startTime = timeit.default_timer()

predictions = net.predict(sceneFeat)
print('\tPrediction time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

startTime = timeit.default_timer()
classes = numpy.argmax(predictions,axis=1)
print('\tArgmax time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

max_scores = numpy.zeros(numpy.shape(classes)[0])

if True:
    for i in range(0,numpy.shape(classes)[0]):
        cl = classes[i]
        max_scores[i] = predictions[i,cl]

hist = numpy.histogram(max_scores)

print hist

a = sceneSurf.array()

print numpy.shape(classes)
print numpy.shape(a)

assert(numpy.shape(classes)[0] == numpy.shape(a)[1])

for i in range(0,numpy.shape(a)[1]):
    cla = classes[i]
    r = g = b = -1
    if (max_scores[i] < 0.9):
        r = 1
        g = 1
        b = 1
    elif (cla == 0):
        # Green
        # Chef
        r = 0
        g = 1
        b = 0
    elif (cla == 1):
        # Red
        # Chicken
        r = 1
        g = 0
        b = 0
    elif (cla == 2):
        # Blue
        # Para
        r = 0
        g = 0
        b = 1
    elif (cla == 3):
        # Yellow
        # T-rex
        r = 1
        g = 1
        b = 0
    elif (cla == 4):
        # Cyan
        # Rhino
        r = 0
        g = 1
        b = 1
    else:
        print "Got class ", cla, " Something is wrong!"
        sys.exit(-1)

    a[7,i] = r
    a[8,i] = g
    a[9,i] = b

cloud = core.PointCloud(a)

util.save(cloud, args.out_name)

# plt.hist(max_scores, bins='auto')
# plt.show()