#!/usr/bin/env python

import argparse, timeit, numpy, re

import sys

sys.path.append('../../covis/build/lib')

from covis import *

sys.path.append('keras-code')
from LoadKerasModelAndPredictEssentials import load

# Setup program options
po = argparse.ArgumentParser()

po.add_argument("root", type=str, help="root path of your dataset")
po.add_argument("--object-dir", '-o', default="objects", type=str, help="subdirectory for the object models")
po.add_argument("--scene-dir", '-s', default="scenes", type=str, help="subdirectory for the scene models")
po.add_argument("--pose-dir", '-p', default="ground_truth", type=str,
                help="subdirectory for the ground truth pose models")
po.add_argument("--object-ext", default=".pcd", type=str, help="object file extension")
po.add_argument("--scene-ext", default=".pcd", type=str, help="scene file extension")
po.add_argument("--pose-ext", default=".txt", type=str, help="pose file extension")
po.add_argument("--pose-sep", default="-", type=str, help="pose file separator")
po.add_argument("--object-regex", default="", type=str,
                help="set this option to use a regular expression search when collecting object files")
po.add_argument("--scene-regex", default="", type=str,
                help="set this option to use a regular expression search when collecting scene files")

po.add_argument("--query-scale", default=1, type=float,
                help="sometimes object models are given in other units, e.g. [mm] - use this value to scale the coordinates")
po.add_argument("--resolution-surface", '-r', default=0, type=float,
                help="downsample point clouds to this resolution (<= 0 for the average object resolution)")

po.add_argument("--resolution-feature", default=10, type=float, help="resolution of features in mr")
po.add_argument("--feature", '-f', default="ppfhistfull", type=str,
                help="name the feature to compute - possible names are: ecsad,fpfh,ndhist,ppfhist,ppfhistfull,si,usc")
po.add_argument("--radius-feature", default=40, type=float,
                help="feature estimation radius as a multiplum of the resolution")

po.add_argument("-knn", '-k', default=1, type=int, help="specifiy number of neighbors to search for during matching")

po.add_argument("--net-regressor", default="", type=str,
                help="specify filename for point regressor network")
po.add_argument("--net-batch-size", default=65536, type=int,
                help="specify batch size to use during matching network prediction")
po.add_argument("--use-gpu", '-u', action='store_true', help="set to true to use GPU for the matcher network")

args = po.parse_args()

print('Loading dataset from root {}...'.format(args.root))
dataset = util.loadDataset(args.root, args.object_dir, args.scene_dir, args.pose_dir, args.object_ext, args.scene_ext,
                           args.pose_ext, args.pose_sep, args.object_regex, args.scene_regex)
print('\tGot {} models, {} scenes and {} poses'.format(len(dataset.objectLabels), len(dataset.sceneLabels),
                                                       len(dataset.poseLabels)))
objects = dataset.objects

# Surface scaling
queryScale = args.query_scale
resolution = args.resolution_surface
resolutionInput = resolution > 0
if not resolutionInput:
    resolution = 0
diag = 0
for i in range(len(objects)):
    if not resolutionInput:
        resolution += detect.computeResolution(objects[i]) * queryScale
    diag += detect.computeDiagonal(objects[i]) * queryScale
if not resolutionInput:
    resolution /= len(objects)
diag /= len(objects)

objectSurf = []
objectCloud = []
sys.stdout.write('Preprocessing '), sys.stdout.flush()
for i in range(len(objects)):
    sys.stdout.write(dataset.objectLabels[i] + ' '), sys.stdout.flush()
    objectSurf.append(filter.preprocess(mesh=objects[i],
                                        scale=queryScale,
                                        resolution=resolution,
                                        normalRadius=5 * resolution,
                                        orientNormals=True))

    # Generate feature points
    objectCloud.append(filter.downsample(cloud=objectSurf[i], resolution=args.resolution_feature * resolution))
sys.stdout.write('\n'), sys.stdout.flush()

# Compute features
print('Computing {} object features with a radius of {:.3f}...'.format(sum([c.width * c.height for c in objectCloud]),
                                                                       args.radius_feature * resolution))
objectFeat = feature.computeFeature(name=args.feature,
                                    clouds=objectCloud,
                                    surfaces=objectSurf,
                                    radius=args.radius_feature * resolution)

# Loop over scenes
totalPositives = 0
totalRetrieved = 0
totalInliers = 0
for i in range(dataset.size):
    scene = dataset.at(i)
    sceneMesh = scene.scene

    print('Processing scene {}/{} ({})...'.format(i + 1, dataset.size, scene.label))
    if scene.empty:
        print('\tScene empty - skipping...')
        continue

    print('Preprocessing scene...')
    sceneSurf = filter.preprocess(mesh=sceneMesh,
                                  resolution=resolution,
                                  normalRadius=5 * resolution)

    sceneCloud = filter.downsample(cloud=sceneSurf, resolution=args.resolution_feature * resolution)

    print('Computing {} scene features...'.format(sceneCloud.size))
    sceneFeat = feature.computeFeature(name=args.feature,
                                       cloud=sceneCloud,
                                       surface=sceneSurf,
                                       radius=args.radius_feature * resolution)

    numQuery = sceneFeat.shape[1]
    numTarget = objectFeat.shape[1]
    objectFeatTranspose = numpy.transpose(objectFeat)
    sceneFeatTranspose = numpy.transpose(sceneFeat)
    numQuery = sceneFeat.shape[1]

    if len(args.net_regressor):

        print('Using network regressor for {} scene features...'.format(numQuery))
        if args.use_gpu:
            print('\tUsing GPU')

        netRegressor = load(args.net_regressor, args.use_gpu, use_custom_rmse=True)
        startTime = timeit.default_timer()
        predictions = netRegressor.predict(sceneFeatTranspose, batch_size=numQuery)
        assert predictions.shape[0] == sceneFeat.shape[1]
        assert predictions.shape[1] == 3
        featureCorr = detect.computeKnnMatches(predictions.T, objectCloud[0].array()[0:3,:], args.knn)
        print('\tRegression based matching time: {:.3f} seconds'.format(timeit.default_timer() - startTime))
    else:
        print('Using baseline k-NN matcher for {} scene features...'.format(numQuery))
        startTime = timeit.default_timer()
        featureCorr = detect.computeKnnMatches(sceneFeat, objectFeat, args.knn)
        print('\tBaseline k-NN matching time: {:.3f} seconds'.format(timeit.default_timer() - startTime))

    featureCorr = core.sort(featureCorr)
    featureCorr = featureCorr[0:numQuery * args.knn]

    objectMask = scene.objectMask
    objectCloudAll = core.PointCloud()
    idxVisible = 0
    for j in range(len(objectCloud)):
        if objectMask[j]:
            tmp = core.transform(objectCloud[j], scene.poses[idxVisible])
            idxVisible += 1
        else:
            tmp = core.PointCloud(size=objectCloud[j].size)
        objectCloudAll += tmp

    if len(objectCloud) > 1:
        raise Exception('Not implemented yet!')

    inlierThreshold = args.resolution_feature
    print('Verifying matches with an inlier threshold of {}...'.format(inlierThreshold))

    corrTotal = core.computeAllCorrespondences(sceneCloud.size, objectCloudAll.size, distance=-1)
    scenePositives = detect.filterCorrespondencesTransform(corrTotal,
                                                           sceneCloud,
                                                           objectCloudAll,
                                                           core.Identity(),
                                                           inlierThreshold)
    scenePositives = len(core.unique(scenePositives, args.knn))
    totalPositives += scenePositives

    sceneRetrieved = len(featureCorr)
    totalRetrieved += sceneRetrieved

    inliers = detect.filterCorrespondencesTransform(featureCorr,
                                                    sceneCloud,
                                                    objectCloudAll,
                                                    core.Identity(),
                                                    inlierThreshold)
    sceneInliers = len(inliers)
    totalInliers += sceneInliers

    print('Stats for scene {}'.format(scene.label))
    print('\t{} feature matches retrieved (searching for {}-NNs)'.format(len(featureCorr), args.knn))
    print('\t{} possible {}-NN matches'.format(scenePositives, args.knn))
    print('\tRecall for scene:    {}/{} ({:.2f} %)'.format(sceneInliers, scenePositives,
                                                           100 * sceneInliers / float(scenePositives)))
    print('\tPrecision for scene: {}/{} ({:.2f} %)'.format(sceneInliers, sceneRetrieved,
                                                           100 * sceneInliers / float(sceneRetrieved)))

    print('Overall stats')
    print(
    '\tRecall:      {}/{} ({:.2f} %)'.format(totalInliers, totalPositives, 100 * totalInliers / float(totalPositives)))
    print(
    '\tPrecision:   {}/{} ({:.2f} %)'.format(totalInliers, totalRetrieved, 100 * totalInliers / float(totalRetrieved)))

