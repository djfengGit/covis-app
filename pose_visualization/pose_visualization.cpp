// STL
#include <fstream>

// PCL
#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>

// Eigen
#include <eigen3/Eigen/Core>

// Covis
#include <covis/covis.h>

using namespace pcl;
using namespace std;

typedef PointXYZ PointT;

PointCloud<PointT>::Ptr cloudSrc, cloudDst;
PolygonMesh::Ptr meshSrc, meshDst;

int main (int argc, char** argv) {
    if(argc < 4) {
        COVIS_MSG("Usage:\n\t" << argv[0] << " <source_cloud> <target_cloud> <pose_file> [scale]");
        COVIS_MSG("\tScene and target clouds can be either in pcd or ply format");
        return 0;
    }

    // Load source
    if(covis::core::extension(argv[1]) == "ply") {
	meshSrc.reset(new PolygonMesh);
    	if (!covis::util::loadPLYFile(argv[1], *meshSrc)) {
	    COVIS_MSG_ERROR("Couldn't read file " << argv[1]);
	    return -1;
    	}
    } else if(covis::core::extension(argv[1]) == "pcd") {
	cloudSrc.reset(new PointCloud<PointT>);
	if (io::loadPCDFile<PointT> (argv[1], *cloudSrc) == -1) {
	    COVIS_MSG_ERROR("Couldn't read file " << argv[1]);
	    return -1;
	}
    } else {
	COVIS_THROW("Unknown file format for file: " << argv[1]);
    }

    // Load destination
    if(covis::core::extension(argv[2]) == "ply") {
	meshDst.reset(new PolygonMesh);
    	if (!covis::util::loadPLYFile(argv[2], *meshDst)) {
	    COVIS_MSG_ERROR("Couldn't read file " << argv[2]);
	    return -1;
    	}
    } else if(covis::core::extension(argv[2]) == "pcd") {
	cloudDst.reset(new PointCloud<PointT>);
	if (io::loadPCDFile<PointT> (argv[2], *cloudDst) == -1) {
	    COVIS_MSG_ERROR("Couldn't read file " << argv[2]);
	    return -1;
	}
    } else {
	COVIS_THROW("Unknown file format for file: " << argv[2]);
    }
    
    // Make both models mesh/cloud
    if(meshSrc && !meshDst) {
	cloudSrc.reset(new PointCloud<PointT>);
	fromPCLPointCloud2<PointT>(meshSrc->cloud, *cloudSrc);
	meshSrc.reset();
    } else if(!meshSrc && meshDst) {
	cloudDst.reset(new PointCloud<PointT>);
	fromPCLPointCloud2<PointT>(meshDst->cloud, *cloudDst);
	meshDst.reset();
    }
    
    // Load pose
    Eigen::Matrix4f T;
    covis::util::loadEigen(argv[3], T);
    
    // Load scale
    double scale = 1;
    if(argc > 4)
	scale = atof(argv[4]);
	    
    if(scale != 1) {
	COVIS_MSG_INFO("Scaling translation component by " << scale << "...");
	T(0,3) *= scale;
	T(1,3) *= scale;
	T(2,3) *= scale;
    }
    
    // Show
    if(meshSrc && meshDst)
	covis::visu::showDetection(meshSrc, meshDst, T);
    else
	covis::visu::showDetection<PointT>(cloudSrc, cloudDst, T);

    return 0;
}
