// STL
#include <sstream>

// PCL
#include <pcl/filters/passthrough.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>

using namespace pcl;
using namespace std;

// Define point type

int main(int argc, char** argv) {
    // Get inputs
    if(argc != 4) {
        cout << "Usage: " << argv[0] << " <input.pcd> <output.pcd> <xmin,xmax,ymin,ymax,zmin,zmax>" << endl;
        return 0;
    }
    
    // Get input point
    cout << "Loading point cloud " << argv[1] << "..." << endl;
    PCLPointCloud2Ptr cloudIn(new PCLPointCloud2);
    io::loadPCDFile(argv[1], *cloudIn);
    
    // Get filter limits
    istringstream ss(argv[3]);
    string token;
    float xmin, xmax, ymin, ymax, zmin, zmax;
    getline(ss, token, ','); xmin = atof(token.c_str());
    getline(ss, token, ','); xmax = atof(token.c_str());
    getline(ss, token, ','); ymin = atof(token.c_str());
    getline(ss, token, ','); ymax = atof(token.c_str());
    getline(ss, token, ','); zmin = atof(token.c_str());
    getline(ss, token, ','); zmax = atof(token.c_str());
    assert(xmax >= xmin && ymax >= ymin && zmax >= zmin);
    
    cout << "Using filter limits:" << endl;
    cout << "\tx: [" << xmin << " " << xmax << "]" << endl;
    cout << "\ty: [" << ymin << " " << ymax << "]" << endl;
    cout << "\tz: [" << zmin << " " << zmax << "]" << endl;
    
    // Output point cloud
    PCLPointCloud2Ptr cloudOut(new PCLPointCloud2);

    // Initializing with true will allow us to extract the points INSIDE the boundaries
    PassThrough<PCLPointCloud2> ptfilter (true);

    // Filter in the x dimension
    ptfilter.setInputCloud (cloudIn);
    ptfilter.setFilterFieldName ("x");
    ptfilter.setFilterLimits (xmin, xmax);
    ptfilter.filter (*cloudOut); // Contains all points within x limits

    // Now do the same in the y dimension, but only for the remaining points after x pruning
    ptfilter.setInputCloud (cloudOut);
    ptfilter.setFilterFieldName ("y");
    ptfilter.setFilterLimits (ymin, ymax);
    ptfilter.filter (*cloudOut); // Contains all points within both x and y limits

    // And now for z, and directly return the result as a point cloud
    ptfilter.setInputCloud (cloudOut);
    ptfilter.setFilterFieldName ("z");
    ptfilter.setFilterLimits (zmin, zmax);
    ptfilter.filter (*cloudOut); // Output contains points within all three limits
    
    // Save output
    cout << "Saving filtered point cloud to " << argv[2] << "..." << endl;
    io::savePCDFile(argv[2], *cloudOut, Eigen::Vector4f::Zero(), Eigen::Quaternionf::Identity(), true);
    
    return 0;
}
