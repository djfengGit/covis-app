// CoViS
#include <covis/covis.h>
using namespace covis;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    COVIS_MSG_INFO("Hello world from CoViS!");
    
    return 0;
}
