cmake_minimum_required(VERSION 2.8)

project(pose_estimation_acat)

find_package(COVIS HINTS ../../covis-new ../../covis-new/build)
find_package(Qt4 COMPONENTS QtCore QtGui QtNetwork REQUIRED)
include(${QT_USE_FILE})

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

set(pose_estimation_acat_HEADERS mainwindow.h)
set(pose_estimation_acat_SOURCES mainwindow.cpp main.cpp)
set(pose_estimation_acat_UIS mainwindow.ui)

qt4_wrap_cpp(pose_estimation_acat_MOC ${pose_estimation_acat_HEADERS})
qt4_wrap_ui(pose_estimation_acat_UIS_H ${pose_estimation_acat_UIS})


include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_executable(pose_estimation_acat
    ${pose_estimation_acat_SOURCES}
    ${pose_estimation_acat_MOC}
    ${pose_estimation_acat_UIS_H}
)

target_link_libraries(pose_estimation_acat ${QT_LIBRARIES} ${COVIS_LIBRARIES} QVTK)
include_directories(${COVIS_INCLUDE_DIRS})
add_definitions(${COVIS_DEFINITIONS})



