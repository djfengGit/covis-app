#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "QVTKWidget.h"

#include <pcl/visualization/cloud_viewer.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QString intrinsicFileName;
    QString extrinsicFileName;
    QString leftImageQString;
    QString rightImageQString;
    QString imgQString;
    void showPointCloud(QVTKWidget* widget, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud);
    void poseEstimation(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud);

private slots:
    void loadStereoImgsEvent();
    void loadPCDImgsEvent();
    // void loadPcdEvent();
    QString loadIntrinsicEvent();
    QString loadExtrinsicEvent();
    void start();

};

#endif // MAINWINDOW_H
