#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include "QVTKWidget.h"
#include <vtkRenderWindow.h>

#include <covis/covis.h>
#include "covis/calib/stereo_match.h"

#include <opencv2/imgproc/imgproc_c.h>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/features/shot_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>

// Types
typedef pcl::PointNormal PointNT;
typedef pcl::SHOT352 FeatureT;
typedef pcl::ReferenceFrame RFT;

// Objects
pcl::PointCloud<PointNT>::Ptr query, target;
pcl::PointCloud<FeatureT>::Ptr fquery, ftarget;
pcl::PointCloud<RFT>::Ptr rfquery, rftarget;



using namespace covis;
using namespace covis::calib;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //QPixmap pixmap("/home/lilita/Desktop/blueButton.png");
    //ui->startButton->setIcon(pixmap);
    //ui->startButton->setIconSize(pixmap.rect().size());

    //ui->startButton->setFixedHeight(100);
    //ui->startButton->setFixedWidth(100);

    //Set Starting point of region 5 pixels inside , make region width & height
    //values same and less than button size so that we obtain a pure-round shape
    //QRegion* region = new QRegion(*(new QRect(ui->startButton->x()+5,ui->startButton->y()+5,20,20)),QRegion::Ellipse);
    //ui->startButton->setMask(*region);

    connect(ui->loadStereoImgsButton, SIGNAL(clicked()), this, SLOT(loadStereoImgsEvent()));
    connect(ui->selectIntrinsicButton, SIGNAL(clicked()), this, SLOT(loadIntrinsicEvent()));
    connect(ui->selectExtrinsicButton, SIGNAL(clicked()), this, SLOT(loadExtrinsicEvent()));
    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(start()));
    connect(ui->LoadPcdButton, SIGNAL(clicked()), this, SLOT(loadPCDImgsEvent()));
    ui->stereoGroupBox->setVisible(false);
    //ui->stereoWidget->setVisible(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadStereoImgsEvent()
{
    this->leftImageQString = QFileDialog::getOpenFileName(this, tr("Open File"),"", tr("Files (*.*)"));

    this->rightImageQString = QFileDialog::getOpenFileName(this, tr("Open File"),leftImageQString , tr("Files (*.*)"));
    //show selected images in gui
    QPixmap pixLeft = QPixmap(leftImageQString);
    QPixmap pixRight = QPixmap(rightImageQString);
    ui->leftImgView->setPixmap(pixLeft.scaled(ui->leftImgView->size(), Qt::KeepAspectRatio));
    ui->rightImgView->setPixmap(pixRight.scaled(ui->rightImgView->size(), Qt::KeepAspectRatio));
    ui->leftImgLabel->setText("Left Image");
    ui->rightImgLabel->setText("Right Image");
    ui->stereoGroupBox->setVisible(true);
}

QString MainWindow::loadIntrinsicEvent()
{
    this->intrinsicFileName = QFileDialog::getOpenFileName(this, tr("Open File"),this->leftImageQString, tr("Files (*.yml)"));
    ui->intrinsicText->setText(this->intrinsicFileName);
}

QString MainWindow::loadExtrinsicEvent()
{
    this->extrinsicFileName = QFileDialog::getOpenFileName(this, tr("Open File"),this->leftImageQString, tr("Files (*.yml)"));
    ui->extrinsicText->setText(this->extrinsicFileName);
}

void MainWindow::start()
{
    StereoMatch sm;
    sm.setAlgorithm(ui->algorithmComboBox->currentIndex());
    sm.setMaxDisparity(ui->maxDisparityText->toPlainText().toInt());
    sm.setMinDisparity(ui->minDisparityText->toPlainText().toInt());
    sm.setBlockSize(ui->blockSizeText->toPlainText().toInt());
    sm.setVerbose(false);
    sm.setRectified(ui->rectifiedCheckBox->checkState());
    //qDebug() << "left: " ; //<< this->leftImageQString.toStdString();
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud = sm.compute(this->leftImageQString.toStdString(), this->rightImageQString.toStdString(),
                                                                    ui->intrinsicText->toPlainText().toStdString(), ui->extrinsicText->toPlainText().toStdString());

    showPointCloud(ui->qvtkWidget, pointCloud);
}

void MainWindow::showPointCloud(QVTKWidget* widget, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud)
{
    vtkSmartPointer<vtkRenderWindow> renderWindow;

    pcl::visualization::PCLVisualizer localPclVisor = pcl::visualization::PCLVisualizer("pclVisorName",false);

    localPclVisor.addPointCloud(pointCloud);
    localPclVisor.setBackgroundColor(0,0,0);
    renderWindow = localPclVisor.getRenderWindow();
    widget->SetRenderWindow(renderWindow);
    widget->show();
}

void MainWindow::loadPCDImgsEvent(){
    this->leftImageQString = QFileDialog::getOpenFileName(this, tr("Open File"),"", tr("Files (*.pcd)"));
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile (this->leftImageQString.toStdString(), *cloud);
    //get the rgb image
    ConvertInput<pcl::PointXYZRGBA> ci;
    cv::Mat rgb = ci.getRgbImageFromPointCloud(cloud);
    cv::imwrite("rgb.png", rgb);
    QPixmap pixLeft = QPixmap("rgb.png");
    ui->leftImgView->setPixmap(pixLeft.scaled(ui->leftImgView->size(), Qt::KeepAspectRatio));

}

void MainWindow::poseEstimation(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud){

}
