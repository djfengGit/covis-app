clear;clc;close all;

% Parameters
output_folder = 'output_mian'; % Folder containing all output data files
methods = {'l2', 'ratio', 'gc', 'voting'}; % For finding file names
method_names = {'L2 distance', 'Ratio', 'GC', 'Voting'}; % For plot legends etc.
styles = {'-dm', '-+r', '-*b', '-sk'}; % Plot styles for the methods
font_size = 18;
data_points = 25; % Number of data points to use for the PR curves
plot_all = 1; % Set to 1 to plot ALL data samples for each method

%
% Start processing data files
%
method_data = cell(1, numel(methods)); % All PR samples, unused for now
recall_points = linspace(0.5/data_points, 1-0.5/data_points, data_points)'; % Recall data points (avoiding 0 and 1)
precision_mean = cell(1, numel(methods)); % Mean precision over all scenes at the recall data points
precision_median = cell(1, numel(methods)); % Median precision
for i = 1:numel(methods)
    % Load data
    method_files = dir([output_folder '/*' methods{i} '*']);
    fprintf('Colllecting PR results for method %s from %i data files...\n',...
        method_names{i}, numel(method_files));
    % Get precision values at the recall points, one column per file
    precisions = zeros(data_points, numel(method_files));
    for j = 1:numel(method_files)
        method_file_data = dlmread([output_folder '/' method_files(j).name]);
        method_data{i} = [method_data{i} ; method_file_data];
        
        % All precision-recall data samples
        precision = method_data{i}(:,1);
        recall = method_data{i}(:,2);
        % Find the indices of the recall data points
        idx_recall_points = zeros(1, data_points);
        for k=1:data_points
            idx_recall_points(k) = find(recall <= recall_points(k), 1, 'last');
        end
        precisions(:,j) = precision(idx_recall_points);
    end
    
    precision_mean{i} = mean(precisions, 2);
    precision_median{i} = median(precisions, 2);
end

%
% Plot mean/median PR curves
%
figure('Name', 'Mean precision results')
hmean = gca;
hold(hmean);
figure('Name', 'Median precision results')
hmed = gca;
hold(hmed)
for i = 1:numel(methods)
    plot(hmean,...
        1-precision_mean{i}, recall_points,...
        styles{i}, 'LineWidth', 2)
    plot(hmed,...
        1-precision_median{i}, recall_points,...
        styles{i}, 'LineWidth', 2)
end
legend(hmean, method_names, 'Location', 'SouthEast');
set(hmean, 'fontsize', font_size);
xlabel(hmean, '1-precision'), ylabel('recall');
xlim(hmean, [0 1]), ylim(hmean, [0 1])

legend(hmed, method_names, 'Location', 'SouthEast');
set(hmed, 'fontsize', font_size);
xlabel(hmed, '1-precision'), ylabel('recall');
xlim(hmed, [0 1]), ylim(hmed, [0 1])

%
% Present max F1 scores
%
for i=1:numel(methods)
    fprintf('Max F1 score for method %s:\n', method_names{i});
    fprintf('\tUsing mean PR: %f\n', ...
        max(2 * precision_mean{i} .* recall_points ./ (precision_mean{i} + recall_points)));
    fprintf('\tUsing median PR: %f\n', ...
        max(2 * precision_median{i} .* recall_points ./ (precision_median{i} + recall_points)));
end


%
% If chosen, plot ALL PR curves in dataset
%
if plot_all
    figure('Name', 'Raw data')
    for i = 1:numel(methods)
        subplot(2, ceil(numel(methods)/2), i)
        plot(1 - method_data{i}(:,1), method_data{i}(:,2), ...
            ['.' styles{i}(end)], 'MarkerSize', 1)
        xlabel(gca, '1-precision'), ylabel('recall');
        xlim(gca, [0 1]), ylim(gca, [0 1])
        title(method_names{i}, 'FontWeight', 'Bold')
    end
end
