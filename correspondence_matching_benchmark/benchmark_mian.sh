#!/bin/bash

# Make sure output folder exists
if [ ! -d output_mian ]; then mkdir output_mian; fi

rm output_mian/*

# Model to use [chef, chicken, parasaurolophus, T-rex]
object_name=parasaurolophus

# Params
flags="--scale --verbose"
minfrac=0.01
thres=0.004
nrad=0.01
frad=0.015
voting_samples=250
voting_similarity=0.9

# Paths
data_dir=../data/mian_2mm
exec_dir=build

# Data set
model_object=$data_dir/models_mm_2/$object_name.pcd

model_scene_path=$data_dir/scenes_mm_2
model_scene_base=rs
model_scene_prefix=$model_scene_path/$model_scene_base
model_scene_suffix=.ply.pcd

pose_prefix=$data_dir/GroundTruth_3Dscenes/$object_name-rs
pose_suffix=.xf

for i in {1..1}
do
    echo "---------- TESTING OBJECT/SCENE PAIR $i: ----------"
    echo -e "\t"$model_object
    echo -e "\t"$model_scene_prefix"$i"$model_scene_suffix"\n"
    
    $exec_dir/correspondence_matching_pairwise \
    	$model_object $model_scene_prefix"$i"$model_scene_suffix $pose_prefix"$i"$pose_suffix \
    	$flags --radius-normal=$nrad --radius-feature=$frad --minfrac=$minfrac --thres=$thres \
    	--voting-samples=$voting_samples --voting-similarity=$voting_similarity \
    	--result-prefix=output_mian/"$object_name"_"$model_scene_base"$i \
    	$* #2>/dev/null
    
    stat=$?
    if [ $stat -ne 0 ]; then echo "FAILED TO GENERATE OUTPUT DATA FOR SCENE $i! STATUS CODE: $stat"; fi
    
    echo -e "---------- SCENE $i DONE! ----------\n"
done

echo -e "\n---------- ALL DONE! ----------"
ding
