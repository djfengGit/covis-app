// Boost
#include <boost/filesystem.hpp>
namespace fs=boost::filesystem;

// CoViS
#include <covis/covis.h>
using namespace covis;

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/features/shot_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/recognition/cg/geometric_consistency.h>

// Types
typedef pcl::PointNormal PointNT;
typedef pcl::SHOT352 FeatureT;
typedef pcl::ReferenceFrame RFT;

// Objects
pcl::PointCloud<PointNT>::Ptr query, target;
pcl::PointCloud<FeatureT>::Ptr fquery, ftarget;
pcl::PointCloud<RFT>::Ptr rfquery, rftarget;

// Normal computation function for a single indexed point in the input, based on NNs
template<typename NormalT>
void normal(pcl::PointCloud<NormalT>& input, int src, const std::vector<int>& idx);

// Output PR results
void pr(const core::Correspondence::VecPtr corr,
        const std::vector<bool>& mask,
        const std::vector<float>& conf,
        const std::string& filename);

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcd-query", "point cloud file for query point cloud");
    po.addPositional("pcd-target", "point cloud file for target point cloud");
    po.addPositional("pose-file", "ground truth pose for placing the query into the target");
    
    po.addOption("thres", 0.004, "Euclidean inlier threshold, must be > 0");
    po.addOption("minfrac", 0.0, "minimum required fraction of inliers [0,1]");
    po.addOption("radius-normal", 0.01, "normal estimation radius (set to <= for disabled)");
    po.addOption("radius-feature", 0.015, "feature estimation radius");
    
    po.addOption("voting-samples", 250, "number of samples (neighbors) to consider during correspondence voting");
    po.addOption("voting-similarity", 0.9, "edge length similiarity threshold for correspondence voting");
    
    po.addOption("result-prefix", "",
            "result file prefix: if not empty, this program will generate one PR file per method (L2, Lowe, voting)");
    
    po.addFlag('c', "disable-correction", "disable normal correction for query");
    po.addFlag('s', "scale", "scale pose and point clouds from [mm] --> [m]");
    po.addFlag('v', "verbose", "print debug information");
    po.addFlag('i', "visualize", "enable visualizations");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const double thres = po.getValue<double>("thres");
    const double minfrac = po.getValue<double>("minfrac");
    const double nrad = po.getValue<double>("radius-normal");
    const double frad = po.getValue<double>("radius-feature");
    
    const size_t votingSamples = po.getValue<size_t>("voting-samples");
    const double votingSimilarity = po.getValue<double>("voting-similarity");
    
    const std::string resultPrefix = po.getValue("result-prefix");
    
    const bool verbose = po.getFlag("verbose");
    const bool visualize = po.getFlag("visualize");
    
    // Load point clouds
    if(verbose)
        COVIS_MSG_INFO("Loading point clouds and GT pose...");
    query.reset(new pcl::PointCloud<PointNT>);
    target.reset(new pcl::PointCloud<PointNT>);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointNT>(po.getValue("pcd-query"), *query) == 0);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointNT>(po.getValue("pcd-target"), *target) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*query, *query, dummy);
    pcl::removeNaNFromPointCloud(*target, *target, dummy);
    COVIS_ASSERT(!query->empty());
    COVIS_ASSERT(!target->empty());
    
    // Load pose
    Eigen::Matrix4f pose;
    core::read(po.getValue("pose-file"), pose);
    
    // Apply scaling
    if(po.getFlag("scale")) {
        if(verbose)
            COVIS_MSG_INFO("Scaling point clouds and GT pose...");
        BOOST_FOREACH(PointNT& p, *query)
            p.getVector3fMap() *= 0.001;
        BOOST_FOREACH(PointNT& p, *target)
            p.getVector3fMap() *= 0.001;
        pose.block<3,1>(0,3) *= 0.001;
    }
    
    COVIS_MSG_WARN("Got pose:" << std::endl << pose);
    
    // Create a timer for the preprocessing
    core::ScopedTimer::Ptr t(new core::ScopedTimer("Preprocessing"));
    
    /*
     * Compute normals
     */
    if(nrad > 0.0f) {
        if(verbose)
            COVIS_MSG_INFO("Computing normals...");
        
        
        // PCL VERSION
        pcl::NormalEstimationOMP<PointNT, PointNT> ne1;
        ne1.setRadiusSearch(nrad);
        ne1.setInputCloud(query);
        ne1.compute(*query);

        pcl::NormalEstimationOMP<PointNT, PointNT> ne2;
        ne2.setRadiusSearch(nrad);
        ne2.setInputCloud(target);
        ne2.compute(*target);
        
        // Correction
        if(!po.getFlag('c')) {
            if(verbose)
                COVIS_MSG_INFO("Pointing query normals outwards...");
            
            feature::computeCorrectedNormals(*query);
        }
        
        t->intermediate("normals");
    }
    
    /*
     * Compute features and get RFs
     */
    if(verbose)
        COVIS_MSG_INFO("Computing features...");
    fquery.reset(new pcl::PointCloud<FeatureT>);
    ftarget.reset(new pcl::PointCloud<FeatureT>);

    {
        pcl::SHOTEstimationOMP<PointNT,PointNT,FeatureT,RFT> se;
        se.setLRFRadius(frad);
        se.setRadiusSearch(frad);
        
        se.setInputCloud(query);
        se.setInputNormals(query);
        se.compute(*fquery);
    }

    {
        pcl::SHOTEstimationOMP<PointNT,PointNT,FeatureT,RFT> se;
        se.setLRFRadius(frad);
        se.setRadiusSearch(frad);
        
        se.setInputCloud(target);
        se.setInputNormals(target);
        se.compute(*ftarget);
    }
    
    // Extract RFs
    rfquery.reset(new pcl::PointCloud<RFT>(fquery->width, fquery->height));
    rftarget.reset(new pcl::PointCloud<RFT>(ftarget->width, ftarget->height));
    for(size_t i = 0; i < fquery->size(); ++i)
        std::memcpy(rfquery->points[i].rf, fquery->points[i].rf, 9 * sizeof(float));
    for(size_t i = 0; i < ftarget->size(); ++i)
        std::memcpy(rftarget->points[i].rf, ftarget->points[i].rf, 9 * sizeof(float));
    
    t->intermediate("features");
    
    // Matching results
    core::Correspondence::VecPtr corr;
    
    /*
     * Match features, but only if not done before
     */
    const std::string corrFile = resultPrefix + ".corr.txt";
    if(fs::is_regular_file(fs::path(corrFile))) {
        if(verbose)
            COVIS_MSG("--> Loading matched features from " << corrFile << "...");
        corr.reset(new core::Correspondence::Vec);
        core::load(corrFile, *corr);
    } else {
        if(verbose)
            COVIS_MSG_INFO("Matching features (L2)...");
        // All feature correspondences
        corr = detect::computeKnnMatches<FeatureT>(fquery, ftarget, 2, 4, 128);
        if(!resultPrefix.empty()) {
            if(verbose)
                COVIS_MSG("--> Saving matched features to " << corrFile << "...");
            core::save(corrFile, *corr);
        }
        
        t->intermediate("matching");
    }
    
    
    // In PCL format for the GC methods
    pcl::CorrespondencesPtr corrpcl = core::convert(*corr);
    
    if(verbose)
        COVIS_MSG_INFO("Finding inliers...");
    
    // Find the inliers
    detect::CorrespondenceFilterTransform<PointNT> cft;
    cft.setQuery(query);
    cft.setTarget(target);
    cft.setTransformation(pose);
    cft.setThreshold(thres);
    core::Correspondence::VecPtr inliers = cft.filter(*corr);
    
    
    COVIS_ASSERT_MSG(inliers->size() >= size_t(minfrac * corr->size() + 0.5),
            "Too few inliers (" << 100.0f * float(inliers->size()) / float(corr->size()) << " % < " <<
            100.0 * minfrac << " %)!");
    
    // Inlier mask, used for benchmarking further results
    const std::vector<bool>& mask = cft.getMask();
    COVIS_ASSERT(mask.size() == corr->size());
    if(verbose)
        COVIS_MSG_WARN("Found " << 100.0f * float(inliers->size()) / float(corr->size()) << " % inliers!");
    
    t->intermediate("inliers");
    
    /*
     * Compute k-NN neighbors on query for each feature point
     */
    if(verbose)
        COVIS_MSG_INFO("Computing query neighbors (k = " << votingSamples << ")...");
    core::Correspondence::VecPtr neighbors(new core::Correspondence::Vec(corr->size()));
    // Create search
    typename pcl::search::Search<PointNT>::Ptr searchsrc;
    if(query->isOrganized())
        searchsrc.reset(new pcl::search::OrganizedNeighbor<PointNT>);
    else
        searchsrc.reset(new pcl::search::KdTree<PointNT>);
    searchsrc->setInputCloud(query);
    // Do search
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < corr->size(); ++i)
        searchsrc->nearestKSearch(query->points[(*corr)[i].query], int(votingSamples + 1),
                (*neighbors)[i].match, (*neighbors)[i].distance);
    
    // Stop timing the preprocessing
    t.reset();
    
    /*
     * Show every 10th feature correspondence if visualization is turned on
     */
    if(visualize)
        visu::showCorrespondences<PointNT>(
                query, target, *corr, 10, "Initial L2 correspondences (every 10th)");
    
    /*
     * L2 confidences
     */
    std::vector<float> confL2(corr->size());
    for(size_t i = 0; i < corr->size(); ++i)
        confL2[i] = -(*corr)[i].distance[0];
    
    if(visualize) {
        core::Correspondence::Vec corrsort;
        std::vector<float> confsort = confL2;
        corrsort = core::reorder(*corr, core::sort(confsort, false));
        corrsort.resize(std::min<size_t>(500, corr->size()));
        visu::showCorrespondences<PointNT>(
                query, target, corrsort, 1, "500 best L2 correspondences");
    }
    
    /*
     * Lowe ratio confidences
     */
    std::vector<float> confRatio(corr->size());
    double timeRatio;
    {
        if(verbose)
            COVIS_MSG_INFO("Performing ratio ranking...");
        core::ScopedTimer t("Ratio");
        for(size_t i = 0; i < corr->size(); ++i)
            confRatio[i] = -(*corr)[i].distance[0] / (*corr)[i].distance[1];
        timeRatio = t.seconds();
    }
    
    if(visualize) {
        core::Correspondence::Vec corrsort;
        std::vector<float> confsort = confRatio;
        corrsort = core::reorder(*corr, core::sort(confsort, false));
        corrsort.resize(std::min<size_t>(500, corr->size()));
        visu::showCorrespondences<PointNT>(
                query, target, corrsort, 1, "500 best ratio correspondences");
    }
    
    /*
     * GC correspondence grouping
     */
    std::vector<float> confGC;
    double timeGC;
    {
        pcl::GeometricConsistencyGrouping<PointNT,PointNT> gcg;
        gcg.setGCSize(0.005);
        gcg.setGCThreshold(5);
        gcg.setInputCloud(query);
        gcg.setSceneCloud(target);
        gcg.setModelSceneCorrespondences(corrpcl);
        std::vector<pcl::Correspondences> corrcgpcl;
        core::ScopedTimer t("Geometric consistency");
        gcg.cluster(corrcgpcl);
        timeGC = t.seconds();
        confGC.resize(corr->size(), 0.0f);
        for(size_t i = 0; i < corrcgpcl.size(); ++i)
            for(size_t j = 0; j < corrcgpcl[i].size(); ++j)
                confGC[ corrcgpcl[i][j].index_query ] = float(corrcgpcl[i].size()) / float(corrpcl->size());
    }
    
    if(visualize) {
        core::Correspondence::Vec corrsort;
        std::vector<float> confsort = confGC;
        corrsort = core::reorder(*corr, core::sort(confsort, false));
        corrsort.resize(std::min<size_t>(500, corr->size()));
        visu::showCorrespondences<PointNT>(
                query, target, corrsort, 1, "500 best GC correspondences");
    }
    
    /*
     * Correspondence voting
     */
    std::vector<float> confVoting;
    double timeVoting;
    {
        detect::CorrespondenceVoting<PointNT,RFT> voting;
        voting.setSamples(votingSamples);
        voting.setSimilarity(votingSimilarity);
        voting.setQuery(query);
        voting.setTarget(target);
        voting.setQueryRF(rfquery);
        voting.setTargetRF(rftarget);
        voting.setQueryFeatureNeighbors(neighbors);
        core::ScopedTimer t("Correspondence voting");
        // Voting correspondences
        core::Correspondence::VecPtr corrv = voting.filter(*corr);
        timeVoting = t.seconds();
        // Voting confidences
        confVoting = voting.getConfidences();
        
        /*
         * Show voting result
         */
        if(visualize)
            visu::showCorrespondences<PointNT>(
                    query, target, *corrv, 1, "Voting correspondences");
    }
    
    /*
     * Generate Matlab-friendly output files
     */
    if(!resultPrefix.empty()) {
        if(verbose) {
            COVIS_MSG_INFO("Outputting PR results:");
            COVIS_MSG('\t' << resultPrefix << ".l2.txt");
            COVIS_MSG('\t' << resultPrefix << ".ratio.txt");
            COVIS_MSG('\t' << resultPrefix << ".gc.txt");
            COVIS_MSG('\t' << resultPrefix << ".voting.txt");
        }
        
        pr(corr, mask, confL2, resultPrefix+".l2.txt");
        pr(corr, mask, confRatio, resultPrefix+".ratio.txt");
        pr(corr, mask, confGC, resultPrefix+".gc.txt");
        pr(corr, mask, confVoting, resultPrefix+".voting.txt");
        
        if(verbose) {
            COVIS_MSG_INFO("Outputting meta data:");
            COVIS_MSG('\t' << resultPrefix << ".meta.txt");
        }
        
        std::ofstream ofs((resultPrefix + ".meta.txt").c_str());
        ofs << "# correspondences_size inliers_size time_ratio time_gc time_voting" << std::endl;
        ofs << corr->size() << " " << inliers->size() << " " <<
                timeRatio << " " << timeGC << " " << timeVoting << std::endl;
    }
    
    if(verbose)
        COVIS_MSG_INFO("All done!");
        
    return 0;
}


template<typename NormalT>
void normal(pcl::PointCloud<NormalT>& input, int src, const std::vector<int>& idx) {
    // Number of points in support
    const int sizec = idx.size();
    const float sizecf(sizec);
    
    // NNs in an OpenCV matrix (row-major), and their mean
    cv::Mat_<float> nncv(sizec, 3);
    float xmean = 0.0f;
    float ymean = 0.0f;
    float zmean = 0.0f;
    for (int i = 0; i < sizec; ++i) {
        // NN
        const NormalT& nni = input[idx[i]];
        // Data
        nncv[i][0] = nni.x;
        nncv[i][1] = nni.y;
        nncv[i][2] = nni.z;
        
        xmean += nni.x;
        ymean += nni.y;
        zmean += nni.z;
    }
    xmean /= sizecf;
    ymean /= sizecf;
    zmean /= sizecf;
    
    // Covariance matrix, only compute upper right part
    cv::Mat_<float> cov(3, 3);
    cov[0][0] = 0.0f;
    cov[0][1] = 0.0f;
    cov[0][2] = 0.0f;
    cov[1][1] = 0.0f;
    cov[1][2] = 0.0f;
    cov[2][2] = 0.0f;
    for (int i = 0; i < sizec; ++i) {
        const float xdemean = nncv[i][0] - xmean;
        const float ydemean = nncv[i][1] - ymean;
        const float zdemean = nncv[i][2] - zmean;
        
        cov[0][0] += xdemean * xdemean;
        cov[0][1] += xdemean * ydemean;
        cov[0][2] += xdemean * zdemean;
        cov[1][1] += ydemean * ydemean;
        cov[1][2] += ydemean * zdemean;
        cov[2][2] += zdemean * zdemean;
    }
    
    cov[0][0] /= sizecf;
    cov[0][1] /= sizecf;
    cov[0][2] /= sizecf;
    cov[1][1] /= sizecf;
    cov[1][2] /= sizecf;
    cov[2][2] /= sizecf;
    
    // Set lower left part
    cov[1][0] = cov[0][1];
    cov[2][0] = cov[0][2];
    cov[2][1] = cov[1][2];
    
    // Eigen decomposition
    cv::Mat_<float> eigenvalues, eigenvectors;
    cv::eigen(cov, eigenvalues, eigenvectors);
    eigenvectors = eigenvectors.t();
    
    // Set normal
    input[src].normal_x = eigenvectors(2);
    input[src].normal_y = eigenvectors(5);
    input[src].normal_z = eigenvectors(8);
}

/*
 * Generate precision-recall data files, can be read into Matlab using dlmread()
 */
void pr(const core::Correspondence::VecPtr corr,
        const std::vector<bool>& mask,
        const std::vector<float>& conf,
        const std::string& filename) {
    COVIS_ASSERT(corr->size() == mask.size());
    COVIS_ASSERT(corr->size() == conf.size());
    
    // Find number of inliers
    size_t inliers = 0;
    for(std::vector<bool>::const_iterator it = mask.begin(); it != mask.end(); ++it)
        if(*it)
            ++inliers;
    
    // Sort confidences descending, and reorder mask
    std::vector<float> conf2 = conf;
    const std::vector<size_t> order = core::sort(conf2, false);
    const std::vector<bool> mask2 = core::reorder(mask, order);
    // Generate PR
    std::vector<double> p(corr->size()), r(corr->size());
    size_t sum = 0;
    for(size_t i = 0; i < corr->size(); ++i) {
        if(mask2[i])
            ++sum;
        p[i] = double(sum) / double(i+1);
        r[i] = double(sum) / double(inliers);
    }
    
    // Output
    std::ofstream ofs(filename.c_str());
    for(size_t i = 0; i < corr->size(); ++i)
        ofs << p[i] << " " << r[i] << std::endl;
}
